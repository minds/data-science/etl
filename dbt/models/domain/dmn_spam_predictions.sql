with coef as (
    select 
        -0.47273527 as intercept,
        -0.300538   as min_signup_seq,
         0.383349   as min_active_seq,
         0.853502   as min_email_confirm_seq,
        -0.461413   as min_vote_up_seq,
        -1.166961   as min_subscribe_seq
),

prediction as (
     select user_id,
            (
                    coef.intercept
                    + vars.min_signup_seq * coef.min_signup_seq
                    + vars.min_active_seq * coef.min_active_seq
                    + vars.min_email_confirm_seq * coef.min_email_confirm_seq
                    + vars.min_vote_up_seq * coef.min_vote_up_seq
                    + vars.min_subscribe_seq * coef.min_subscribe_seq
                )                                          as logit,
            (exp(logit) / (1 + exp(logit)))                as probability,
            case when probability >= 0.5 then 1 else 0 end as prediction,
            case when prediction = 1 then 'Spam' else 'Authentic' end as account_type,
            vars.min_signup_seq,
            vars.min_active_seq,
            vars.min_email_confirm_seq,
            vars.min_vote_up_seq,
            vars.min_subscribe_seq
     from 
        {{ ref('dmn_user_categorical_vars') }} as vars
              cross join coef
)

select * from prediction
