with

stg_comments as (
    select * from {{ ref("stg_comments") }}
),

spam_predict as (
  select * from {{ ref("stg_comments_predict_spam") }} where valid_to is null
),

comments as (
    select
        stg_comments.comment_guid,
        stg_comments.user_guid,
        stg_comments.entity_guid,
        stg_comments.parent_guid_l1,
        stg_comments.parent_guid_l2,
        stg_comments.parent_guid_l3,
        case
            when stg_comments.parent_guid_l3 is not null then 'tier_4'
            when stg_comments.parent_guid_l2 is not null then 'tier_3'
            when stg_comments.parent_guid_l1 is not null then 'tier_2'
            else 'tier_1'
        end as comment_tier,
        stg_comments.access_id,
        stg_comments.is_deleted,
        stg_comments.is_edited,
        stg_comments.is_mature,
        stg_comments.is_spam,
        stg_comments.has_children,
        stg_comments.body,
        stg_comments.comment_link,
        stg_comments.character_count,
        stg_comments.replies_count,
        stg_comments.time_created,
        stg_comments.time_updated,
        stg_comments.votes_up_count,
        stg_comments.votes_down_count,
        stg_comments.votes_up,
        stg_comments.votes_down,
        stg_comments.valid_from,
        stg_comments.valid_to,
        spam_predict.spam_predict,
        case
            when stg_comments.valid_to is null
            then true
            else false
        end as is_current
    from
        stg_comments
    left join
        spam_predict
            on stg_comments.comment_guid = spam_predict.comment_guid
)

select * from comments where is_current
