with

stg_activities as (
    select * from {{ ref("stg_activities") }}
),

dmn_activity as (
    select
        activity_guid,
        user_guid,
        group_guid,
        entity_guid,
        access_id,
        time_created,
        time_updated,
        time_sent,
        language,
        is_deleted,
        is_edited,
        is_enabled,
        is_mature,
        is_remind,
        remind_guid,
        nsfw,
        nsfw_lock,
        rating,
        is_spam,
        tags,
        boost_rejection_reason,
        votes_up,
        votes_down,
        title,
        blurb,
        message,
        case
            when title is null and message is null then 0
            when title is null then len(message)
            when message is null then len(title)
            else len(message) + len(title)
        end as post_length,
        valid_from,
        valid_to,
        case
            when valid_to is null
            then true
            else false
        end as is_current
    from
        stg_activities
)

select * from dmn_activity where is_current
