with coef as (
    select
        -0.00000000000142261608 as intercept,
         0.0000004353124   as first_activity_mins,
         0.00000000000001058055   as spam_comments
),

prediction as (
     select user_id,
            (
                    coef.intercept
                    + vars.first_activity_mins * coef.first_activity_mins
                    + vars.spam_comments * coef.spam_comments
                )                                          as logit,
            (exp(logit) / (1 + exp(logit)))                as cassandra_spam_account_probability,
            case when cassandra_spam_account_probability >= 0.5 then 1 else 0 end as cassandra_is_spam_account,
            case when cassandra_is_spam_account = 1 then 'Spam' else 'Authentic' end as cassandra_spam_account_type,
            vars.first_activity_mins,
            vars.spam_comments
     from
        {{ ref('dmn_cassandra_user_categorical_vars') }} as vars
              cross join coef
)

select * from prediction
