with

dmn_comments as (
    select * from {{ ref("dmn_comments") }}
),

dmn_activity as (
    select * from {{ ref("dmn_activity") }}
),

stg_object_album as (
    select * from {{ ref("stg_object_album") }}
),

stg_object_carousel as (
    select * from {{ ref("stg_object_carousel") }}
),

stg_object_image as (
    select * from {{ ref("stg_object_image") }}
),

int_tagged_engagement_stats as (
    select * from {{ ref("int_tagged_engagement_stats") }}
),

dmn_entity as (
    select
        comment_guid as entity_guid,
        user_guid,
        null as group_guid,
        'comment' as entity_type,
        access_id,
        time_created,
        time_updated,
        votes_up,
        votes_down,
        valid_from,
        valid_to
    from
        dmn_comments

    union

    select
        activity_guid as entity_guid,
        user_guid,
        group_guid,
        'activity' as entity_type,
        access_id,
        time_created,
        time_updated,
        votes_up,
        votes_down,
        valid_from,
        valid_to
    from
        dmn_activity

    union

    select
        entity_guid,
        user_guid,
        group_guid,
        'object_album' as entity_type,
        access_id,
        time_created,
        time_updated,
        votes_up,
        votes_down,
        valid_from,
        valid_to
    from
        stg_object_album
    where
        valid_to is null

    union

    select
        entity_guid,
        user_guid,
        group_guid,
        'object_carousel' as entity_type,
        access_id,
        time_created,
        time_updated,
        votes_up,
        votes_down,
        valid_from,
        valid_to
    from
        stg_object_carousel
    where
        valid_to is null

    union

    select
        entity_guid,
        user_guid,
        group_guid,
        'object_image' as entity_type,
        access_id,
        time_created,
        time_updated,
        votes_up,
        votes_down,
        valid_from,
        valid_to
    from
        stg_object_image
    where
        valid_to is null
),

engagement_score as (
    select
        dmn_entity.entity_guid,
        int_tagged_engagement_stats.total_engagement_ratio,
        dmn_entity.user_guid,
        dmn_entity.group_guid,
        dmn_entity.entity_type,
        dmn_entity.access_id,
        dmn_entity.time_created,
        dmn_entity.time_updated,
        dmn_entity.votes_up,
        dmn_entity.votes_down,
        dmn_entity.valid_to
    from
        dmn_entity
    left join
        int_tagged_engagement_stats
    on dmn_entity.entity_guid::varchar(120) = int_tagged_engagement_stats.entity_guid::varchar(120)
)

select * from engagement_score
