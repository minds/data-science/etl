with

dmn_entity as (
    select * from {{ ref("dmn_entity") }}
),

vote_up as (
    select
        entity_guid,
        entity_type,
        'up' as vote_type,
        user_vote.value::int as user_guid
    from
        dmn_entity,
        lateral flatten (input => votes_up, OUTER => TRUE) as user_vote
),

vote_down as (
    select
        entity_guid,
        entity_type,
        'down' as vote_type,
        user_vote.value::int as user_guid
    from
        dmn_entity,
        lateral flatten (input => votes_down, OUTER => TRUE) as user_vote
),

dmn_votes as (
    select
        entity_guid,
        entity_type,
        vote_type,
        user_guid
    from
        vote_up

    union

    select
        entity_guid,
        entity_type,
        vote_type,
        user_guid
    from vote_down
)

select * from dmn_votes
