with

stg_blockchain_transactions as (
    select * from {{ ref("stg_blockchain_transactions") }}
),

stg_token_rewards as (
    select * from {{ ref("stg_token_rewards") }}
),

unnest_transactions as (
    select
        transaction_guid,
        user_guid,
        tstamp,
        transaction_data:receiver_guid::string as receiver_guid,
        transaction_data:sender_guid::string as sender_guid,
        wallet_address,
        amount/pow(10,18)::numeric as amount,
        transaction_data:gas::string as gas,
        is_completed,
        transaction_type,
        transaction_data,
        case
            when transaction_type in ('boost', 'offchain:boost') and receiver_guid is null then 1
            else 0
        end as is_boost,
        case
            when transaction_type in ('boost', 'offchain:boost') and receiver_guid is not null then 1
            else 0
        end as is_boost_offer,
        case
            when transaction_type like '%offchain%' then 0
            else 1
        end as is_on_chain,
        is_failed,
        valid_from,
        valid_to,
        case
            when valid_to is null
            then true
            else false
        end as is_current
    from
        stg_blockchain_transactions
),

final as (
    select 
        unnest_transactions.transaction_guid,
        unnest_transactions.user_guid,
        unnest_transactions.tstamp,
        unnest_transactions.receiver_guid,
        unnest_transactions.sender_guid,
        unnest_transactions.wallet_address,
        case 
            when unnest_transactions.transaction_type ilike '%reward%' and unnest_transactions.amount is null 
            then stg_token_rewards.token_amount
            else unnest_transactions.amount
        end as amount,
        unnest_transactions.gas,
        unnest_transactions.is_completed,
        unnest_transactions.transaction_type,
        stg_token_rewards.reward_type,
        unnest_transactions.transaction_data,
        unnest_transactions.is_boost,
        unnest_transactions.is_boost_offer,
        unnest_transactions.is_on_chain,
        unnest_transactions.is_failed,
        unnest_transactions.valid_from,
        unnest_transactions.valid_to,
        unnest_transactions.is_current
    from 
        unnest_transactions
    left join stg_token_rewards
        on unnest_transactions.transaction_guid = stg_token_rewards.payout_transaction
    where unnest_transactions.is_current
    and unnest_transactions.is_failed is null 
)

select * from final
