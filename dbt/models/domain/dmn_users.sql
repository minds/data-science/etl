with

stg_users as (
  select * from {{ ref("stg_users") }}
),

dmn_comments as (
    select * from {{ ref("dmn_comments") }}
),

dmn_vote as (
    select * from {{ ref("dmn_vote") }}
),

dmn_activity as (
    select * from {{ ref("dmn_activity") }}
),

dmn_entity as (
    select * from {{ ref("dmn_entity") }}
),

dmn_cassandra_spam_predictions as (
    select * from {{ ref('dmn_cassandra_spam_predictions') }}
),

dmn_spam_predictions as (
    select * from {{ ref('dmn_spam_predictions') }}
),

engagement_score as (
    select
        user_guid,
        entity_guid
    from
        dmn_comments
    where comment_tier = 'tier_1'

    union

    select
        user_guid,
        remind_guid
    from
        dmn_activity
    where is_remind = 1

    union

    select
        user_guid,
        entity_guid
    from
        dmn_vote
),

user_engagement_score as (
    select
        engagement_score.user_guid,
        avg(dmn_entity.total_engagement_ratio) as avg_engagement_score
    from
        engagement_score
    left join dmn_entity
        on engagement_score.entity_guid = dmn_entity.entity_guid
    where dmn_entity.total_engagement_ratio is not null
    group by
        engagement_score.user_guid
),

user_engagement_count as (
    select
        user_guid,
        count(entity_guid) as engagement_count
    from
        engagement_score
    group by
        user_guid
),

content_engagement_score as (
    select
        user_guid,
        avg(dmn_entity.total_engagement_ratio) as avg_content_score
    from
        dmn_entity
    where entity_type in ('activity', 'object_image', 'object_carsousel', 'object_album')
    group by
        user_guid
),

content_engagement_count as (
    select
        user_guid,
        count(entity_guid) as content_count
    from
        dmn_entity
    where entity_type in ('activity', 'object_image', 'object_carsousel', 'object_album')
    group by
        user_guid
),

users as (
    select
        stg_users.user_guid,
        stg_users.username,
        user_engagement_score.avg_engagement_score,
        user_engagement_count.engagement_count,
        case
            when user_engagement_score.avg_engagement_score > 0.35 and user_engagement_count.engagement_count > 5 then true
            else false
        end as is_token_farming_engager,
        content_engagement_score.avg_content_score,
        content_engagement_count.content_count,
        case
            when content_engagement_score.avg_content_score > 0.35 and content_engagement_count.content_count > 5 then true
            else false
        end as is_token_farming_creator,
        dmn_cassandra_spam_predictions.cassandra_spam_account_probability,
        dmn_cassandra_spam_predictions.cassandra_is_spam_account,
        dmn_spam_predictions.probability as snowplow_spam_account_probability,
        dmn_spam_predictions.prediction as snowplow_is_spam_account,
        stg_users.time_created,
        stg_users.email_confirmed_at,
        stg_users.language,
        stg_users.dob,
        stg_users.gender,
        stg_users.ip,
        stg_users.is_verifed,
        stg_users.is_admin,
        stg_users.is_enabled,
        stg_users.is_banned,
        stg_users.is_deleted,
        stg_users.is_plus,
        stg_users.rating,
        stg_users.is_founder,
        stg_users.pro_expires,
        stg_users.is_pro_published,
        stg_users.last_login,
        stg_users.is_ban_monetization,
        stg_users.is_boost_autorotate,
        stg_users.boost_rating,
        stg_users.categories,
        stg_users.is_disabled_boost,
        stg_users.eth_incentive,
        stg_users.eth_wallet,
        stg_users.group_membership,
        stg_users.is_mature,
        stg_users.mature_content,
        stg_users.mature_lock,
        stg_users.mode,
        stg_users.monetization_settings,
        stg_users.is_onboarding_shown,
        stg_users.pinned_posts,
        stg_users.programs,
        stg_users.is_spam,
        stg_users.briefdescription,
        stg_users.tags,
        stg_users.signupparentid,
        stg_users.signuppreviousurl,
        stg_users.valid_from,
        stg_users.valid_to,
        case
            when stg_users.valid_to is null
            then true
            else false
        end as is_current
    from
        stg_users
    left join user_engagement_score
        on stg_users.user_guid = user_engagement_score.user_guid
    left join user_engagement_count
        on stg_users.user_guid = user_engagement_count.user_guid
    left join content_engagement_score
        on stg_users.user_guid = content_engagement_score.user_guid
    left join content_engagement_count
        on stg_users.user_guid = content_engagement_count.user_guid
    left join
        dmn_cassandra_spam_predictions
          on dmn_cassandra_spam_predictions.user_id = stg_users.user_guid
    left join
        dmn_spam_predictions
          on dmn_spam_predictions.user_id = stg_users.user_guid
),

user_type as (
    select
        user_guid,
        username,
        avg_engagement_score,
        engagement_count,
        is_token_farming_engager,
        avg_content_score,
        content_count,
        is_token_farming_creator,
        case
            when is_token_farming_engager = true and is_token_farming_creator = true
            then 'tf_creator_engager'
            when is_token_farming_engager = true and is_token_farming_creator = false
            then 'tf_engager'
            when is_token_farming_engager = false and is_token_farming_creator = true
            then 'tf_creator'
            else 'not_tf'
        end as user_type,
        cassandra_spam_account_probability,
        cassandra_is_spam_account,
        snowplow_spam_account_probability,
        snowplow_is_spam_account,
        time_created,
        email_confirmed_at,
        language,
        dob,
        gender,
        ip,
        is_verifed,
        is_admin,
        is_enabled,
        is_banned,
        is_deleted,
        is_plus,
        rating,
        is_founder,
        pro_expires,
        is_pro_published,
        last_login,
        is_ban_monetization,
        is_boost_autorotate,
        boost_rating,
        categories,
        is_disabled_boost,
        eth_incentive,
        eth_wallet,
        group_membership,
        is_mature,
        mature_content,
        mature_lock,
        mode,
        monetization_settings,
        is_onboarding_shown,
        pinned_posts,
        programs,
        is_spam,
        briefdescription,
        tags,
        signupparentid,
        signuppreviousurl,
        valid_from,
        valid_to,
        is_current
    from
        users
)

select * from user_type where is_current
