with   

stg_stripe_charges as (
    select * from {{ ref("stg_stripe_charges") }}
),

stg_stripe_invoices as (
    select * from {{ ref("stg_stripe_invoices") }}
),

stg_stripe_customers as (
    select * from {{ ref("stg_stripe_customers") }}
),

stg_stripe_refunds as (
    select * from {{ ref("stg_stripe_refunds") }}
),

stg_stripe_account_id_map as (
    select * from {{ ref("stg_stripe_account_id_map") }}
),

base as (
    select 
        stg_stripe_charges.charge_id,
        stg_stripe_charges.invoice_id, 
        stg_stripe_charges.balance_transaction,
        stg_stripe_invoices.plan,
        stg_stripe_charges.destination,
        stg_stripe_charges.customer_id,
        stg_stripe_invoices.line_description,
        coalesce(stg_stripe_charges.order_id, stg_stripe_invoices.order_id) as order_id,
        case 
            when stg_stripe_charges.user_guid = '1030390936930099216' or stg_stripe_charges.customer_id = 'cus_H5cDc4UqBJOzuP' 
            then 'Minds Pro Payouts'
            else coalesce(coalesce(stg_stripe_charges.user_guid, stg_stripe_customers.user_guid), stg_stripe_customers.customer_name)
        end as originating_user_id,
        coalesce(stg_stripe_charges.destination, stg_stripe_charges.on_behalf_of) as receiving_user_id,
        stg_stripe_charges.user_guid,
        stg_stripe_charges.boost_guid,
        stg_stripe_charges.boost_owner_guid,
        stg_stripe_charges.transaction_tstamp,
        stg_stripe_charges.amount,
        stg_stripe_charges.amount_captured,
        stg_stripe_charges.amount_refunded,
        stg_stripe_charges.currency,
        stg_stripe_charges.charge_status,
        stg_stripe_refunds.refund_status
    from 
        stg_stripe_charges
    left join stg_stripe_invoices
        on stg_stripe_charges.invoice_id = stg_stripe_invoices.invoice_id
    left join stg_stripe_refunds
        on stg_stripe_charges.charge_id = stg_stripe_refunds.charge_id
    left join stg_stripe_customers
        on stg_stripe_charges.customer_id = stg_stripe_customers.customer_id
),

sender_receiver as (
    select 
        charge_id,
        invoice_id, 
        balance_transaction,
        coalesce(originating_user_id, customer_id) as originating_user_id,
        case 
            when receiving_user_id = 'acct_1Ae0P0GHVmIhiZC8' then 'Minds Plus Account' 
            when receiving_user_id = 'acct_1FTOqBDHgMdCy9fL' then 'Minds Pro Account'
            when order_id ilike '%plus%' then 'Minds Plus Account'
            when (order_id ilike '%boost%' or order_id ilike '%points%' or plan = 'points') then 'Minds'
            when plan = 'plus' then 'Minds Plus Account'
            when invoice_id = 'in_1FjStuEtkBDgTlGKbZCHEOqQ' or line_description ilike '%Hosting%' then 'Minds'
            else receiving_user_id
        end as destination,
        transaction_tstamp,
        amount,
        amount_captured,
        amount_refunded,
        currency,
        charge_status,
        refund_status,
        case 
            when order_id ilike '%boost%' then 'boost'
            when (order_id ilike '%points%' or plan = 'points') then 'points'
            when order_id ilike '%plus%' then 'plus subscription'
            when plan = 'plus' then 'plus subscription'
            when invoice_id = 'in_1FjStuEtkBDgTlGKbZCHEOqQ' or line_description ilike '%Hosting%' then 'hosting/nodes'
        end as transaction_type
    from 
        base
),

final as (
    select 
        charge_id,
        invoice_id,
        balance_transaction,
        originating_user_id,
        coalesce(stg_stripe_account_id_map.user_guid::varchar, destination) as receiving_user_id,
        transaction_tstamp,
        (amount / 100)::int as amount,
        (amount_captured / 100)::int as amount_captured,
        (amount_refunded / 100)::int as amount_refunded,
        currency,
        charge_status,
        refund_status,
        case 
            when receiving_user_id = 'Minds Plus Account'  then 'plus subscription'
            when receiving_user_id = 'Minds Pro Account' then 'pro subscription'
            when receiving_user_id not ilike '%Minds%' and transaction_type is null then 'wire'
            when originating_user_id = 'Minds Pro Payouts' then 'Pro Payout'
            else lower(transaction_type)
        end as transaction_type
    from 
        sender_receiver
    left join stg_stripe_account_id_map
        on sender_receiver.destination = stg_stripe_account_id_map.stripe_account_id
)

select * from final