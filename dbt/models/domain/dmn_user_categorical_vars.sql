with
    users as (
        select 
            user_id,
            min(case when action='signup' then collector_tstamp end) as user_created
        from
            {{ ref('stg_snowplow_events_userid') }}
        group by
            user_id
        having
            user_created is not null
),

    user_events as (
        select
            users.user_id ,
            users.user_created,
            event.event_id,
            event.collector_tstamp as event_tstamp,
            event.action as event_action,
            row_number() over (partition by users.user_id order by collector_tstamp) as event_number,
            timestampdiff('hours', user_created, event_tstamp) as hours_since_signup
        from
            users
        left join
            {{ ref('stg_snowplow_events_userid') }} as event
                on users.user_id = event.user_id
        group by
            users.user_id,
            users.user_created,
            event_id,
            event.collector_tstamp,
            event.action
        having hours_since_signup < 168
    ),


    user_stats as (
        select user_id,
               min(case
                       when event_action = 'signup' then event_number 
                    end) as min_signup_seq,
               min(case
                       when event_action = 'active' then event_number 
                       end) as min_active_seq,
               min(case
                       when event_action = 'email:confirm' then event_number
                   end) as min_email_confirm_seq,
               min(case
                       when event_action = 'vote:up' then event_number 
                   end) as min_vote_up_seq,
               min(case
                       when event_action = 'subscribe' then event_number 
                   end) as min_subscribe_seq
        from 
            user_events
         group by 
            user_id
),

    events_final as (
         select distinct
             user_id,
             case when min_signup_seq > 0 then 1 else 0 end        as min_signup_seq,
             case when min_active_seq > 0 then 1 else 0 end        as min_active_seq,
             case when min_email_confirm_seq > 0 then 1 else 0 end as min_email_confirm_seq,
             case when min_vote_up_seq > 0 then 1 else 0 end       as min_vote_up_seq,
             case when min_subscribe_seq > 0 then 1 else 0 end     as min_subscribe_seq
         from 
             user_stats
)

select 
    events_final.user_id,
    events_final.min_signup_seq,
    events_final.min_active_seq,
    events_final.min_email_confirm_seq,
    events_final.min_vote_up_seq,
    events_final.min_subscribe_seq
from 
    events_final
