with

users_created as ( -- referencing stg table to allow results to be added to dmn_users table
  select
      user_guid as user_id,
      min(time_created) as user_created
  from
      {{ ref('stg_users') }}
  group by
      user_guid
  having
      user_created is not null
),

event_stream as (
  select * from {{ ref('int_event_stream') }}
),

dmn_comments as (
  select * from {{ ref('dmn_comments') }}
),

users as (
  select
    users_created.user_id,
    users_created.user_created,
    events.event_action
  from
    users_created
  cross join
      (select distinct event_action from event_stream) as events
),

spam_comments as (
  select
    user_guid,
    sum(is_spam) as spam_comments
  from
    dmn_comments
  group by
    user_guid
),

metrics as (
  select
    users.user_id,
    users.user_created,
    users.event_action,
    event_stream.event_count,
    event_stream.mins_to_first,
    event_stream.min_sequence
  from
    users
  left join
    event_stream
      on event_stream.user_guid = users.user_id
      and users.event_action = event_stream.event_action
  order by
    user_id
),

user_stats as (
  select
      user_id,
      min(case
               when event_action = 'activity'
                   then mins_to_first end) as first_activity_mins,
      max(spam_comments) as spam_comments
  from
     metrics
  left join
    spam_comments
        on spam_comments.user_guid::varchar = metrics.user_id::varchar
  group by
    user_id
)

select * from user_stats
