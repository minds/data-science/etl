with
dmn_comments as (
    select * from {{ ref('dmn_comments') }}
),

moderation_reports as (
    select * from {{ ref('stg_moderation_reports') }}
),

final as (
select
    user_guid,
    count(distinct comment_guid) as total_comments,
    count(distinct case
                       when spam_predict >= 0.5 then comment_guid
                   end
          ) as spam_comments,
    count(distinct case when spam_predict >= 0.5
                           and comment_link is not null then comment_guid
                   end
          ) as spam_comments_with_link,
    spam_comments / nullif(total_comments,0) as spam_percent,
    spam_comments_with_link / nullif(spam_comments,0) as link_percent,
    count(distinct case when spam_predict >= 0.5 and comment_link is not null then comment_link end) as unique_spam_links,
    greatest(datediff(second, max(case when spam_predict >= 0.5 then time_created end), current_date()), 1::int) as secs_since_most_recent_spam_comment,

    (spam_comments
        + (spam_comments_with_link*0.5))
        / log(2, secs_since_most_recent_spam_comment/86400.0)
    as score
from
    dmn_comments
where
    user_guid::integer not in (select entity_urn::integer
                      from moderation_reports
                      where entity_type = 'user' and reason_code = 8)
group by
    user_guid
having
    ((spam_comments > 5
        and spam_percent >= 0.6) or
    (spam_comments > 20
        and spam_percent >= 0.4))
        and secs_since_most_recent_spam_comment < 5184000
order by
    score desc
)

select * from final order by score desc
