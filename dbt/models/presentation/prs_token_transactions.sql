with

dmn_blockchain_transactions as (
    select * from {{ ref("dmn_blockchain_transactions") }}
),

dmn_users as (
    select * from {{ ref("dmn_users") }}
),

token_transactions as (
    select 
        transaction_guid,
        dmn_blockchain_transactions.transaction_type,
        dmn_blockchain_transactions.reward_type,
        tstamp,
        case 
            when dmn_blockchain_transactions.transaction_type in ('offchain:wire', 'wire') or is_boost_offer = 1 then dmn_blockchain_transactions.sender_guid
            when dmn_blockchain_transactions.is_boost = 1 then dmn_blockchain_transactions.user_guid
            else 'Minds' 
        end as originating_user_id,
        case 
            when dmn_blockchain_transactions.transaction_type in ('offchain:wire', 'wire') or is_boost_offer = 1 then sender_user.user_type
            when dmn_blockchain_transactions.is_boost = 1 then dmn_users.user_type
            else 'not_tf'
        end as originating_user_type,
        case 
            when dmn_blockchain_transactions.transaction_type in ('offchain:wire', 'wire') or is_boost_offer = 1 then dmn_blockchain_transactions.receiver_guid
            when dmn_blockchain_transactions.transaction_type in ('withdraw', 'offchain:plus', 'offchain:Pro Payout', 'offchain:reward') then dmn_blockchain_transactions.user_guid
            else 'Minds'
        end as receiving_user_id,
        case 
            when dmn_blockchain_transactions.transaction_type in ('offchain:wire', 'wire') or is_boost_offer = 1 then receiver_user.user_type
            when dmn_blockchain_transactions.transaction_type in ('withdraw', 'offchain:plus', 'offchain:Pro Payout', 'offchain:reward') then dmn_users.user_type
            else 'not_tf'
        end as receiving_user_type,
        dmn_blockchain_transactions.is_boost,
        dmn_blockchain_transactions.is_boost_offer,
        dmn_blockchain_transactions.is_on_chain,
        case
            when is_boost = 1 then ABS(dmn_blockchain_transactions.amount)
            --list of old transaction_ids which seem to be negative for some reason and are affecting analysis
            when transaction_guid in (
                'oc:953978075551375362',
                'oc:953978980938031107',
                'oc:953977851487461389',
                'oc:953979219178692626',
                'oc:953978288651378698',
                'oc:953977064992546818',
                'oc:953978731112701959',
                'oc:953978188554313742',
                'oc:953979139348504586',
                'oc:953977314012569617',
                'oc:953977560880914446',
                'oc:953977984937631762',
                'oc:953977763973308432',
                'oc:953977436846956554',
                'oc:953979069349765140',
                'oc:953978804320083969',
                'oc:953977670067036178',
                'oc:953978897710456850',
                '0x49be7df1a460e953c051e5da2cf0ba18e49eb9d06edbb7e62b1c1fc98f12aaeb',
                'oc:953977057870618625'
            ) then ABS(dmn_blockchain_transactions.amount)
            else dmn_blockchain_transactions.amount
        end as transaction_value,
        dmn_blockchain_transactions.gas as fee
    from 
        dmn_blockchain_transactions
    left join dmn_users on 
            dmn_users.user_guid = dmn_blockchain_transactions.user_guid
    left join dmn_users as sender_user on
            sender_user.user_guid = dmn_blockchain_transactions.sender_guid
    left join dmn_users as receiver_user on 
            receiver_user.user_guid = dmn_blockchain_transactions.receiver_guid
    where
        dmn_blockchain_transactions.transaction_type in ('withdraw', 'offchain:plus', 'offchain:Pro Payout', 'offchain:reward')
    or (dmn_blockchain_transactions.transaction_type in ('offchain:wire', 'wire') and sign(transaction_value) = 1)
    or (dmn_blockchain_transactions.transaction_type in ('boost', 'offchain:boost') and is_boost = 1)
    or (dmn_blockchain_transactions.transaction_type in ('boost', 'offchain:boost') and is_boost_offer = 1 and sign(transaction_value) = 1) 
)

select * from token_transactions