with

dates as (
-- creates a date spine
{{ dbt_utils.date_spine(
    datepart="month",
    start_date="cast('2021-01-01' as date)",
    end_date="current_date")
}}
),

engagement as (
    select
        dates.date_month::date as date_month,
        coalesce(event_aggregate.user_type, sessions_aggregate.user_type) as user_type,
        coalesce(event_aggregate.platform, sessions_aggregate.platform) as user_platform,
        coalesce(event_aggregate.app_version, sessions_aggregate.app_version) as app_version,
        coalesce(event_aggregate.device_type, sessions_aggregate.device_type) as device_type,
        event_aggregate.unique_user_engagement,
        event_aggregate.unique_user_comments,
        event_aggregate.unique_user_reminds,
        event_aggregate.unique_user_posts,
        event_aggregate.unique_user_up_votes,
        event_aggregate.unique_user_down_votes,
        event_aggregate.unique_user_subscribes,
        event_aggregate.unique_user_searches,
        sessions_aggregate.unique_users,
        sessions_aggregate.event_count,
        sessions_aggregate.sessions_count,
        sessions_aggregate.total_session_time_minutes/60 as total_session_time_minutes,
        sessions_max.max_unique_sessions,
        case
            when
                sessions_aggregate.unique_users = sessions_max.max_unique_sessions
            then
                true
            else
                false
        end as all_time_highest_users,
        case
            when
                sessions_aggregate.unique_users > sessions_max.max_unique_sessions * 0.9
            then
                true
            else
                false
        end as users_exceed_90pct
    from
        dates
    left join
        (
        select
            dates.date_month::date as date_month,
            events.platform,
            events.app_version,
            events.user_type,
            events.device_type,
            count(distinct case when events.action in ('comment', 'remind','vote:up','vote:down') then events.user_id end) as unique_user_engagement,
            count(distinct case when events.action = 'comment' then events.user_id end) as unique_user_comments,
            count(distinct case when events.action = 'remind' then events.user_id end) as unique_user_reminds,
            count(distinct case when events.entity_guid is not null then events.user_id end) as unique_user_posts,
            count(distinct case when events.action = 'vote:up' then events.user_id end) as unique_user_up_votes,
            count(distinct case when events.action = 'vote:down' then events.user_id end) as unique_user_down_votes,
            count(distinct case when events.action = 'subscribe' then events.user_id end) as unique_user_subscribes,
            count(distinct case when events.page_url like '%/search%' then events.user_id end) as unique_user_searches
        from
            dates
        left join
            (select
                case
                    when
                        events_case.br_name = 'Unknown'
                    then
                        'app'
                    else
                        'web'
                end as platform,
                case
                    when
                        (events_case.useragent ilike '%Android%' and events_case.br_name = 'Unknown')
                    then
                        concat('android ', substring(split_part(events_case.useragent,'/',2),0,7))
                    when
                        (events_case.useragent ilike '%iPhone%' and events_case.br_name = 'Unknown')
                    then
                        concat('ios ', substring(split_part(events_case.useragent,'/',2),0,7))
                    else
                        'web'
                end as app_version,
                case when events_case.user_id regexp '^[0-9a-z]{16,22}$' or
                          events_case.user_id regexp '^[0-9]{18,19}$' then 'Registered User'
                     else 'Not Registered User'
                end as user_type,
                events_case.dvce_type as device_type,
                events_case.user_id,
                events_case.action,
                events_case.entity_guid,
                events_case.entity_owner_guid,
                events_case.page_url,
                events_case.collector_tstamp
            from
                {{ ref('stg_snowplow_events_userid') }} as events_case
        ) as events
        on
            dates.date_month = date_trunc('month', events.collector_tstamp)::date
        group by
            dates.date_month,
            platform,
            app_version,
            device_type,
            user_type
        ) as event_aggregate
    on
        dates.date_month = event_aggregate.date_month
    left join
        (
        select
            sessions.date_month,
            sessions.platform,
            sessions.app_version,
            sessions.user_type,
            sessions.device_type,
            count(distinct sessions.user_id) as unique_users,
            sum(sessions.event_count) as event_count,
            count(sessions.*) as sessions_count,
            sum(sessions.duration) as total_session_time_minutes
        from
            (
              select
                  dates.date_month::date as date_month,
                  snowplow_sessions.user_id,
                  case when snowplow_sessions.user_id regexp '^[0-9a-z]{16,22}$' or
                            snowplow_sessions.user_id regexp '^[0-9]{18,19}$' then 'Registered User'
                       else 'Not Registered User'
                  end as user_type,
                  case
                      when
                          snowplow_sessions.br_name = 'Unknown'
                      then
                          'app'
                      else
                          'web'
                  end as platform,
                  case
                      when
                          (snowplow_sessions.useragent ilike '%Android%' and snowplow_sessions.br_name = 'Unknown')
                      then
                          concat('android ', substring(split_part(snowplow_sessions.useragent,'/',2),0,7))
                      when
                          (snowplow_sessions.useragent ilike '%iPhone%' and snowplow_sessions.br_name = 'Unknown')
                      then
                          concat('ios ',  substring(split_part(snowplow_sessions.useragent,'/',2),0,7))
                      else
                          'web'
                  end as app_version,
                  snowplow_sessions.dvce_type as device_type,
                  snowplow_sessions.first_tstamp,
                  snowplow_sessions.duration,
                  snowplow_sessions.event_count
              from
                  dates
              left join
                  {{ ref('stg_snowplow_sessions') }} as snowplow_sessions
              on
                  dates.date_month = date_trunc('month', snowplow_sessions.first_tstamp)::date
            ) as sessions
            group by
                sessions.date_month,
                sessions.platform,
                sessions.app_version,
                sessions.device_type,
                sessions.user_type
        ) as sessions_aggregate
      on
          dates.date_month = sessions_aggregate.date_month
      and
          event_aggregate.platform = sessions_aggregate.platform
      and
          event_aggregate.app_version = sessions_aggregate.app_version
      and
          event_aggregate.device_type = sessions_aggregate.device_type
      and
          event_aggregate.user_type = sessions_aggregate.user_type
      cross join
          (
          select
              max(max_session_count.unique_users) as max_unique_sessions
          from
              (
              select
                  session_count.date_month,
                  session_count.platform,
                  session_count.app_version,
                  session_count.device_type,
                  count(distinct session_count.user_id) as unique_users
              from
                  (
                    select
                        dates.date_month::date as date_month,
                        snowplow_sessions_2.user_id,
                        case
                            when
                                snowplow_sessions_2.br_name = 'Unknown'
                            then
                                'app'
                            else
                                'web'
                        end as platform,
                        case
                            when
                                (snowplow_sessions_2.useragent ilike '%Android%' and snowplow_sessions_2.br_name = 'Unknown')
                            then
                                concat('android ', substring(split_part(snowplow_sessions_2.useragent,'/',2),0,7))
                            when
                                (snowplow_sessions_2.useragent ilike '%iPhone%' and snowplow_sessions_2.br_name = 'Unknown')
                            then
                                concat('ios ', substring(split_part(snowplow_sessions_2.useragent,'/',2),0,7))
                            else
                                'web'
                        end as app_version,
                        case when snowplow_sessions_2.user_id regexp '^[0-9a-z]{16,22}$' or
                                  snowplow_sessions_2.user_id regexp '^[0-9]{18,19}$' then 'Registered User'
                             else 'Not Registered User'
                        end as user_type,
                        snowplow_sessions_2.dvce_type as device_type
                    from
                        dates
                    left join
                        {{ ref('stg_snowplow_sessions') }} as snowplow_sessions_2
                    on
                        dates.date_month = date_trunc('month', snowplow_sessions_2.first_tstamp)::date
                    ) as session_count
              group by
              session_count.date_month,
              session_count.platform,
              session_count.app_version,
              session_count.device_type,
              session_count.user_type
            ) as max_session_count
        ) as sessions_max
      order by
          dates.date_month,
          user_platform,
          app_version,
          device_type
)
select * from engagement
