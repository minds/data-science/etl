with
dmn_comments as (
    select * from {{ ref('dmn_comments') }}
),

int_tagged_engagement_stats as (
    select * from {{ ref('int_tagged_engagement_stats') }}
),

moderation_reports as (
    select * from {{ ref('stg_moderation_reports') }}
),

combined as (
select
    dmn_comments.comment_guid,
    dmn_comments.time_created,
    greatest(datediff(second, dmn_comments.time_created,current_timestamp()), 1::int) as comment_age_secs,
    dmn_comments.user_guid,
    dmn_comments.entity_guid,
    dmn_comments.parent_guid_l1,
    dmn_comments.parent_guid_l2,
    dmn_comments.parent_guid_l3,
    dmn_comments.comment_tier,
    dmn_comments.is_deleted,
    dmn_comments.is_spam,
    dmn_comments.is_mature,
    dmn_comments.body,
    dmn_comments.comment_link,
    dmn_comments.spam_predict,
    int_tagged_engagement_stats.total_views as activity_views,
    int_tagged_engagement_stats.last_engagement,
    greatest(datediff(second, int_tagged_engagement_stats.last_engagement, current_timestamp()), 1::int) as secs_since_last_engagement
from
    dmn_comments
        left join
            int_tagged_engagement_stats
                on dmn_comments.entity_guid = int_tagged_engagement_stats.entity_guid
where
    comment_guid not in (select entity_urn::number(38) from stg_moderation_reports where entity_type = 'comment')
        and dmn_comments.spam_predict >= 0.5
        and is_current = true
        and is_deleted = false
        and last_engagement is not null
),

scores as (
  select
    *,
    spam_predict
       * (case when comment_link is null then 1 else 1.5 end)
       / log(2,secs_since_last_engagement/86400.0)
       / log(2,comment_age_secs/86400.0)
    as score
  from
    combined
)

select * from scores order by score desc
