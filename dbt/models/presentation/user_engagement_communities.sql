with
interaction_events as (
    select * from {{ ref('int_user_interaction_events') }}
),

clusters as (
    select * from {{ source('cassandra_extract', 'int_interaction_clusters') }}
),

users as (
    select * from {{ ref('dmn_users')}}
),

combined as (
select
    interaction_events.*,
    engager.community as engager_community,
    owner.community as owner_community

from
    interaction_events
        left join
            clusters as engager
                on engager.user_guid = interaction_events.engager_guid
        left join
            clusters as owner
                on owner.user_guid = interaction_events.owner_guid
),

engagements_to_user as (
select
    owner_guid as user_guid,
    owner_community as user_community,
    count(distinct engager_guid) as unique_engagers,
    count(*) as total_engagements_from_other_users,
    count(case when owner_community != engager_community then engager_guid end) as engagements_from_external_users,
    count(case when owner_community = engager_community then engager_guid end) as engagements_from_internal_users
from
    combined
group by 1,2
),

engagements_by_user as (
select
    engager_guid as user_guid,
    owner_community as user_community,
    count(*) as total_engagements_to_other_users,
    count(distinct owner_guid) as unique_owners,
    count(case when owner_community != engager_community then owner_guid end) as engagements_to_external_users,
    count(case when owner_community = engager_community then owner_guid end) as engagements_to_internal_users
from
    combined
group by 1,2
),

aggregated as (
select
    coalesce(engagements_to_user.user_guid, engagements_by_user.user_guid) as user_guid,
    coalesce(engagements_to_user.user_community, engagements_by_user.user_community) as user_community,
    max(total_engagements_from_other_users) as total_engagements_from_other_users,
    max(unique_engagers) as unique_engagers,
    max(engagements_from_external_users) as engagements_from_external_users,
    max(engagements_from_internal_users) as engagements_from_internal_users,
    max(total_engagements_to_other_users) as total_engagements_to_other_users,
    max(unique_owners) as unique_owners,
    max(engagements_to_external_users) as engagements_to_external_users,
    max(engagements_to_internal_users) as engagements_to_internal_users
from
    engagements_to_user
        full outer join engagements_by_user
            on engagements_to_user.user_guid = engagements_by_user.user_guid
group by 1, 2
),

engagement_rates as (
select
    aggregated.user_guid,
    users.user_type,
    aggregated.user_community,
    aggregated.total_engagements_from_other_users,
    aggregated.unique_engagers,
    aggregated.engagements_from_external_users,
    aggregated.engagements_from_internal_users,
    aggregated.total_engagements_to_other_users,
    aggregated.unique_owners,
    aggregated.engagements_to_external_users,
    aggregated.engagements_to_internal_users,
    aggregated.engagements_from_internal_users / total_engagements_from_other_users as engagement_from_community_perc,
    aggregated.engagements_to_internal_users / total_engagements_to_other_users as engagement_to_community_perc
from
    aggregated
        inner join
            users
                on aggregated.user_guid = users.user_guid
)

select * from engagement_rates
