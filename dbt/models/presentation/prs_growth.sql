with users as (
    select
        user_id,
        min(case when action='signup' then collector_tstamp end) as time_created,
        min(case when action='email:confirm' then collector_tstamp end) as email_confirmed_at
    from
        {{ ref('stg_snowplow_events_userid') }}
    group by
        user_id
    having
        time_created is not null
),
user_events as (
    select
        events.user_id,
        events.collector_tstamp,
        events.page_url,
        events.page_referrer,
        events.action,
        events.mkt_source,
        events.mkt_medium,
        events.mkt_campaign,
        users.time_created as sign_up_timestamp,
        users.email_confirmed_at as registered_timestamp
    from
        {{ ref('stg_snowplow_events_userid') }} as events
    left join users on
        events.user_id = users.user_id
),
first_values as (
    select
        *
    from (
        select
            user_id,
            collector_tstamp as first_visit_timestamp,
            page_url as first_visit_url,
            action as first_visit_action,
            page_referrer as first_touch_referrer,
            mkt_source as first_touch_source,
            mkt_medium as first_touch_medium,
            mkt_campaign as first_touch_campaign,
            sign_up_timestamp,
            row_number() over (partition by user_id order by collector_tstamp) as row_number
        from
            user_events
    ) as first_rows
    where
        row_number = 1
),
last_values as (
    select
        *
    from (
        select
            user_id,
            mkt_source as last_visit_source,
            mkt_medium as last_visit_medium,
            mkt_campaign as last_visit_campaign,
            row_number() over (partition by user_id order by collector_tstamp desc) as row_number
        from
            user_events
        where
            registered_timestamp is not null
            and collector_tstamp < registered_timestamp
    ) as last_rows
    where
        row_number = 1
),
after as (
    select
        user_id,
        registered_timestamp,
        count(
            case when
                collector_tstamp > timestampadd('hour', 1, registered_timestamp)
                and collector_tstamp < timestampadd('hour', 24, registered_timestamp)
            then 1
            end
        ) as active_d1_count,
        count(
            case when
                collector_tstamp > timestampadd('hour', 24, registered_timestamp)
                and collector_tstamp < timestampadd('hour', 48, registered_timestamp)
            then 1
            end
        ) as active_d2_count,
        count(
            case when
                collector_tstamp > timestampadd('hour', 48, registered_timestamp)
                and collector_tstamp < timestampadd('hour', 72, registered_timestamp)
            then 1
            end
        ) as active_d3_count,
        max(collector_tstamp) as last_active
    from
        user_events
    where
        registered_timestamp is not null
        and collector_tstamp > registered_timestamp
    group by
        user_id,
        registered_timestamp
)
select
    first_values.user_id,
    first_values.first_visit_timestamp,
    first_values.first_visit_url,
    first_values.first_visit_action,
    first_values.first_touch_referrer,
    first_values.first_touch_source,
    first_values.first_touch_medium,
    first_values.first_touch_campaign,
    last_values.last_visit_source,
    last_values.last_visit_medium,
    last_values.last_visit_campaign,
    first_values.sign_up_timestamp,
    after.registered_timestamp,
    after.active_d1_count,
    after.active_d2_count,
    after.active_d3_count,
    after.last_active,
    timestampdiff('hours', after.registered_timestamp, after.last_active) as last_active_delta_hours,
    predictions.account_type,
    case
        when registered_timestamp is not null
        then timestampadd('hour', 72, registered_timestamp)
    end as d3_date,
    case
        when sign_up_timestamp is not null
        then true
        else false
    end as is_signed_up,
    case
        when registered_timestamp is not null
        then true
        else false
    end as is_registered,
    case
        when active_d1_count > 0
        then true
        else false
    end as is_active_d1,
    case
        when active_d2_count > 0
        then true
        else false
    end as is_active_d2,
    case
        when active_d3_count > 0
        then true
        else false
    end as is_active_d3,
    case
        when first_touch_medium ilike '%email%' then 'email'
        when first_touch_medium in ('cpc') then 'Paid Search'
        when first_touch_medium = 'fb' or
             first_touch_referrer ilike '%facebook.com%' or
             first_touch_referrer ilike '%youtube.com%' or
             first_touch_referrer ilike '%twitter.com%' or
             first_touch_referrer ilike '%pinterest.com%' or
             first_touch_referrer ilike '%instagram.com%' or
             first_touch_referrer ilike '%reddit.com%' or
             first_touch_medium = 'social' or
             first_touch_medium = 'twitter' or
             first_touch_medium = 'social_post' or
             first_touch_medium = 'quora' or
             first_touch_medium = 'social_link' or
             first_touch_medium = 'zalo' then 'organic_social'
        when (first_touch_source = 'google' or
             first_touch_referrer ilike '%google.com%' or 
             first_touch_referrer ilike '%duckduckgo%' or
             first_touch_referrer ilike '%search.yahoo.com%' or
             first_touch_referrer ilike '%bing.com%' or
             first_touch_source = 'bing') and
             first_touch_medium not in ('cpc') then 'organic_search'
        when first_touch_medium = 'referral' then 'referral'
        when first_touch_referrer ilike '%minds.com%' or
             first_touch_source = 'minds' or first_touch_referrer not like '%.%' then 'minds.com'
        when first_touch_medium is null and first_touch_source is null then 'no_data'
        else 'other'
    end as channel
from
    first_values
    left join last_values on
        first_values.user_id = last_values.user_id
    left join after on
        first_values.user_id = after.user_id
    left join {{ ref('dmn_spam_predictions') }} as predictions on
        predictions.user_id = first_values.user_id and
        first_values.sign_up_timestamp is not null
    

