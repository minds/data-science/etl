with

dmn_platform_engagement as (
    select * from {{ ref("stg_snowplow_events_userid") }}
    where event != 'page_ping'
),

stg_snowplow_sessions as (
    select * from {{ ref("stg_snowplow_sessions") }}
),

dmn_activity as (
  select * from {{ ref("dmn_activity")}}
),

signups as (
    select
        user_id,
        min(collector_tstamp) as user_signup_at
    from
        dmn_platform_engagement
    where
        user_id is not null and action = 'signup'
    group by
        user_id
),

sessions as (
    select
        user_id,
        first_tstamp,
        last_tstamp,
        useragent,
        user_ipaddress,
        session_number as user_session_sequential
    from
        stg_snowplow_sessions
),

viewed as (
    select
        coalesce(
            dmn_platform_engagement.view_entity_guid,
            dmn_platform_engagement.entity_guid) as entity_guid,
        dmn_platform_engagement.user_id,
        dmn_platform_engagement.collector_tstamp as first_view_date,
        dmn_platform_engagement.dvce_type as first_viewed_device_type,
        dmn_platform_engagement.br_lang as first_viewed_region,
        dmn_platform_engagement.entity_subtype,
        sessions.user_session_sequential
    from
        dmn_platform_engagement
            left join sessions
                on dmn_platform_engagement.user_id::varchar = sessions.user_id::varchar
                and dmn_platform_engagement.useragent = sessions.useragent
                and dmn_platform_engagement.user_ipaddress = sessions.user_ipaddress
                and dmn_platform_engagement.collector_tstamp between sessions.first_tstamp and sessions.last_tstamp
    where
        dmn_platform_engagement.event_name = 'view'
        and (dmn_platform_engagement.entity_type in ('activity', 'object')
            or dmn_platform_engagement.entity_type is null)
        and coalesce(
            dmn_platform_engagement.view_entity_guid,
            dmn_platform_engagement.entity_guid) is not null
        and dmn_platform_engagement.user_id is not null
),

first_viewed as (
    select
        entity_guid,
        user_id,
        first_view_date,
        first_viewed_device_type,
        first_viewed_region,
        entity_subtype,
        user_session_sequential
    from
        viewed
    qualify(row_number() over (
        partition by user_id, entity_guid
        order by first_view_date) = 1)
),

deleted as (
    select distinct
        entity_guid as deleted_entity_guid
    from
        dmn_platform_engagement
    where
        action = 'delete'
        and (entity_type in ('activity', 'object')
            or entity_type is null)
        and entity_guid is not null
),

viewed_undeleted as (
    select
        *
    from
        first_viewed
        left join deleted on
            first_viewed.entity_guid::varchar = deleted.deleted_entity_guid::varchar
),

engaged as (
    select distinct
        entity_guid,
        user_id,
        collector_tstamp as engaged_at,
        dvce_type as engaged_device_type,
        br_lang as engaged_region,
        useragent,
        user_ipaddress
    from
        dmn_platform_engagement as events
    where
        events.action in ('vote:up', 'remind', 'comment', 'vote:down')
        and events.entity_type = 'activity'
        and user_id is not null

    union

    select distinct
        dmn_activity.activity_guid::varchar as entity_guid,
        events.user_id,
        events.collector_tstamp as engaged_at,
        dvce_type as engaged_device_type,
        br_lang as engaged_region,
        useragent,
        user_ipaddress
    from
        dmn_platform_engagement as events
        inner join dmn_activity on
            events.entity_guid::varchar = dmn_activity.entity_guid::varchar
    where
        events.action in ('vote:up', 'remind', 'comment', 'vote:down')
        and events.entity_type = 'object'
        and user_id is not null
),

first_engaged as (
    select
        engaged.entity_guid,
        engaged.user_id,
        engaged.engaged_at,
        engaged.engaged_device_type,
        engaged.engaged_region,
        sessions.user_session_sequential
    from
        engaged
            left join sessions
                on engaged.user_id::varchar = sessions.user_id::varchar
                and engaged.useragent = sessions.useragent
                and engaged.user_ipaddress = sessions.user_ipaddress
                and engaged.engaged_at between sessions.first_tstamp and sessions.last_tstamp
    qualify(row_number() over (
        partition by engaged.user_id, engaged.entity_guid
        order by engaged.engaged_at) = 1)
),

combined as (
    select
        viewed_undeleted.user_id,
        signups.user_signup_at,
        viewed_undeleted.entity_subtype as activity_type,
        dmn_activity.post_length as activity_length,
        dmn_activity.tags as activity_tags,
        viewed_undeleted.entity_guid as activity_viewed_id,
        viewed_undeleted.first_view_date as first_viewed_at,
        viewed_undeleted.first_viewed_device_type,
        viewed_undeleted.first_viewed_region,
        viewed_undeleted.user_session_sequential as first_viewed_session_rank,
        first_engaged.engaged_at,
        first_engaged.engaged_device_type,
        first_engaged.engaged_region,
        first_engaged.user_session_sequential as engaged_session_rank,
        case
            when first_engaged.engaged_at is not null then 1
            else 0
        end as is_engaged,
        datediff('minute', viewed_undeleted.first_view_date, first_engaged.engaged_at) as mins_to_engage,
        datediff('second', viewed_undeleted.first_view_date, first_engaged.engaged_at) as secs_to_engage,
        first_engaged.user_session_sequential - viewed_undeleted.user_session_sequential as sessions_to_engage
    from
        viewed_undeleted
        left join signups
            on viewed_undeleted.user_id::varchar = signups.user_id::varchar
        left join first_engaged on
            viewed_undeleted.entity_guid::varchar = first_engaged.entity_guid::varchar
            and viewed_undeleted.user_id::varchar = first_engaged.user_id::varchar
        left join dmn_activity
            on viewed_undeleted.entity_guid::varchar = dmn_activity.activity_guid::varchar
    where
        viewed_undeleted.user_id is not null
        and viewed_undeleted.deleted_entity_guid is null
)

select * from combined
