with

dmn_users as (
    select * from {{ ref('dmn_users') }}
),

final as (
    select
        user_guid,
        time_created,
        user_type,
        engagement_count,
        avg_engagement_score,
        content_count,
        avg_content_score,
        case
            when user_type = 'tf_engager' then ifnull(engagement_count,0)*ifnull(avg_engagement_score,0)
            when user_type like 'tf_creat_r' then ifnull(content_count,0)*ifnull(avg_content_score,0)
            when user_type like 'tf_creat_r_engager'
                then (ifnull(engagement_count,0)+ifnull(content_count,0))*(ifnull(avg_engagement_score,0)*ifnull(avg_content_score,0))
        end as score
    from
        dmn_users
    where
        user_type <> 'not_tf'
            and (is_banned = false or is_banned is null)
            and (is_deleted = false or is_deleted is null)
            and ((avg_engagement_score >= 0.7 and engagement_count > 20)
                   or (avg_content_score >= 0.7 and content_count > 20))
)

select * from final order by score desc

        
