with 

dmn_stripe_transactions as (
    select * from {{ ref('dmn_stripe_transactions') }}
),

dmn_users as (
    select * from {{ ref('dmn_users') }}
),

prs_token_transactions as (
    select * from {{ ref('prs_token_transactions')}}
),

subscription as (
    select 
        originating_user_id,
        count(case 
                when transaction_type in ('plus subscription', 'pro subscription') 
                then 1 else null end) 
        as subscribed_time
    from 
        dmn_stripe_transactions
    group by 
        originating_user_id
),

subscription_dates as (
    select  
        originating_user_id as user_id,
        transaction_type,
        transaction_tstamp as subscription_start_date,
        dateadd(day, 30, transaction_tstamp) as subscription_end_date
    from 
        dmn_stripe_transactions
    where transaction_type in ('plus subscription', 'pro subscription')
    and charge_status = 'succeeded'
    and refund_status is null 
),

stripe_revenue as (
    select 
        dmn_stripe_transactions.balance_transaction,
        dmn_stripe_transactions.transaction_tstamp,
        dmn_stripe_transactions.transaction_type,
        dmn_stripe_transactions.amount_captured as amount,
        dmn_stripe_transactions.currency,
        dmn_stripe_transactions.originating_user_id,
        subscription.subscribed_time as originating_subscribed_time,
        sending_user.user_type as originating_user_type,
        dmn_stripe_transactions.receiving_user_id,
        receiving_user.user_type as receiving_user_type
    from 
        dmn_stripe_transactions
    left join dmn_users sending_user
        on dmn_stripe_transactions.originating_user_id = sending_user.user_guid
    left join dmn_users receiving_user
        on dmn_stripe_transactions.receiving_user_id = receiving_user.user_guid
    left join subscription
        on dmn_stripe_transactions.originating_user_id = subscription.originating_user_id
    where dmn_stripe_transactions.charge_status = 'succeeded'
    and dmn_stripe_transactions.refund_status is null
),

token_revenue as (
    select 
        prs_token_transactions.transaction_guid,
        prs_token_transactions.tstamp,
        prs_token_transactions.transaction_type,
        prs_token_transactions.reward_type,
        prs_token_transactions.transaction_value,
        'token' as currency,
        prs_token_transactions.originating_user_id,
        subscription.subscribed_time as originating_subscribed_time,
        prs_token_transactions.originating_user_type,
        prs_token_transactions.receiving_user_id,
        prs_token_transactions.receiving_user_type
    from 
       prs_token_transactions
    left join subscription
        on prs_token_transactions.originating_user_id = subscription.originating_user_id
),

revenue_union as (
    select 
        balance_transaction as transaction_guid,
        transaction_tstamp,
        transaction_type,
        null as reward_type,
        amount,
        currency,
        originating_user_id,
        originating_subscribed_time,
        originating_user_type,
        receiving_user_id,
        receiving_user_type
    from 
        stripe_revenue
    
    union 

    select 
        transaction_guid,
        tstamp as transaction_tstamp,
        transaction_type,
        reward_type,
        transaction_value as amount,
        currency,
        originating_user_id,
        originating_subscribed_time,
        originating_user_type,
        receiving_user_id,
        receiving_user_type
    from
        token_revenue
),

revenue_subscription as (
    select 
        revenue_union.transaction_guid,
        revenue_union.transaction_tstamp,
        revenue_union.transaction_type,
        revenue_union.reward_type,
        revenue_union.amount,
        revenue_union.currency,
        revenue_union.originating_user_id,
        revenue_union.originating_subscribed_time,
        revenue_union.originating_user_type,
        case 
            when revenue_union.transaction_type = 'plus subscripton' then 1
            when originating_subscription_dates.transaction_type is not null then 1
            else 0
        end as originating_is_plus,
        case 
            when revenue_union.transaction_type = 'pro subscripton' then 1
            when originating_subscription_dates.transaction_type = 'pro subscription' then 1
            else 0
        end as originating_is_pro,
        revenue_union.receiving_user_id,
        receiving_user_type,
        case 
            when receiving_subscription_dates.transaction_type is not null then 1
            else 0
        end as receiving_is_plus,
        case 
            when receiving_subscription_dates.transaction_type = 'pro subscription' then 1
            else 0
        end as receiving_is_pro
    from 
        revenue_union
    left join subscription_dates originating_subscription_dates
        on revenue_union.originating_user_id = originating_subscription_dates.user_id
        and (revenue_union.transaction_tstamp >= originating_subscription_dates.subscription_start_date
        and revenue_union.transaction_tstamp <= originating_subscription_dates.subscription_end_date)
    left join subscription_dates receiving_subscription_dates
        on revenue_union.receiving_user_id = receiving_subscription_dates.user_id
        and (revenue_union.transaction_tstamp >= receiving_subscription_dates.subscription_start_date
        and revenue_union.transaction_tstamp <= receiving_subscription_dates.subscription_end_date)
),

final as (
    select 
        transaction_guid,
        transaction_tstamp,
        transaction_type,
        reward_type,
        amount,
        currency,
        originating_user_id,
        originating_subscribed_time,
        originating_user_type,
        max(originating_is_plus) as originating_is_plus,
        max(originating_is_pro) as originating_is_pro,
        receiving_user_id,
        receiving_user_type,
        max(receiving_is_plus) as receiving_is_plus,
        max(receiving_is_pro) as receiving_is_pro
    from 
        revenue_subscription
    group by
        transaction_guid,
        transaction_tstamp,
        transaction_type,
        reward_type,
        amount,
        currency,
        originating_user_id,
        originating_subscribed_time,
        originating_user_type,
        receiving_user_id,
        receiving_user_type
)

select * from final