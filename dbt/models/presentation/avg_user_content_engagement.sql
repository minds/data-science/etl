with

stg_snowplow_events_userid as (
  select * from {{ ref('stg_snowplow_events_userid') }}
),

stg_activities as (
  select * from {{ ref('stg_activities') }}
),

viewed as (
    select distinct
        view_entity_guid as entity_guid,
        user_id,
        min(collector_tstamp) as first_view_date
    from
        stg_snowplow_events_userid as events
    where
        events.event_name='view'
        and (events.entity_type in ('activity', 'object') or events.entity_type is null)
        and events.view_entity_guid is not null
        and user_id is not null
    group by
        view_entity_guid,
        user_id
),

deleted as (
    select distinct
        entity_guid as deleted_entity_guid
    from
        stg_snowplow_events_userid
    where
        action = 'delete'
        and entity_type = 'activity'
        and entity_guid is not null
),

viewed_undeleted as (
    select
        *
    from
        viewed
        left join deleted on
            viewed.entity_guid = deleted.deleted_entity_guid
    where
        deleted_entity_guid is null
),

engaged as (
    select distinct
        entity_guid,
        user_id,
        min(collector_tstamp) as collector_tstamp
    from
        stg_snowplow_events_userid as events
    where
        events.action in ('vote:up', 'remind', 'comment', 'vote:down')
        and events.entity_type = 'activity'
        and user_id is not null
    group by
        entity_guid,
        user_id

    union

    select distinct
        stg_activities.activity_guid::varchar as entity_guid,
        events.user_id,
        min(events.collector_tstamp) as collector_tstamp

    from
        stg_snowplow_events_userid as events
        inner join stg_activities on
            events.entity_guid = stg_activities.entity_guid
    where
        events.action in ('vote:up', 'remind', 'comment', 'vote:down')
        and events.entity_type = 'object'
        and user_id is not null
    group by
        stg_activities.activity_guid::varchar,
        user_id
),

first_engaged as (
    select
        entity_guid,
        user_id,
        min(collector_tstamp) as first_engaged_date
    from
        engaged
    group by
        entity_guid,
        user_id
),

combined as (
    select
        viewed_undeleted.entity_guid,
        viewed_undeleted.user_id,
        viewed_undeleted.first_view_date,
        first_engaged.first_engaged_date,
        count(distinct first_engaged.user_id) as has_engaged
    from
        viewed_undeleted
        left join first_engaged on
            viewed_undeleted.entity_guid = first_engaged.entity_guid
            and viewed_undeleted.user_id = first_engaged.user_id
    where
        viewed_undeleted.user_id is not null
        and viewed_undeleted.deleted_entity_guid is null

    group by
        viewed_undeleted.entity_guid,
        viewed_undeleted.user_id,
        viewed_undeleted.first_view_date,
        first_engaged.first_engaged_date
),

averaged as (
    select
        user_id,
        date_trunc('week', first_view_date) as week,
        count(*) as new_content_views,
        avg(has_engaged) as avg_engaged
    from
        combined
    group by
        user_id,
        week
)

select * from averaged
