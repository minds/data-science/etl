with 

stripe_charges as (
    select * from {{ source("stripe_extract", "stripe_charges") }}
),

charges as (
    select
        _airbyte_data:id::varchar as charge_id,
        _airbyte_data:created::datetime as transaction_tstamp,
        _airbyte_data:amount::int as amount,
        _airbyte_data:amount_captured::int as amount_captured,
        _airbyte_data:amount_refunded::int as amount_refunded,
        _airbyte_data:status::varchar as status,
        _airbyte_data:customer::varchar as customer_id,
        _airbyte_data:destination::varchar as destination_id,
        _airbyte_data:metadata:user_guid::bigint as user_guid,
        _airbyte_data:metadata:receiver_guid::bigint as receiver_guid,
        _airbyte_data:metadata:sender_guid::bigint as sender_guid,
        _airbyte_data:application_fee_amount::integer as application_fee_amount,
        _airbyte_data:metadata:supermind::bigint as supermind,
        _airbyte_data:metadata:boost_entity_guid::bigint as boost_entity_guid,
        _airbyte_data:metadata:boost_guid::bigint as boost_guid,
        _airbyte_data:metadata:boost_owner_guid::bigint as boost_owner_guid,
        _airbyte_data:metadata:boost_sender_guid::bigint as boost_sender_guid,
        _airbyte_data:metadata:boost_type::varchar as boost_type,
        _airbyte_data:metadata:boost_location::varchar as boost_location,
        _airbyte_data:metadata:impressions::integer as boost_impressions,
        _airbyte_data:metadata:is_manual_transfer::boolean as is_manual_transfer,
        _airbyte_data,
        _airbyte_data:statement_descriptor as statement_descriptor,
        _airbyte_data:calculated_statement_descriptor as calculated_statement_descriptor,
        _airbyte_emitted_at
    from
        stripe_charges
    qualify
        row_number() over (partition by charge_id order by _airbyte_emitted_at desc) = 1
),
charge_types as (
    select
        charges.*,
        case
            when destination_id = 'acct_1Ae0P0GHVmIhiZC8' then 'plus'
            when statement_descriptor = 'Minds: Plus sub' then 'plus'
            when destination_id = 'acct_1FTOqBDHgMdCy9fL' then 'pro'
            when statement_descriptor = 'Minds: Pro sub' then 'pro'
            when boost_guid is not null and boost_type is not null then 'boost_v2'
            when boost_guid is not null and boost_location is not null then 'boost_v3'
            when statement_descriptor = 'Minds: Boost' then 'boost'
            when supermind is not null and customer_id = 'cus_MYhmvxKvtGSFYS' then 'promo_supermind'
            when supermind is not null then 'supermind'
            when statement_descriptor = 'Minds: Supermind' then 'supermind'
            when statement_descriptor = 'Minds: Payment' then 'payment'
            when statement_descriptor = 'Minds: Tip' then 'tip'
            when statement_descriptor = 'Minds: Membership' then 'membership'
            when receiver_guid is not null and user_guid is not null then 'payment'
            when customer_id = 'cus_H5cDc4UqBJOzuP' then 'payout'
            when statement_descriptor = 'MINDS, INC.' or calculated_statement_descriptor = 'MINDS, INC.' then 'payout'
            else 'other'
        end as category
    from
        charges
),
revenue as (
    select
        *,
        case
            when category in ('plus', 'pro', 'boost', 'boost_v2', 'boost_v3', 'other') then amount_captured-amount_refunded
            when category in ('supermind', 'payment', 'tip', 'membership') then application_fee_amount
        end as revenue,
        case
            when category in ('payout', 'promo_supermind') then amount_captured - coalesce(application_fee_amount, 0)
        end as expense,
        case when revenue > 0 then true else false end as is_revenue
    from
        charge_types
),
revenue_history as (
    select
        revenue.*,
        row_number() over (partition by customer_id, category, is_revenue order by transaction_tstamp) as revenue_number
    from
        revenue
),
revenue_history_category as (
    select
        revenue_history.*,
        case
            when category = 'plus' and is_revenue and revenue_number = 1 then 'plus_new'
            when category = 'pro' and is_revenue and revenue_number = 1 then 'pro_new'
            else category
        end as revenue_history_category
    from
        revenue_history
),
by_date as (
    select
        date_trunc(day, transaction_tstamp)::date as date,
        revenue_history_category as category,
        to_number(coalesce(sum(revenue)/100, 0.0), 10, 2) as revenue
    from
        revenue_history_category
    group by date, revenue_history_category
),
pivoted as (
    select
        *
    from
        by_date
        pivot (
            sum(revenue)
            for category in ('plus', 'plus_new', 'pro', 'pro_new', 'boost_v2', 'boost_v3', 'supermind', 'membership', 'tip', 'payment')) as p
            (date, plus, plus_new, pro, pro_new, boost_v2, boost_v3, supermind, membership, tip, payment)
),
zeroed as (
    select
        date,
        coalesce(plus, 0) as plus,
        coalesce(plus_new, 0) as plus_new,
        coalesce(pro, 0) as pro,
        coalesce(pro_new, 0) as pro_new,
        coalesce(boost_v2, 0) as boost_v2,
        coalesce(boost_v3, 0) as boost_v3,
        coalesce(supermind, 0) as supermind,
        coalesce(membership, 0) as membership,
        coalesce(tip, 0) as tip,
        coalesce(payment, 0) as payment
    from
        pivoted
),
totaled as (
    select
        *,
        plus + plus_new + pro + pro_new + boost_v2 + boost_v3 + supermind + membership + tip + payment as total
    from
        zeroed
),
trailing as (
    select
        *,
        to_number(avg(plus) over (order by date rows between 13 preceding and current row), 10, 2) as plus_t14d,
        to_number(avg(plus_new) over (order by date rows between 13 preceding and current row), 10, 2) as plus_new_t14d,
        to_number(avg(pro) over (order by date rows between 13 preceding and current row), 10, 2) as pro_t14d,
        to_number(avg(pro_new) over (order by date rows between 13 preceding and current row), 10, 2) as pro_new_t14d,
        to_number(avg(boost_v2) over (order by date rows between 13 preceding and current row), 10, 2) as boost_v2_t14d,
        to_number(avg(boost_v3) over (order by date rows between 13 preceding and current row), 10, 2) as boost_v3_t14d,
        to_number(avg(supermind) over (order by date rows between 13 preceding and current row), 10, 2) as supermind_t14d,
        to_number(avg(membership) over (order by date rows between 13 preceding and current row), 10, 2) as membership_t14d,
        to_number(avg(tip) over (order by date rows between 13 preceding and current row), 10, 2) as tip_t14d,
        to_number(avg(payment) over (order by date rows between 13 preceding and current row), 10, 2) as payment_t14d,
        to_number(avg(total) over (order by date rows between 13 preceding and current row), 10, 2) as total_t14d
    from
        totaled
)
select * from trailing order by date desc
