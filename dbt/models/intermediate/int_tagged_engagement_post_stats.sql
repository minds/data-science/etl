with
view_stats as (
    select
        entity_guid,
        count(*) as total_views,
        count(case when has_engaged > 0 then 1 end) as total_engagements,
        count(case when has_engaged > 0 and meta_engagement_score <= 0.20 then 1 end) as organic_engagements,
        total_engagements::float/total_views as total_engagement_ratio,
        organic_engagements::float/total_views as organic_engagement_ratio,
        min(last_engaged) as first_engagement,
        max(last_engaged) as last_engagement,
        count(case when int_tagged_engagement.user_id regexp '^[0-9]{18,19}$' then 1 end) as oldid_views,
        count(case when int_tagged_engagement.user_id regexp '^[0-9]{18,19}$' and has_engaged > 0 then 1 end) as oldid_engagements,
        count(case when int_tagged_engagement.user_id regexp '^[0-9]{18,19}$' then null when int_tagged_engagement.user_id regexp '^[0-9a-z]{16,22}' then 1 end) as newid_views,
        count(case when int_tagged_engagement.user_id regexp '^[0-9]{18,19}$' then null when int_tagged_engagement.user_id regexp '^[0-9a-z]{16,22}' and has_engaged > 0 then 1 end) as newid_engagements,
        count(case when art is not null and meta_engagement_score <= 0.20 then 1 end) as tagged_views,
        count(case when art is not null and meta_engagement_score <= 0.20 and has_engaged > 0 then 1 end) as tagged_engagements
    from
        {{ ref('int_tagged_engagement') }}
    inner join {{ ref('int_user_meta_engagement') }} on
        int_tagged_engagement.user_id = int_user_meta_engagement.user_id
    group by
        entity_guid
),

publics as (
    select
        activity_guid::varchar(120) as entity_guid,
        case
            when group_guid = user_guid and access_id = 2 and (is_spam is null or is_spam = false) and (is_deleted is null or is_deleted = false) then true
            else false
        end as is_public
    from
        {{ ref('stg_activities') }}
    where
        valid_to is null
)

select
    view_stats.entity_guid,
    publics.is_public,
    view_stats.total_views,
    view_stats.total_engagements,
    view_stats.organic_engagements,
    view_stats.total_engagement_ratio,
    view_stats.organic_engagement_ratio,
    view_stats.first_engagement,
    view_stats.last_engagement,
    view_stats.oldid_views,
    view_stats.oldid_engagements,
    view_stats.newid_views,
    view_stats.newid_engagements,
    view_stats.tagged_views,
    view_stats.tagged_engagements
from
    view_stats
    left join publics on
        view_stats.entity_guid = publics.entity_guid
