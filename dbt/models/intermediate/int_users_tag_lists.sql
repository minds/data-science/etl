select
    user_guid,
    parse_json(tags) as tag_list

from
    {{ ref('stg_users') }}

where
    valid_to is null
    and tags is not null
    and len(tags) > 0
