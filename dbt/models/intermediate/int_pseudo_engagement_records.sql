with
viewed as (
    select
        view_entity_guid as entity_guid,
        user_id as user_id,
        count(*) as viewed_count,
        min(collector_tstamp) as first_viewed,
        max(collector_tstamp) as last_viewed
    from
        {{ ref('stg_snowplow_events_userid') }} as events
    where
        events.event_name='view'
        and (events.entity_type = 'activity' or entity_type is null)
        and events.view_entity_guid is not null
        and user_id is not null
    group by
        view_entity_guid,
        user_id
),

deleted as (
    select distinct
        entity_guid as deleted_entity_guid
    from
        {{ ref('stg_snowplow_events_userid') }}
    where
        action = 'delete'
        and entity_type = 'activity' or entity_type is null
        and entity_guid is not null
),

activities as (
    select
        activity_guid::varchar(120) as activity_guid,
        entity_guid::varchar(120) as entity_guid,
        user_guid::varchar(120) as channel_guid
    from
        {{ ref('stg_activities') }}
    where
        valid_to is null
),

viewed_undeleted as (
    select
        viewed.*,
        activities.channel_guid,
        deleted.*
    from
        viewed
        inner join activities on
            viewed.entity_guid = activities.activity_guid
        left join deleted on
            viewed.entity_guid = deleted.deleted_entity_guid
),

engaged as (
    select distinct
        events.entity_guid,
        events.user_id,
        events.action,
        events.collector_tstamp
    from
        {{ ref('stg_snowplow_events_userid') }} as events
        inner join activities on
            events.entity_guid = activities.activity_guid
    where
        events.action in ('vote:up', 'remind', 'comment', 'vote:down')
        and (events.entity_type = 'activity' or entity_type is null)
        and user_id is not null

    union

    select distinct
        activities.activity_guid as entity_guid,
        events.user_id,
        events.action,
        events.collector_tstamp

    from
        {{ ref('stg_snowplow_events_userid') }} as events
        inner join activities on
            events.entity_guid = activities.entity_guid
    where
        events.action in ('vote:up', 'remind', 'comment', 'vote:down')
        and events.entity_type = 'object'
        and user_id is not null
),

categorised as (
    select
        viewed_undeleted.entity_guid,
        viewed_undeleted.channel_guid,
        viewed_undeleted.user_id::varchar(120) as user_id,
        case
            when viewed_undeleted.user_id regexp '^[0-9]{18,19}$' then 'user'
            when viewed_undeleted.user_id regexp '^[0-9a-z]{16,22}$' then 'pseudo'
            else 'network'
        end as userid_type,
        viewed_undeleted.viewed_count,
        viewed_undeleted.first_viewed,
        viewed_undeleted.last_viewed,

        count(engaged.action) as engaged_count,
        min(collector_tstamp) as first_engaged,
        max(collector_tstamp) as last_engaged,
        count(case when engaged.action = 'vote:up' then 1 end) as upvote_count,
        count(case when engaged.action = 'remind' then 1 end) as remind_count,
        count(case when engaged.action = 'comment' then 1 end) as comment_count,
        count(case when engaged.action = 'vote:down' then 1 end) as downvote_count
    from
        viewed_undeleted
        left join engaged on
            viewed_undeleted.entity_guid = engaged.entity_guid
            and viewed_undeleted.user_id = engaged.user_id::varchar(120)
    where
        viewed_undeleted.user_id is not null
        and viewed_undeleted.deleted_entity_guid is null

    group by
        viewed_undeleted.entity_guid,
        viewed_undeleted.channel_guid,
        viewed_undeleted.user_id,
        viewed_undeleted.viewed_count,
        viewed_undeleted.first_viewed,
        viewed_undeleted.last_viewed
)

select * from categorised

