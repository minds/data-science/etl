{{
    config (
        materialized='incremental',
        incremental_strategy='merge',
        unique_key='event_id'
    )
}}

select
    event_id,
    count(distinct domain_userid) as domain_id_count,
    count(distinct event_fingerprint) as fingerprint_count,
    min(collector_tstamp) as first,
    max(collector_tstamp) as last,
    count(*) as instances
from
    {{ source('atomic', 'events') }} as events
    cross join {{ ref('int_tstamp_minmax') }} as minmax
where
    events.collector_tstamp > minmax.min
    and events.collector_tstamp <= minmax.max
group by
    event_id
having
    instances > 4
    and timestampdiff('minute', first, last) > 10
