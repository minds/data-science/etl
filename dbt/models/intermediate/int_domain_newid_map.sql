select
    min(case
            when user_id regexp '^[0-9]{18,19}$' then null
            when user_id regexp '^[0-9a-z]{16,22}$' then user_id end) as min_new_userid,
    max(case
            when user_id regexp '^[0-9]{18,19}$' then null
            when user_id regexp '^[0-9a-z]{16,22}$' then user_id end) as max_new_userid,
    max(case when user_id is null then 1 else 0 end) as has_null,
    domain_userid,
    count(distinct event_id) as event_count
from
    {{ ref('stg_snowplow_events') }}
where
    collector_tstamp > '2021-10-21'
group by
    domain_userid
having
    event_count > 1
    and min_new_userid is not null
    and min_new_userid = max_new_userid
    and has_null = 1
