
select
    int_tagged_engagement.*
from
    {{ ref('int_tagged_engagement') }}
    inner join {{ ref('int_tagged_engagement_post_stats') }} on
        int_tagged_engagement.entity_guid = int_tagged_engagement_post_stats.entity_guid
    inner join {{ ref('int_user_meta_engagement') }} on
        int_tagged_engagement.user_id = int_user_meta_engagement.user_id
where
    int_tagged_engagement_post_stats.tagged_engagements > 3
    and int_tagged_engagement_post_stats.tagged_views > int_tagged_engagement_post_stats.tagged_engagements
    and int_user_meta_engagement.meta_engagement_score <= 0.20
    and int_tagged_engagement_post_stats.is_public = true
    and int_tagged_engagement.art is not null
