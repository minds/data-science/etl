with
user_ids as (
    select
        -- min(collector_tstamp) - '1 minute'::interval as first,
        timestampadd('minute', -1, min(collector_tstamp)) as first,
        -- max(collector_tstamp) + '1 minute'::interval as last,
        timestampadd('minute', 1, max(collector_tstamp)) as last,
        user_ipaddress,
        useragent,
        user_id
    from
        {{ ref('stg_snowplow_events') }} as events
    where
        user_id regexp '^[0-9]{18,19}$'
    group by
        user_ipaddress,
        useragent,
        user_id
),
domain_userids as (
    select
        --min(collector_tstamp) - '1 minute'::interval as first,
        timestampadd('minute', -1, min(collector_tstamp)) as first,
        --max(collector_tstamp) + '1 minute'::interval as last,
        timestampadd('minute', 1, max(collector_tstamp)) as last,
        user_ipaddress,
        useragent,
        domain_userid
    from
        {{ ref('stg_snowplow_events') }} as events
    where
        domain_userid is not null
        and user_id is null
    group by
        user_ipaddress,
        useragent,
        domain_userid
)
select
    domain_userid,
    min(user_id) as user_id,
    count(*) as matches
from (
        select
            domain_userids.domain_userid,
            user_ids.user_id
        from
            user_ids
            inner join domain_userids on
                user_ids.user_ipaddress = domain_userids.user_ipaddress
                and user_ids.useragent = domain_userids.useragent
                and user_ids.last > domain_userids.first
                and user_ids.first < domain_userids.last
    ) as matches
group by
    domain_userid
having
    matches = 1