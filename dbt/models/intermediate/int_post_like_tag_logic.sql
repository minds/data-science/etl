with
dmn_activity as (

    select * from {{ ref('dmn_activity') }}
),

posts as (
  select
    entity_guid,
    strtok_to_array(regexp_replace(lower(trim(tags)::string), '\\[|\\]|\\"',''),',') as tags,
    votes_up
  from
  dmn_activity
  where
      tags is not null
      and entity_guid is not null
      and user_guid is not null
)

, flatten_usrs as (

  select
      entity_guid,
      tags,
      array_size(tags) as size,
      user_vote.value::int as user_guid
  from
       posts,
          lateral flatten (input => votes_up) as user_vote
  where 
     size > 0
),

activity_user_tags as
(
  select
    entity_guid,
    tags.value::string as tag,
    user_guid
  from flatten_usrs,lateral flatten(flatten_usrs.tags) tags
  where 
      user_guid is not null
      and 
      entity_guid is not null
),
   
   
cnt as (
  select 
    user_guid,
    tag, 
    count(distinct entity_guid) as times_tag_type_liked_in_post
  from 
      activity_user_tags
  group by
    user_guid,
    tag
  order by 
  times_tag_type_liked_in_post
),

final_tag_lookup as (
select
    distinct
    user_guid,
    tag,
    times_tag_type_liked_in_post
from
    cnt  
where 
    times_tag_type_liked_in_post > 3
)

select * from final_tag_lookup
