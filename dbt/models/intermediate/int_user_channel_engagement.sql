with

int_user_engagement_records as (
    select * from {{ ref('int_user_engagement_records') }}
),

channel_engagement_records as (
    select
        channel_guid,
        user_id,
        sum(engaged_count) as engaged_count,
        min(first_engaged) as first_engaged,
        max(last_engaged) as last_engaged,
        sum(upvote_count) as upvote_count,
        sum(remind_count) as remind_count,
        sum(comment_count) as comment_count,
        sum(downvote_count) as downvote_count
    from
        int_user_engagement_records
    group by
        channel_guid,
        user_id
),

channel_engagement_users as (
    select
        user_id,
        sum(engaged_count) as engaged_count,
        min(first_engaged) as first_engaged,
        max(last_engaged) as last_engaged,
        sum(upvote_count) as upvote_count,
        sum(remind_count) as remind_count,
        sum(comment_count) as comment_count,
        sum(downvote_count) as downvote_count
    from
        channel_engagement_records
    group by
        user_id
),

engagement_log_proportions as (
    select
        *,
        case when upvote_count > 0 then ln(upvote_count) else 0.0 end as log_upvote_count,
        case when remind_count > 0 then ln(remind_count) else 0.0 end as log_remind_count,
        case when comment_count > 0 then ln(comment_count) else 0.0 end as log_comment_count,
        case when downvote_count > 0 then ln(downvote_count) else 0.0 end as log_downvote_count,
        (log_upvote_count + log_remind_count + log_comment_count + log_downvote_count) as log_sum,
        case when log_sum > 0.0 then log_upvote_count / log_sum else 0.25 end as upvotes_proportion,
        case when log_sum > 0.0 then log_remind_count / log_sum else 0.25 end as reminds_proportion,
        case when log_sum > 0.0 then log_comment_count / log_sum else 0.25 end as comments_proportion,
        case when log_sum > 0.0 then log_downvote_count / log_sum else 0.25 end as downvotes_proportion,
        case when upvote_count > 0 then upvotes_proportion / upvote_count else 0 end as upvote_share,
        case when remind_count > 0 then reminds_proportion / remind_count else 0 end as remind_share,
        case when comment_count > 0 then comments_proportion / comment_count else 0 end as comment_share,
        case when downvote_count > 0 then downvotes_proportion / downvote_count else 0 end as downvote_share
    from
        channel_engagement_users
),

user_channel_engagement as (
    select
        c.user_id,
        c.channel_guid,
        c.engaged_count,
        c.first_engaged,
        c.last_engaged,
        (
            c.upvote_count*p.upvote_share +
            c.remind_count*p.remind_share +
            c.comment_count*p.comment_share -
            c.downvote_count*p.downvote_share
        ) as engagement_score
    from
        channel_engagement_records as c
        inner join engagement_log_proportions as p on
            c.user_id = p.user_id
)

select * from user_channel_engagement
