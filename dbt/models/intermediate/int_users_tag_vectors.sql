with 
int_user_tag_lists as (

    select * from {{ ref('int_users_tag_lists') }}
),

comment_tag_list as (
    select * from {{ ref('int_post_like_tag_logic') }}
),
    
    
bio_tags_categorized as (
    
    
    select distinct
        users_tags.user_guid::varchar(25) as user_id,
        
        
        (case when tag.value::varchar ilike 'film' then 1.0 else 0.0 end)::float as film,
        (case when tag.value::varchar ilike 'food' then 1.0 else 0.0 end)::float as food,
        (case when tag.value::varchar ilike 'history' then 1.0 else 0.0 end)::float as history,
        (case when tag.value::varchar ilike 'journalism' then 1.0 else 0.0 end)::float as journalism,
        (case when tag.value::varchar ilike 'minds' then 1.0 else 0.0 end)::float as minds,
        (case when tag.value::varchar ilike 'mindsth' then 1.0 else 0.0 end)::float as mindsth,
        (case when tag.value::varchar ilike 'music' then 1.0 else 0.0 end)::float as music,
        (case when tag.value::varchar ilike 'myphoto' then 1.0 else 0.0 end)::float as myphoto,
        (case when tag.value::varchar ilike 'nutrition' then 1.0 else 0.0 end)::float as nutrition,
        (case when tag.value::varchar ilike 'outdoors' then 1.0 else 0.0 end)::float as outdoors,
        (case when tag.value::varchar ilike 'photography' then 1.0 else 0.0 end)::float as photography,
        (case when tag.value::varchar ilike 'poetry' then 1.0 else 0.0 end)::float as poetry,
        (case when tag.value::varchar ilike 'sports' then 1.0 else 0.0 end)::float as sports,
        (case when tag.value::varchar ilike 'travel' then 1.0 else 0.0 end)::float as travel,
        (case when tag.value::varchar ilike 'videos' then 1.0 else 0.0 end)::float as videos,


        (case when tag.value::varchar ilike 'freespeech' or regexp_like(tag.value::varchar,'.*censor.*|.*freedom.*|.*speech.*', 'i') then 1.0 else 0.0 end)::float as freespeech,
        (case when tag.value::varchar ilike 'politics' or regexp_like(tag.value::varchar,'.*election.*|.*trump|.*biden.*|.*president.*|.*mp.*|.*maga.*|.*blm.*|.*putin.*|.*war.*|.*impeach.*|.*women.*|.*trans.*|.*democrat.*|.*lib.*|.*vote.*.|.*plandemic.*|.*pro choice.*|.*pro life.*|.*abortion.*|.*rights.*|.*law.*', 'i') then 1.0 else 0.0 end)::float as politics,
        (case when tag.value::varchar ilike 'comedy' or regexp_like(tag.value::varchar,'.*funny.*|.*humour.*', 'i') then 1.0 else 0.0 end)::float as comedy,
        (case when tag.value::varchar ilike 'health' or regexp_like(tag.value::varchar,'.*covid.*|.*pandemic.*|.*vacc.*|.*vax.*|.*cannabis.*|.*weight.*', 'i') then 1.0 else 0.0 end)::float as health,
        (case when tag.value::varchar ilike 'memes' or regexp_like(tag.value::varchar,'.*meme.*|.*rick and morty.*|hindudindu|.*gifs.*', 'i') then 1.0 else 0.0 end)::float as memes,
        (case when tag.value::varchar ilike 'gaming' or regexp_like(tag.value::varchar,'.*aura.*|.*fabulous aura.*|.*game.*|.*gaming.*', 'i') then 1.0 else 0.0 end)::float as gaming,
        (case when tag.value::varchar ilike 'news' or regexp_like(tag.value::varchar,'.*news.*|.*truth.*|china|india|russia|france|uk|usa|canada|.*elon.*|.*alex jones.*', 'i') then 1.0 else 0.0 end)::float as news,
        (case when tag.value::varchar ilike 'spirituality' or regexp_like(tag.value::varchar,'.*religion.*|.*philosophy.*|.*jesus.*|.*christian.*|.*jew.*|.*pagan.*|.*islam.*|.*bible.*|.*god.*', 'i') then 1.0 else 0.0 end)::float as spirituality,
        (case when tag.value::varchar ilike 'fashion' or regexp_like(tag.value::varchar,'.*beaut.*|.*outfit.*|.*clothes.*|.*dress.*|.*fashion', 'i') then 1.0 else 0.0 end)::float as fashion,
        (case when tag.value::varchar ilike 'nature' or regexp_like(tag.value::varchar,'.*animals.*|.*cat.*|.*dog.*|.*outdoor.*', 'i') then 1.0 else 0.0 end)::float as nature,
        (case when tag.value::varchar ilike 'technology' or regexp_like(tag.value::varchar,'.*tech.*|.*linux.*|.*apple.*|.*google.*|.*amazon.*|.*tesla.*|.*microsoft.*|.*bill gates.*', 'i')  then 1.0 else 0.0 end)::float as technology,
        (case when tag.value::varchar ilike 'art' or regexp_like(tag.value::varchar,'.*comics.*|.*furry.*|.*drawing.*|.*artist.*|.*painting.*|.*anime.*|.*manga.*|.*cosplay.*|.*pokemon.*|.*animation.*|.*anime.*', 'i') then 1.0 else 0.0 end)::float as art,
        (case when tag.value::varchar ilike 'blockchain' or regexp_like(tag.value::varchar,'.*nft.*|.*smart contract.*|.*token.*', 'i') then 1.0 else 0.0 end)::float as blockchain,
        (case when tag.value::varchar ilike 'education' or regexp_like(tag.value::varchar,'.*book.*|.*read.*|.*learn.*', 'i') then 1.0 else 0.0 end)::float as education,
        (case when tag.value::varchar ilike 'science' or regexp_like(tag.value::varchar,'.*ology.*|.*astro.*|.*physics.*|.*chemist.*|.*engineering.*', 'i') then 1.0 else 0.0 end)::float as science,
        (case when tag.value::varchar ilike 'blog' or regexp_like(tag.value::varchar,'.*writing.*', 'i') then 1.0 else 0.0 end)::float as blog,
        (case when tag.value::varchar ilike 'crypto' or regexp_like(tag.value::varchar,'.*bitcoin.*|.*dogecoin.*|.*ethereum.*|eth|btc', 'i')  then 1.0 else 0.0 end)::float as crypto,

        -- new additions (2 tags: anime and lifestyle)
        (case when tag.value::varchar ilike 'anime' or regexp_like(tag.value::varchar,'.*anime.*|.*manga.*|.*cosplay.*|.*pokemon.*|.*animation.*', 'i') then 1.0 else 0.0 end)::float as anime,
        (case when tag.value::varchar ilike 'lifestyle' or regexp_like(tag.value::varchar,'.*life.*|.*dating.*|.*love.*', 'i') then 1.0 else 0.0 end)::float as lifestyle


    from
        int_users_tag_lists as users_tags,
        lateral flatten(input => users_tags.tag_list) as tag
),

comment_tag_categorized as (

select distinct
        user_guid::varchar(25) as user_id,

        (case when tag::varchar ilike 'film' then 1.0 else 0.0 end)::float as film,
        (case when tag::varchar ilike 'food' then 1.0 else 0.0 end)::float as food,
        (case when tag::varchar ilike 'history' then 1.0 else 0.0 end)::float as history,
        (case when tag::varchar ilike 'journalism' then 1.0 else 0.0 end)::float as journalism,
        (case when tag::varchar ilike 'minds' then 1.0 else 0.0 end)::float as minds,
        (case when tag::varchar ilike 'mindsth' then 1.0 else 0.0 end)::float as mindsth,
        (case when tag::varchar ilike 'music' then 1.0 else 0.0 end)::float as music,
        (case when tag::varchar ilike 'myphoto' then 1.0 else 0.0 end)::float as myphoto,
        (case when tag::varchar ilike 'nutrition' then 1.0 else 0.0 end)::float as nutrition,
        (case when tag::varchar ilike 'outdoors' then 1.0 else 0.0 end)::float as outdoors,
        (case when tag::varchar ilike 'photography' then 1.0 else 0.0 end)::float as photography,
        (case when tag::varchar ilike 'poetry' then 1.0 else 0.0 end)::float as poetry,
        (case when tag::varchar ilike 'sports' then 1.0 else 0.0 end)::float as sports,
        (case when tag::varchar ilike 'travel' then 1.0 else 0.0 end)::float as travel,
        (case when tag::varchar ilike 'videos' then 1.0 else 0.0 end)::float as videos,


        (case when tag::varchar ilike 'freespeech' or regexp_like(tag::varchar,'.*censor.*|.*freedom.*|.*speech.*', 'i') then 1.0 else 0.0 end)::float as freespeech,
        (case when tag::varchar ilike 'politics' or regexp_like(tag::varchar,'.*election.*|.*trump|.*biden.*|.*president.*|.*mp.*|.*maga.*|.*blm.*|.*putin.*|.*war.*|.*impeach.*|.*women.*|.*trans.*|.*democrat.*|.*lib.*|.*vote.*.|.*plandemic.*|.*pro choice.*|.*pro life.*|.*abortion.*|.*rights.*|.*law.*', 'i') then 1.0 else 0.0 end)::float as politics,
        (case when tag::varchar ilike 'comedy' or regexp_like(tag::varchar,'.*funny.*|.*humour.*', 'i') then 1.0 else 0.0 end)::float as comedy,
        (case when tag::varchar ilike 'health' or regexp_like(tag::varchar,'.*covid.*|.*pandemic.*|.*vacc.*|.*vax.*|.*cannabis.*|.*weight.*', 'i') then 1.0 else 0.0 end)::float as health,
        (case when tag::varchar ilike 'memes' or regexp_like(tag::varchar,'.*meme.*|.*rick and morty.*|hindudindu|.*gifs.*', 'i') then 1.0 else 0.0 end)::float as memes,
        (case when tag::varchar ilike 'gaming' or regexp_like(tag::varchar,'.*aura.*|.*fabulous aura.*|.*game.*|.*gaming.*', 'i') then 1.0 else 0.0 end)::float as gaming,
        (case when tag::varchar ilike 'news' or regexp_like(tag::varchar,'.*news.*|.*truth.*|china|india|russia|france|uk|usa|canada|.*elon.*|.*alex jones.*', 'i') then 1.0 else 0.0 end)::float as news,
        (case when tag::varchar ilike 'spirituality' or regexp_like(tag::varchar,'.*religion.*|.*philosophy.*|.*jesus.*|.*christian.*|.*jew.*|.*pagan.*|.*islam.*|.*bible.*|.*god.*', 'i') then 1.0 else 0.0 end)::float as spirituality,
        (case when tag::varchar ilike 'fashion' or regexp_like(tag::varchar,'.*beaut.*|.*outfit.*|.*clothes.*|.*dress.*|.*fashion', 'i') then 1.0 else 0.0 end)::float as fashion,
        (case when tag::varchar ilike 'nature' or regexp_like(tag::varchar,'.*animals.*|.*cat.*|.*dog.*|.*outdoor.*', 'i') then 1.0 else 0.0 end)::float as nature,
        (case when tag::varchar ilike 'technology' or regexp_like(tag::varchar,'.*tech.*|.*linux.*|.*apple.*|.*google.*|.*amazon.*|.*tesla.*|.*microsoft.*|.*bill gates.*', 'i')  then 1.0 else 0.0 end)::float as technology,
        (case when tag::varchar ilike 'art' or regexp_like(tag::varchar,'.*comics.*|.*furry.*|.*drawing.*|.*artist.*|.*painting.*|.*anime.*|.*manga.*|.*cosplay.*|.*pokemon.*|.*animation.*|.*anime.*', 'i') then 1.0 else 0.0 end)::float as art,
        (case when tag::varchar ilike 'blockchain' or regexp_like(tag::varchar,'.*nft.*|.*smart contract.*|.*token.*', 'i') then 1.0 else 0.0 end)::float as blockchain,
        (case when tag::varchar ilike 'education' or regexp_like(tag::varchar,'.*book.*|.*read.*|.*learn.*', 'i') then 1.0 else 0.0 end)::float as education,
        (case when tag::varchar ilike 'science' or regexp_like(tag::varchar,'.*ology.*|.*astro.*|.*physics.*|.*chemist.*|.*engineering.*', 'i') then 1.0 else 0.0 end)::float as science,
        (case when tag::varchar ilike 'blog' or regexp_like(tag::varchar,'.*writing.*', 'i') then 1.0 else 0.0 end)::float as blog,
        (case when tag::varchar ilike 'crypto' or regexp_like(tag::varchar,'.*bitcoin.*|.*dogecoin.*|.*ethereum.*|eth|btc', 'i')  then 1.0 else 0.0 end)::float as crypto,

        -- new additions (2 tags: anime and lifestyle)
        (case when tag::varchar ilike 'anime' or regexp_like(tag::varchar,'.*anime.*|.*manga.*|.*cosplay.*|.*pokemon.*|.*animation.*', 'i') then 1.0 else 0.0 end)::float as anime,
        (case when tag::varchar ilike 'lifestyle' or regexp_like(tag::varchar,'.*life.*|.*dating.*|.*love.*', 'i') then 1.0 else 0.0 end)::float as lifestyle

from
    comment_tag_list
),

unioned as (
    select * from bio_tags_categorized
        union all
    select * from comment_tag_categorized
),

agg_up as (

select 
        user_id,
        max(film) as film,
        max(food) as food,
        max(history) as history,
        max(journalism) as journalism,
        max(minds) as minds,
        max(mindsth) as mindsth,
        max(music) as music,
        max(myphoto) as myphoto,
        max(nutrition) as nutrition,
        max(outdoors) as outdoors,
        max(photography) as photography,
        max(poetry) as poetry,
        max(sports) as sports,
        max(travel) as travel,
        max(videos) as videos,
        max(freespeech) as freespeech,
        max(politics) as politics,
        max(comedy) as comedy,
        max(health) as health,
        max(memes) as memes,
        max(gaming) as gaming,
        max(news) as news,
        max(spirituality) as spirituality,
        max(fashion) as fashion,
        max(nature) as nature,
        max(technology) as technology,
        max(art) as art,
        max(blockchain) as blockchain,
        max(education) as education,
        max(science) as science,
        max(blog) as blog,
        max(crypto) as crypto,
        -- new additions (2 tags: anime and lifestyle)
        max(anime) as anime,
        max(lifestyle) as lifestyle
from 
    unioned
group by user_id
)

select
    user_id,
    art,blockchain,blog,comedy,crypto,education,fashion,film,food,freespeech,gaming,health,history,journalism,memes,minds,mindsth,
    music,myphoto,nature,news,nutrition,outdoors,photography,poetry,politics,science,spirituality,sports,technology,travel,videos
from agg_up
