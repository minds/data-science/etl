with

dmn_activity as (
  select * from {{ ref('dmn_activity') }}
),

dmn_comments as (
  select * from {{ ref('dmn_comments') }}
),

dmn_users as (
  select * from {{ ref('dmn_users') }}
),

combined as (
select
    dmn_activity.activity_guid,
    dmn_activity.time_created,
    dmn_activity.user_guid as owner_guid,
    t.value::string as engager_guid,
    'vote_up' as engagement_type
from
    dmn_activity,
    Table(Flatten(dmn_activity.votes_up))t

union

select
    dmn_activity.activity_guid,
    dmn_activity.time_created,
    dmn_activity.user_guid as owner_guid,
    dmn_comments.user_guid::string as engager_guid,
    'comment' as engagement_type
from
    dmn_activity
inner join
    dmn_comments
        on dmn_activity.activity_guid::string = dmn_comments.entity_guid::string
union

select
    reminds.remind_guid,
    dmn_activity.time_created,
    dmn_activity.user_guid as owner_guid,
    reminds.user_guid::string as engager_guid,
    'remind' as engagement_type
from
    dmn_activity as reminds
inner join
    dmn_activity
        on reminds.remind_guid::string = dmn_activity.activity_guid::string
),

user_tf_types as (
select
    combined.activity_guid,
    combined.time_created,
    combined.owner_guid,
    combined.engager_guid,
    combined.engagement_type,
    owner.user_type as owner_user_type,
    engager.user_type as engager_user_type
from
    combined
left join
    dmn_users as owner
        on combined.owner_guid = owner.user_guid
left join
    dmn_users as engager
        on combined.engager_guid = engager.user_guid
)

select * from user_tf_types
