with

dmn_comments as (
    select * from {{ ref('dmn_comments') }}
),

stg_users as (
    select * from {{ ref('stg_users') }} where valid_to is null
),

dmn_activity as (
    select * from {{ ref('dmn_activity') }}
),


combined as (
    select
        user_guid::string as user_guid,
        'email_verified' as event_action,
        email_confirmed_at as event_tstamp
    from
        stg_users
    where email_confirmed_at is not null

    union

    select
        user_guid::string as user_guid,
        case
            when comment_link is null then 'comment'
            when comment_link is not null then 'comment with link'
        end as event_action,
        time_created as event_tstamp
    from
        dmn_comments

    union

    select
        user_guid::string as user_guid,
        case
            when is_remind = 1 then 'remind'
            when is_remind = 0 then 'activity'
        end as event_action,
        time_created as event_tstamp
    from
        dmn_activity

    union

    select
        originating_user_id::string as user_guid,
        concat(transaction_type, ' - ', currency, ' sent') as event_action,
        transaction_tstamp as event_tstamp
    from
        prs_revenue
    where originating_user_id is not null
    and originating_user_id not ilike '%Minds%'
    and originating_user_id not ilike '%cust%'

    union

    select
        receiving_user_id::string as user_guid,
        concat(transaction_type, ' - ', currency, ' received') as event_action,
        transaction_tstamp as event_tstamp
    from
       prs_revenue
    where receiving_user_id is not null
    and receiving_user_id not ilike '%Minds%'
    and receiving_user_id not ilike '%acc%'
),

user_events as (
    select
        combined.user_guid,
        stg_users.time_created as user_created,
        combined.event_action,
        combined.event_tstamp,
        row_number() over (partition by combined.user_guid order by combined.event_tstamp) as event_number
    from
        combined
    left join stg_users
        on combined.user_guid = stg_users.user_guid
    group by
        combined.user_guid,
        user_created,
        event_action,
        event_tstamp
),

aggregated as (
    select
        user_guid,
        user_created,
        event_action,
        count(*) as event_count,
        min(datediff('minutes', user_created, event_tstamp)) as mins_to_first,
        min(event_number) as min_sequence
    from
        user_events
    group by
        user_guid,
        user_created,
        event_action
)

select * from aggregated
