with

events as (
    select * from {{ source('elasticsearch', 'engagement_events') }} 
),

activities as (
    select * from {{ ref('stg_activities') }}
),

engaged as (
    select distinct
        events.user_guid as user_id,
        events.entity_guid::number(38) as entity_guid,
        activities.user_guid as channel_guid,
        activities.access_id,
        events.action,
        events.timestamp
    from
        events
        inner join activities on
            events.entity_guid = activities.activity_guid
    where
        events.action in ('vote:up', 'remind', 'comment', 'vote:down')
        and (events.entity_type = 'activity' or entity_type is null)
        and events.user_guid is not null

    union

    select distinct
        events.user_guid as user_id,
        activities.activity_guid::number(38) as entity_guid,
        activities.user_guid as channel_guid,
        activities.access_id,
        events.action,
        events.timestamp

    from
        events
        inner join activities on
            events.entity_guid = activities.entity_guid
    where
        events.action in ('vote:up', 'remind', 'comment', 'vote:down')
        and events.entity_type = 'object'
        and events.user_guid is not null
),

categorised as (
    select
        user_id,
        entity_guid,
        channel_guid,
        access_id,

        count(action) as engaged_count,
        min(timestamp) as first_engaged,
        max(timestamp) as last_engaged,
        count(case when action = 'vote:up' then 1 end) as upvote_count,
        count(case when action = 'remind' then 1 end) as remind_count,
        count(case when action = 'comment' then 1 end) as comment_count,
        count(case when action = 'vote:down' then 1 end) as downvote_count
    from
        engaged

    group by
        user_id,
        entity_guid,
        channel_guid,
        access_id
)

select * from categorised

