with
user_tags_extract as (
    select * from {{ source("cassandra_extract", "pseudo_user_hashtags") }}
),

last_extract as (
    select
        pseudo_id,
        max(extracted_at) as last_extract
    from
        user_tags_extract
    group by
        pseudo_id
),

user_tags as (
    select
        user_tags_extract.pseudo_id as user_id,
        user_tags_extract.hashtag
    from
        user_tags_extract
        inner join last_extract on
            user_tags_extract.pseudo_id = last_extract.pseudo_id
            and user_tags_extract.extracted_at = last_extract.last_extract
),

users_tag_vectors as (
    select
        user_tags.user_id,
        max(case when user_tags.hashtag = 'film' then 1.0 else 0.0 end)::float as film,
        max(case when user_tags.hashtag = 'food' then 1.0 else 0.0 end)::float as food,
        max(case when user_tags.hashtag = 'history' then 1.0 else 0.0 end)::float as history,
        max(case when user_tags.hashtag = 'journalism' then 1.0 else 0.0 end)::float as journalism,
        max(case when user_tags.hashtag = 'minds' then 1.0 else 0.0 end)::float as minds,
        max(case when user_tags.hashtag = 'mindsth' then 1.0 else 0.0 end)::float as mindsth,
        max(case when user_tags.hashtag = 'music' then 1.0 else 0.0 end)::float as music,
        max(case when user_tags.hashtag = 'myphoto' then 1.0 else 0.0 end)::float as myphoto,
        max(case when user_tags.hashtag = 'nutrition' then 1.0 else 0.0 end)::float as nutrition,
        max(case when user_tags.hashtag = 'outdoors' then 1.0 else 0.0 end)::float as outdoors,
        max(case when user_tags.hashtag = 'photography' then 1.0 else 0.0 end)::float as photography,
        max(case when user_tags.hashtag = 'poetry' then 1.0 else 0.0 end)::float as poetry,
        max(case when user_tags.hashtag = 'sports' then 1.0 else 0.0 end)::float as sports,
        max(case when user_tags.hashtag = 'travel' then 1.0 else 0.0 end)::float as travel,
        max(case when user_tags.hashtag = 'videos' then 1.0 else 0.0 end)::float as videos,
        max(case when user_tags.hashtag = 'freespeech' then 1.0 else 0.0 end)::float as freespeech,
        max(case when user_tags.hashtag = 'politics' then 1.0 else 0.0 end)::float as politics,
        max(case when user_tags.hashtag = 'comedy' then 1.0 else 0.0 end)::float as comedy,
        max(case when user_tags.hashtag = 'health' then 1.0 else 0.0 end)::float as health,
        max(case when user_tags.hashtag = 'memes' then 1.0 else 0.0 end)::float as memes,
        max(case when user_tags.hashtag = 'gaming' then 1.0 else 0.0 end)::float as gaming,
        max(case when user_tags.hashtag = 'news' then 1.0 else 0.0 end)::float as news,
        max(case when user_tags.hashtag = 'spirituality' then 1.0 else 0.0 end)::float as spirituality,
        max(case when user_tags.hashtag = 'fashion' then 1.0 else 0.0 end)::float as fashion,
        max(case when user_tags.hashtag = 'nature' then 1.0 else 0.0 end)::float as nature,
        max(case when user_tags.hashtag = 'technology' then 1.0 else 0.0 end)::float as technology,
        max(case when user_tags.hashtag = 'art' then 1.0 else 0.0 end)::float as art,
        max(case when user_tags.hashtag = 'blockchain' then 1.0 else 0.0 end)::float as blockchain,
        max(case when user_tags.hashtag = 'education' then 1.0 else 0.0 end)::float as education,
        max(case when user_tags.hashtag = 'science' then 1.0 else 0.0 end)::float as science,
        max(case when user_tags.hashtag = 'blog' then 1.0 else 0.0 end)::float as blog,
        max(case when user_tags.hashtag = 'crypto' then 1.0 else 0.0 end)::float as crypto,
        max(case when user_tags.hashtag = 'anime' then 1.0 else 0.0 end)::float as anime,
        max(case when user_tags.hashtag = 'lifestyle' then 1.0 else 0.0 end)::float as lifestyle
    from
        user_tags
    group by
        user_id
)

select
    user_id,
    art,blockchain,blog,comedy,crypto,education,fashion,film,food,freespeech,gaming,health,history,journalism,memes,minds,mindsth,
    music,myphoto,nature,news,nutrition,outdoors,photography,poetry,politics,science,spirituality,sports,technology,travel,videos
from users_tag_vectors