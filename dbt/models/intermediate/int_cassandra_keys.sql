{{
    config(
        materialized='view'
    )
}}

with
minmax as (
    select
        min.min,
        max.max
    from
        (select max(extracted_at) as min from  {{ source('cassandra_extract', 'activity') }} ) as min,
        (select max(collector_tstamp) as max from  {{ source('atomic', 'events') }} ) as max
),

events as (
    select
        parse_json(contexts):data as contexts_json,
        parse_json(unstruct_event):data.data as event_props
    from
        {{ source('atomic', 'events') }}
        cross join minmax
    where
        events.COLLECTOR_TSTAMP > minmax.min
        and events.COLLECTOR_TSTAMP <= minmax.max
),

guids as (
    select
        event_props:entity_guid as view_entity_guid,
        event_props:entity_owner_guid as view_entity_owner_guid,
        case
            when contexts_json[0].schema = 'iglu:com.minds/entity_context/jsonschema/1-0-0' then contexts_json[0].data
            when contexts_json[1]['schema'] = 'iglu:com.minds/entity_context/jsonschema/1-0-0' then contexts_json[1]['data']
            when contexts_json[2]['schema'] = 'iglu:com.minds/entity_context/jsonschema/1-0-0' then contexts_json[2]['data']
            when contexts_json[3]['schema'] = 'iglu:com.minds/entity_context/jsonschema/1-0-0' then contexts_json[3]['data']
        end as entity_data,
        entity_data:entity_guid::varchar(20) as entity_guid,
        entity_data:entity_owner_guid::varchar(20) as entity_owner_guid,
        entity_data:entity_container_guid::varchar(20) as entity_container_guid,
        entity_data:entity_access_id::varchar(20) as entity_access_id
    from
        events
)
      select entity_guid as key from guids where entity_guid is not null
union select distinct entity_owner_guid as key from guids where entity_owner_guid is not null
union select distinct entity_access_id as key from guids where entity_access_id is not null
union select distinct entity_container_guid as key from guids where entity_container_guid is not null
union select distinct view_entity_guid as key from guids where view_entity_guid is not null
union select distinct view_entity_owner_guid as key from guids where view_entity_owner_guid is not null

