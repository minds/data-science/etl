select
    min.min,
    max.max
from
    -- Identify the processing range between the last dbt run and the current Snowplow high-water mark
    -- Fake the "A" in DAG (otherwise, with a 'ref()', dbt complains about the cyclical dependency):
    (select max(collector_tstamp) as min from  {{schema}}.stg_snowplow_events) as min,
    (select max(collector_tstamp) as max from  {{ source('atomic', 'events') }} ) as max
    --(select '2021-11-01'::timestamp as min) as min,
    --(select '2021-12-01'::timestamp as max) as max
