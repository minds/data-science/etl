{{
    config(
        sort='collector_tstamp'
    )
}}

with
old_new_id_map as (
    select
        min_old_userid as old_userid,
        min(min_new_userid) as new_userid
    from
        {{ ref('int_network_id_map') }}
    where
        min_old_userid is not null
        and min_new_userid is not null
    group by
        old_userid
)

select 
    events.collector_tstamp,
    events.event_id,
    case
        when
            events.user_id is not null
            and events.user_id <> 'null'
        then
            coalesce(old_new_id_map.new_userid, events.user_id)
        else
            coalesce(
                network_id_map.min_new_userid,
                domain_newid_map.min_new_userid,
                network_id_map.min_old_userid,
                old_new_domainid_map.new_userid,
                domain_oldid_map.user_id,
                network_id_map.network_userid,
                events.domain_userid
            )
    end as user_identifier

from
    {{ ref('stg_snowplow_events') }} as events
    cross join {{ ref('int_tstamp_minmax') }} as minmax
    left join {{ ref('int_domain_userid_map') }} as domain_oldid_map on
        events.domain_userid = domain_oldid_map.domain_userid
    left join {{ ref('int_domain_newid_map') }} as domain_newid_map on
        events.domain_userid = domain_newid_map.domain_userid
    left join {{ ref('int_network_id_map') }} as network_id_map on
        events.network_userid = network_id_map.network_userid
    left join old_new_id_map on
        events.user_id = old_new_id_map.old_userid
    left join old_new_id_map as old_new_domainid_map on
        domain_oldid_map.user_id = old_new_domainid_map.old_userid

where
    (events.user_id is null or events.user_id='null' or events.user_id regexp '^[0-9]{18,19}$')
    and events.collector_tstamp > minmax.min
    and events.collector_tstamp < minmax.max