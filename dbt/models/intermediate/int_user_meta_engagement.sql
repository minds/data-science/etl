
with
engagements as (
    select
        int_tagged_engagement.user_id,
        int_tagged_engagement.has_engaged,
        int_tagged_engagement_stats.is_public,
        int_tagged_engagement_stats.total_views,
        int_tagged_engagement_stats.total_engagement_ratio
    from
        {{ ref('int_tagged_engagement') }}
        left join {{ ref('int_tagged_engagement_stats') }} on
            int_tagged_engagement.entity_guid = int_tagged_engagement_stats.entity_guid
    where
        int_tagged_engagement_stats.total_views >= 1
        and int_tagged_engagement.has_engaged > 0
)

select
    user_id,
    median(total_engagement_ratio) as meta_engagement_score,
    count(*) as engaged_entity_count
from
    engagements
group by
    user_id
having
    engaged_entity_count >= 1

