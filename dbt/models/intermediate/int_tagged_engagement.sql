with
viewed as (
    select distinct
        view_entity_guid as entity_guid,
        user_id
    from
        {{ ref('stg_snowplow_events_userid') }} as events
    where
        events.event_name='view'
        and (events.entity_type = 'activity' or entity_type is null)
        and events.view_entity_guid is not null
        and user_id is not null
),

deleted as (
    select distinct
        entity_guid as deleted_entity_guid
    from
        {{ ref('stg_snowplow_events_userid') }}
    where
        action = 'delete'
        and entity_type = 'activity' or entity_type is null
        and entity_guid is not null
),

activities as (
    select
        activity_guid::varchar(120) as activity_guid,
        entity_guid::varchar(120) as entity_guid
    from
        {{ ref('stg_activities') }}
    where
        (nsfw = '[]'
        or nsfw is null)
        and valid_to is null
),

viewed_undeleted as (
    select
        viewed.*,
        deleted.*
    from
        viewed
        inner join activities on
            viewed.entity_guid = activities.activity_guid
        left join deleted on
            viewed.entity_guid = deleted.deleted_entity_guid
),

engaged as (
    select distinct
        events.entity_guid,
        events.user_id,
        events.collector_tstamp
    from
        {{ ref('stg_snowplow_events_userid') }} as events
        inner join activities on
            events.entity_guid = activities.activity_guid
    where
        events.action in ('vote:up', 'remind', 'comment', 'vote:down')
        and (events.entity_type = 'activity' or entity_type is null)
        and user_id is not null

    union

    select distinct
        activities.activity_guid as entity_guid,
        events.user_id,
        events.collector_tstamp

    from
        {{ ref('stg_snowplow_events_userid') }} as events
        inner join activities on
            events.entity_guid = activities.entity_guid
    where
        events.action in ('vote:up', 'remind', 'comment', 'vote:down')
        and events.entity_type = 'object'
        and user_id is not null
),

user_vectors as (
    select * from {{ ref('int_users_tag_vectors') }}
    union all
    select * from {{ ref('int_pseudo_users_tag_vectors') }}
),

categorised as (
    select
        viewed_undeleted.entity_guid,
        viewed_undeleted.user_id::varchar(120) as user_id,
        max(collector_tstamp) as last_engaged,
        count(distinct engaged.user_id) as has_engaged
    from
        viewed_undeleted
        left join engaged on
            viewed_undeleted.entity_guid = engaged.entity_guid
            and viewed_undeleted.user_id = engaged.user_id::varchar(120)
    where
        viewed_undeleted.user_id is not null
        and viewed_undeleted.deleted_entity_guid is null

    group by
        viewed_undeleted.entity_guid,
        viewed_undeleted.user_id
)

select
    categorised.*,
    art,blockchain,blog,comedy,crypto,education,fashion,film,food,freespeech,gaming,health,history,journalism,memes,minds,mindsth,
    music,myphoto,nature,news,nutrition,outdoors,photography,poetry,politics,science,spirituality,sports,technology,travel,videos
from
    categorised
    left join user_vectors on
        categorised.user_id = user_vectors.user_id
