select
    min(case when user_id regexp '^[0-9]{18,19}$' then user_id end) as min_old_userid,
    max(case when user_id regexp '^[0-9]{18,19}$' then user_id end) as max_old_userid,
    min(case when user_id regexp '^[0-9]{18,19}$' then null when user_id regexp '^[0-9a-z]{16,22}$' then user_id end) as min_new_userid,
    max(case when user_id regexp '^[0-9]{18,19}$' then null when user_id regexp '^[0-9a-z]{16,22}$' then user_id end) as max_new_userid,
    max(case when user_id is null then 1 else 0 end) as has_null,
    min(collector_tstamp) as min_tstamp,
    max(collector_tstamp) as max_tstamp,
    network_userid,
    count(distinct event_id) as event_count
from
    {{ ref('stg_snowplow_events') }}
where
    collector_tstamp > '2021-10-21'
group by
    network_userid
having
    event_count > 2
    and (min_old_userid is null or min_old_userid = max_old_userid)
    and (min_new_userid is null or min_new_userid = max_new_userid)