with

blockchain_transactions_mainnet as (
    select * from {{ source("cassandra_extract", "blockchain_transactions_mainnet") }}
),

blockchain_transactions as (
    select
        tx as transaction_guid,
        user_guid::string as user_guid,
        max(timestamp) as tstamp,
        wallet_address,
        amount,
        completed as is_completed,
        contract as transaction_type,
        PARSE_JSON(data) as transaction_data,
        failed as is_failed,
        min(extracted_at) as valid_from
    from
        blockchain_transactions_mainnet
    group by
        transaction_guid,
        user_guid,
        wallet_address,
        amount,
        is_completed,
        transaction_type,
        transaction_data,
        is_failed
),

final as (
    select
        *,
        lead(valid_from, 1) over (
            partition by transaction_guid
            order by valid_from
        ) as valid_to
    from
        blockchain_transactions
)

select * from final
