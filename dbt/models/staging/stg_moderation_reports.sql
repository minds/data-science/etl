with

moderation_reports as (
    select * from {{ source("cassandra_extract", "moderation_reports") }}
),

json_logic as (
    select 
        entity_urn,
        split_part(entity_urn, ':', 2) as entity_type,
        entity_owner_guid,
        reason_code, 
        sub_reason_code,
        state as current_report_state,
        timestamp as reported_tstamp,
        state_changes:initial_jury_decided::timestamp as initial_jury_decided_tstamp,
        state_changes:appealed::timestamp as appealed_tstamp,
        state_changes:appeal_jury_decided::timestamp as appeal_jury_decided_tstamp,
        reports,
        replace(initial_jury::varchar, '"', '') as initial_jury,
        appeal_jury::varchar as appeal_jury,
        appeal_note,
        uphold as is_upheld,
        user_hashes,
        extracted_at
    from 
        moderation_reports
),

stg_moderation_reports as (
    select 
        case 
            when entity_type = 'comment' then split_part(entity_urn, ':', 7)::int 
            else split_part(entity_urn, ':', 3)::int
        end as entity_urn,
        entity_type,
        entity_owner_guid,
        reason_code, 
        sub_reason_code,
        current_report_state,
        reported_tstamp,
        initial_jury_decided_tstamp,
        appealed_tstamp,
        appeal_jury_decided_tstamp,
        reports,
        replace(split_part(initial_jury, ':', 1), '{', '') as initial_jury,
        replace(split_part(initial_jury, ':', 2), '}', '') as initial_jury_verdict,
        appeal_jury,
        regexp_count(appeal_jury::varchar, 'true', 1) as appeal_true_count,
        regexp_count(appeal_jury::varchar, 'false', 1) as appeal_false_count,
        case 
            when appeal_true_count / (appeal_true_count + appeal_false_count) >= 0.75 then 1
            else 0
        end as is_appealed,
        appeal_note,
        is_upheld,
        user_hashes,
        min(extracted_at) as valid_from
    from 
        json_logic
    group by 
        entity_urn,
        entity_type,
        entity_owner_guid,
        reason_code, 
        sub_reason_code,
        current_report_state,
        reported_tstamp,
        initial_jury_decided_tstamp,
        appealed_tstamp,
        appeal_jury_decided_tstamp,
        reports,
        initial_jury,
        initial_jury_verdict,
        appeal_jury,
        appeal_true_count,
        appeal_false_count,
        is_appealed,
        appeal_note,
        is_upheld,
        user_hashes
),

final as (
    select
        *,
        lead(valid_from, 1) over (
            partition by entity_urn
            order by valid_from
        ) as valid_to
    from
       stg_moderation_reports
)

select * from final