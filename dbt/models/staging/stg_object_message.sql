with

object_message as (
  select * from {{ source("cassandra_extract", "object_message") }}
),

message as (
    select
        key as entity_guid,
        owner_guid as user_guid,
        container_guid as group_guid,
        access_id,
        enabled as is_enabled,
        time_created,
        time_updated,
        min(extracted_at) as valid_from
    from
        object_message
    group by
        entity_guid,
        user_guid,
        group_guid,
        access_id,
        is_enabled,
        time_created,
        time_updated
),

final as (
    select
        *,
        lead(valid_from,1) over (
            partition by user_guid
            order by valid_from
        ) as valid_to
    from
        message
)

select * from final
