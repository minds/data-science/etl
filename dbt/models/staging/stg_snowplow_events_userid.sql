{{
    config (
        materialized='incremental',
        incremental_strategy='merge',
        unique_key='event_id'
    )
}}

select
    events.event_id,
    events.collector_tstamp,
    events.derived_tstamp,
    events.event,
    events.event_name,
    events.user_ipaddress,
    coalesce(int_event_userid_map.user_identifier, events.user_id) as user_id,
    events.network_userid,
    events.domain_userid,
    events.platform,
    events.useragent,
    events.br_name,
    events.br_lang,
    events.os_name,
    events.dvce_type,
    events.page_url,
    events.page_referrer,
    events.mkt_medium,
    events.mkt_source,
    events.mkt_term,
    events.mkt_content,
    events.mkt_campaign,
    events.domain_sessionid,
    events.domain_sessionidx,
    events.action,
    events.boost_rating,
    events.boost_reject_reason,
    events.comment_guid,
    events.entity_guid,
    events.entity_owner_guid,
    events.entity_type,
    events.entity_subtype,
    events.entity_container_guid,
    events.entity_access_id,
    events.view_campaign,
    events.view_delta,
    events.view_medium,
    events.view_platform,
    events.view_position,
    events.view_entity_guid,
    events.view_entity_owner_guid,
    events.boost_prompt,
    events.boost_rotator,
    events.channel_gallery

from
    {{ ref('stg_snowplow_events') }} as events
    cross join {{ ref('int_tstamp_minmax') }} as minmax
    left join {{ ref('int_event_userid_map') }} on
        events.event_id = int_event_userid_map.event_id
        and events.collector_tstamp = int_event_userid_map.collector_tstamp

where
    events.collector_tstamp > minmax.min
    and events.collector_tstamp < minmax.max