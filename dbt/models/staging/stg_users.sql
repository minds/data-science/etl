with

user as (
  select * from {{ source("cassandra_extract", "user") }}
),

cassandra_users as (
    select
        key::varchar(24) as user_guid,
        username,
        time_created,
        email_confirmed_at,
        language,
        dob,
        gender,
        ip,
        verified as is_verifed,
        admin as is_admin,
        enabled as is_enabled,
        banned as is_banned,
        deleted as is_deleted,
        plus as is_plus,
        rating,
        founder as is_founder,
        pro_expires,
        pro_published as is_pro_published,
        last_login,
        ban_monetization as is_ban_monetization,
        boost_autorotate as is_boost_autorotate,
        boost_rating,
        categories,
        disabled_boost as is_disabled_boost,
        eth_incentive,
        eth_wallet,
        group_membership,
        coalesce(mature, is_mature) as is_mature,
        mature_content,
        mature_lock,
        mode,
        monetization_settings,
        onboarding_shown as is_onboarding_shown,
        pinned_posts,
        programs,
        spam as is_spam,
        briefdescription,
        tags,
        "signupParentId" as signupparentid,
        "signupPreviousUrl" as signuppreviousurl,
        min(extracted_at) as valid_from
    from
        user
    group by
        user_guid,
        username,
        time_created,
        email_confirmed_at,
        language,
        dob,
        gender,
        ip,
        verified,
        admin,
        enabled,
        banned,
        deleted,
        plus,
        rating,
        founder,
        pro_expires,
        pro_published,
        last_login,
        mature,
        ban_monetization,
        boost_autorotate,
        boost_rating,
        categories,
        disabled_boost,
        eth_incentive,
        eth_wallet,
        group_membership,
        is_mature,
        mature_content,
        mature_lock,
        mode,
        monetization_settings,
        onboarding_shown,
        pinned_posts,
        programs,
        spam,
        briefdescription,
        tags,
        signupparentid,
        signuppreviousurl
),

final as (
    select
        *,
        lead(valid_from,1) over (
            partition by user_guid
            order by valid_from
        ) as valid_to
    from
        cassandra_users
)

select * from final
