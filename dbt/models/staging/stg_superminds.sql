with superminds_valid_from as (
    select
        guid,
        activity_guid,
        reply_activity_guid,
        sender_guid,
        receiver_guid,
        status as status_code,
        case
            when status = 0 then 'PENDING'
            when status = 1 then 'CREATED'
            when status = 2 then 'ACCEPTED'
            when status = 3 then 'REVOKED'
            when status = 4 then 'REJECTED'
            when status = 5 then 'FAILED_PAYMENT'
            when status = 6 then 'FAILED'
            when status = 7 then 'EXPIRED'
        end as status,
        payment_amount,
        case when payment_method is null then 0 else payment_method end as payment_method_code,
        case
            when payment_method is null or payment_method = 0 then 'CASH'
            when payment_method = 1 then 'OFFCHAIN_TOKEN'
        end as payment_method,
        payment_reference,
        created_timestamp,
        updated_timestamp,
        twitter_required,
        case when reply_type is null then 0 else reply_type end as reply_type_code,
        case
            when reply_type is null or reply_type = 0 then 'TEXT'
            when reply_type = 1 then 'IMAGE'
            when reply_type = 2 then 'VIDEO'
        end as reply_type,
        min(extracted_at) as _valid_from
    from
        vitess_extract.superminds
    group by
        guid,
        activity_guid,
        reply_activity_guid,
        sender_guid,
        receiver_guid,
        status,
        payment_amount,
        payment_method,
        payment_reference,
        created_timestamp,
        updated_timestamp,
        twitter_required,
        reply_type
),
superminds_valid_to as (
    select
        *,
        lead(_valid_from) over (partition by guid order by _valid_from ) as _valid_to
    from
        superminds_valid_from
)
select * from superminds_valid_to 
