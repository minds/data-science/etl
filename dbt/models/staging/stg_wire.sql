with

wire as (
    select * from {{ source("cassandra_extract", "wire") }}
),

stg_wire as (
    select
        wire_guid,
        entity_guid,
        sender_guid,
        receiver_guid,
        method,
        timestamp as tstamp,
        amount,
        recurring as is_recurring,
        status as wire_status,
        wei,
        min(extracted_at) as valid_from
    from
        wire
    group by
        wire_guid,
        entity_guid,
        sender_guid,
        receiver_guid,
        method,
        tstamp,
        amount,
        is_recurring,
        wire_status,
        wei
),

final as (
    select
        *,
        lead(valid_from, 1) over ( 
            partition by wire_guid
            order by valid_from
        ) as valid_to
    from
        stg_wire
)

select * from final