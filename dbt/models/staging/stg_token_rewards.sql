with

token_rewards as (
    select * from {{ source("cassandra_extract", "token_rewards") }}
),

stg_token_rewards as (
    select
        user_guid,
        reward_type,
        date as tstamp,
        multiplier,
        payout_tx as payout_transaction,
        score,
        token_amount,
        tokenomics_version,
        min(extracted_at) as valid_from
    from
        token_rewards
    group by
        user_guid,
        reward_type,
        tstamp,
        multiplier,
        payout_transaction,
        score,
        token_amount,
        tokenomics_version
),

final as (
    select
        *,
        lead(valid_from, 1) over (
            partition by user_guid, reward_type, tstamp, multiplier, payout_transaction
            order by valid_from
        ) as valid_to
    from
        stg_token_rewards
)

select * from final