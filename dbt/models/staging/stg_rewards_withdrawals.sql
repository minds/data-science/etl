with

rewards_withdrawals as (
    select * from {{ source("cassandra_extract", "rewards_withdrawals") }}
),

stg_rewards_withdrawals as (
    select
        tx as transaction_guid,
        user_guid,
        timestamp as tstamp,
        address as withdrawal_address,
        amount,
        completed as is_completed,
        completed_tx as completed_transaction,
        gas,
        status as withdrawal_status,
        min(extracted_at) as valid_from
    from
        rewards_withdrawals
    group by
        transaction_guid,
        user_guid,
        tstamp,
        withdrawal_address,
        amount,
        is_completed,
        completed_transaction,
        gas,
        withdrawal_status
),

final as (
    select
        *,
        lead(valid_from, 1) over (
            partition by transaction_guid
            order by valid_from
        ) as valid_to
    from
        stg_rewards_withdrawals
)

select * from final