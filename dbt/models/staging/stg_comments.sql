with

comments as (
    select * from {{ source("cassandra_extract", "comments") }}
),

cassandra_comments as (
    select
        guid as comment_guid,
        owner_guid as user_guid,
        entity_guid as entity_guid,
        container_guid as container_guid,
        parent_guid_l1,
        parent_guid_l2,
        parent_guid_l3,
        access_id,
        flags,
        has_children,
        body,
        len(body) as character_count,
        replies_count,
        time_created,
        time_updated,
        votes_up,
        votes_down,
        min(extracted_at) as valid_from
    from
        comments
    group by
        guid,
        owner_guid,
        entity_guid,
        container_guid,
        parent_guid_l1,
        parent_guid_l2,
        parent_guid_l3,
        access_id,
        flags,
        has_children,
        body,
        replies_count,
        time_created,
        time_updated,
        votes_up,
        votes_down
),

additional_fields as (
    select
        comment_guid,
        user_guid,
        entity_guid,
        container_guid,
        parent_guid_l1,
        parent_guid_l2,
        parent_guid_l3,
        access_id,
        flags:deleted as is_deleted,
        flags:edited as is_edited,
        flags:mature as is_mature,
        flags:spam as is_spam,
        has_children,
        body,
        regexp_substr(body, '(http|https)\\:\\/\\/[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,3}(\\/\\S*)?') as comment_link,
        character_count,
        replies_count,
        time_created,
        time_updated,
        array_size(votes_up) as votes_up_count,
        array_size(votes_down) as votes_down_count,
        votes_up,
        votes_down,
        valid_from
    from
        cassandra_comments
),

final as (
    select
        *,
        lead(valid_from, 1) over (
            partition by comment_guid
            order by valid_from
        ) as valid_to
    from
        additional_fields
)

select * from final