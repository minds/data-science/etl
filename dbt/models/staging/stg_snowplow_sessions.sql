with
user_event_order as (
    select
        row_number() over (partition by user_id order by collector_tstamp) as event_order,
        first_value(collector_tstamp) over (partition by user_id order by collector_tstamp) as user_first_tstamp,
        collector_tstamp,
        user_id,
        network_userid,
        coalesce(action, event_name) as event_type,
        user_ipaddress,
        useragent,
        br_name,
        dvce_type
    from
        {{ ref('stg_snowplow_events_userid') }}
    where
        action is null or action <> 'active' -- workaround for broken snowplow cookie contexts
),

session_starts as (
    select
        timestampdiff('seconds', lag(collector_tstamp)
                             over (partition by user_id order by event_order),
                  collector_tstamp) as tstamp_offset,
        case when tstamp_offset > 1800 then 1 when tstamp_offset is null then 1 else 0 end as is_session_start,
        event_order,
        collector_tstamp,
        user_id,
        user_first_tstamp,
        network_userid,
        event_type,
        user_ipaddress,
        useragent,
        br_name,
        dvce_type
    from
        user_event_order
),

sessions_numbered as (
    select
        sum(is_session_start)
        over (partition by user_id order by event_order rows unbounded preceding) as session_number,
        *
    from
        session_starts
),

sessions_aggregated as (
    select
        session_number,
        user_id,
        user_first_tstamp,
        count(distinct network_userid) as network_userid_count,
        max(case when is_session_start=1 then user_ipaddress end) as user_ipaddress,
        max(case when is_session_start=1 then useragent end) as useragent,
        max(case when is_session_start=1 then br_name end) as br_name,
        max(case when is_session_start=1 then dvce_type end) as dvce_type,
        min(collector_tstamp) as first_tstamp,
        max(collector_tstamp) as last_tstamp,
        timestampdiff('seconds', min(collector_tstamp), max(collector_tstamp)) as duration,
        count(*) as event_count,
        count(case when event_type='view' then 1 end) as view_count,
        count(case when event_type='vote:up' then 1 end) as vote_up_count,
        count(case when event_type='vote:down' then 1 end) as vote_down_count,
        count(case when event_type='remind' then 1 end) as remind_count,
        count(case when event_type='comment' then 1 end) as comment_count,
        count(case when event_type='login' then 1 end) as login_count,
        count(case when event_type='subscribe' then 1 end) as subscribe_count,
        count(case when event_type='boost' then 1 end) as boost_count,
        count(case when event_type='accept' then 1 end) as accept_count,
        count(case when event_type='email:confirm' then 1 end) as email_confirm_count,
        count(case when event_type='email:clicks' then 1 end) as email_click_count,
        count(case when event_type='email:unsubscribe' then 1 end) as email_unsubscribe_count,
        count(case when event_type='block' then 1 end) as block_count,
        count(case when event_type='unblock' then 1 end) as unblock_count,
        count(case when event_type='join' then 1 end) as join_count,
        count(case when event_type='joined' then 1 end) as joined_rewards_count
    from
        sessions_numbered
    group by
        user_id,
        user_first_tstamp,
        session_number
),

session_intervals as (
    select
        session_number,
        user_id,
        user_first_tstamp,
        network_userid_count,
        case
            when user_id regexp '^[0-9]{18,19}$' then 'user'
            when user_id regexp '^[0-9a-z]{16,22}$' then 'pseudo'
            else 'network'
        end as userid_type,
        user_ipaddress,
        useragent,
        br_name,
        dvce_type,
        first_tstamp,
        last_tstamp,
        duration,
        timestampdiff(second, lag(last_tstamp) over (partition by user_id order by session_number), first_tstamp) as interval,
        event_count,
        view_count,
        vote_up_count,
        vote_down_count,
        remind_count,
        comment_count,
        login_count,
        subscribe_count,
        boost_count,
        accept_count,
        email_confirm_count,
        email_click_count,
        email_unsubscribe_count,
        block_count,
        unblock_count,
        join_count,
        joined_rewards_count
    from
        sessions_aggregated
)

select * from session_intervals order by first_tstamp
