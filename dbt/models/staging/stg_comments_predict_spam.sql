with

predict_spam as (
  select * from {{ source('cassandra_extract','int_comments_predict_spam')}}
),

valid_from as (
    select
        comment_guid,
        owner_guid,
        time_created,
        body,
        spam_predict,
        min(extracted_at) as valid_from
    from
        predict_spam
    group by
        comment_guid,
        owner_guid,
        time_created,
        body,
        spam_predict
),

valid_to as (
    select
        *,
        lead(valid_from, 1) over (
            partition by comment_guid
            order by valid_from
        ) as valid_to
    from
        valid_from
)

select * from valid_to
