{{
    config (
        materialized='incremental',
        incremental_strategy='merge',
        unique_key='event_id'
    )
}}


with
dedup as (
    select
        event_id,
        collector_tstamp,
        derived_tstamp,
        event,
        event_name,
        user_ipaddress,
        user_id,
        network_userid,
        domain_userid,
        platform,
        useragent,
        br_name,
        br_lang,
        os_name,
        dvce_type,
        page_url,
        page_referrer,
        mkt_medium,
        mkt_source,
        mkt_term,
        mkt_content,
        mkt_campaign,
        domain_sessionid,
        domain_sessionidx,
        unstruct_event,
        contexts,
        row_number() over (partition by event_id order by collector_tstamp) as row_number
    from
        {{ source('atomic', 'events') }} as raw_events
        cross join {{ ref('int_tstamp_minmax') }} as minmax
    where
        raw_events.event_id not in (select event_id from {{ ref('int_event_blacklist') }} )
        and raw_events.collector_tstamp > minmax.min
        and raw_events.collector_tstamp <= minmax.max
        
),
events as (
    select
        *,
        PARSE_JSON(contexts)['data'] as contexts_json,
        PARSE_JSON(unstruct_event)['data']['data'] as event_props
    from
        dedup
    where
        row_number = 1
),
structured_events as (
    select
        event_id,
        collector_tstamp,
        derived_tstamp,
        event,
        event_name,
        user_ipaddress,
        user_id,
        network_userid,
        domain_userid,
        platform,
        useragent,
        br_name,
        br_lang,
        os_name,
        dvce_type,
        page_url,
        page_referrer,
        mkt_medium,
        mkt_source,
        mkt_term,
        mkt_content,
        mkt_campaign,
        domain_sessionid,
        domain_sessionidx,
        event_props:action::varchar(20) as action,
        event_props:boost_rating::smallint as boost_rating,
        event_props:boost_reject_reason::varchar(240) as boost_reject_reason,
        parse_json(base64_decode_string(event_props:comment_guid)):guid::varchar(20) as comment_guid,
        case
            when contexts_json[0].schema = 'iglu:com.minds/entity_context/jsonschema/1-0-0' then contexts_json[0].data
            when contexts_json[1]['schema'] = 'iglu:com.minds/entity_context/jsonschema/1-0-0' then contexts_json[1]['data']
            when contexts_json[2]['schema'] = 'iglu:com.minds/entity_context/jsonschema/1-0-0' then contexts_json[2]['data']
            when contexts_json[3]['schema'] = 'iglu:com.minds/entity_context/jsonschema/1-0-0' then contexts_json[3]['data']
        end as entity_data,
        entity_data:entity_guid::varchar(20) as entity_guid,
        entity_data:entity_owner_guid::varchar(20) as entity_owner_guid,
        entity_data:entity_type::varchar(20) as entity_type,
        entity_data:entity_subtype::varchar(20) as entity_subtype,
        entity_data:entity_container_guid::varchar(20) as entity_container_guid,
        entity_data:entity_access_id::varchar(20) as entity_access_id,
        event_props:campaign::varchar(240) as view_campaign,
        event_props:delta::integer as view_delta,
        event_props:medium::varchar(240) as view_medium,
        event_props:platform::varchar(20) as view_platform,
        event_props:position::integer as view_position,
        event_props:entity_guid::varchar(20) as view_entity_guid,
        event_props:entity_owner_guid::varchar(20) as view_entity_owner_guid,
        event_props,
        contexts_json,
        case
            when contexts_json[0].schema = 'iglu:com.minds/growthbook_context/jsonschema/1-0-1' and contexts_json[0].data.experiment_id = 'boost-prompt' then contexts_json[0].data.variation_id
            when contexts_json[1].schema = 'iglu:com.minds/growthbook_context/jsonschema/1-0-1' and contexts_json[1].data.experiment_id = 'boost-prompt' then contexts_json[1].data.variation_id
            when contexts_json[2].schema = 'iglu:com.minds/growthbook_context/jsonschema/1-0-1' and contexts_json[2].data.experiment_id = 'boost-prompt' then contexts_json[2].data.variation_id
            when contexts_json[3].schema = 'iglu:com.minds/growthbook_context/jsonschema/1-0-1' and contexts_json[3].data.experiment_id = 'boost-prompt' then contexts_json[3].data.variation_id
            end as boost_prompt,
        case
            when contexts_json[0].schema = 'iglu:com.minds/growthbook_context/jsonschema/1-0-1' and contexts_json[0].data.experiment_id = 'boost-rotator' then contexts_json[0].data.variation_id
            when contexts_json[1].schema = 'iglu:com.minds/growthbook_context/jsonschema/1-0-1' and contexts_json[1].data.experiment_id = 'boost-rotator' then contexts_json[1].data.variation_id
            when contexts_json[2].schema = 'iglu:com.minds/growthbook_context/jsonschema/1-0-1' and contexts_json[2].data.experiment_id = 'boost-rotator' then contexts_json[2].data.variation_id
            when contexts_json[3].schema = 'iglu:com.minds/growthbook_context/jsonschema/1-0-1' and contexts_json[3].data.experiment_id = 'boost-rotator' then contexts_json[3].data.variation_id
        end as boost_rotator,
        case
            when contexts_json[0].schema = 'iglu:com.minds/growthbook_context/jsonschema/1-0-1' and contexts_json[0].data.experiment_id = 'channel-gallery' then contexts_json[0].data.variation_id
            when contexts_json[1].schema = 'iglu:com.minds/growthbook_context/jsonschema/1-0-1' and contexts_json[1].data.experiment_id = 'channel-gallery' then contexts_json[1].data.variation_id
            when contexts_json[2].schema = 'iglu:com.minds/growthbook_context/jsonschema/1-0-1' and contexts_json[2].data.experiment_id = 'channel-gallery' then contexts_json[2].data.variation_id
            when contexts_json[3].schema = 'iglu:com.minds/growthbook_context/jsonschema/1-0-1' and contexts_json[3].data.experiment_id = 'channel-gallery' then contexts_json[3].data.variation_id
        end as channel_gallery,
        event_props:experiment_id::varchar(120) as experiment_id,
        event_props:variation_id::smallint as variation_id,
        contexts_json[0] as context_0,
        contexts_json[1] as context_1,
        contexts_json[2] as context_2,
        contexts_json[3] as context_3
    from
        events
),
final_events as (
    select
        event_id,
        collector_tstamp,
        derived_tstamp,
        event,
        event_name,
        user_ipaddress,
        user_id,
        network_userid,
        domain_userid,
        platform,
        useragent,
        br_name,
        br_lang,
        os_name,
        dvce_type,
        page_url,
        page_referrer,
        mkt_medium,
        mkt_source,
        mkt_term,
        mkt_content,
        mkt_campaign,
        domain_sessionid,
        domain_sessionidx,
        action,
        boost_rating,
        boost_reject_reason,
        comment_guid,
        entity_guid,
        entity_owner_guid,
        entity_type,
        entity_subtype,
        entity_container_guid,
        entity_access_id,
        view_campaign,
        view_delta,
        view_medium,
        view_platform,
        view_position,
        view_entity_guid,
        view_entity_owner_guid,
        boost_prompt,
        boost_rotator,
        channel_gallery,
        event_props
    from
        structured_events
)

select * from final_events
