with

contributions as (
    select * from {{ source("cassandra_extract", "contributions") }}
),

stg_contributions as (
    select
        user_guid,
        timestamp as tstamp,
        metric,
        amount, 
        score,
        min(extracted_at) as valid_from
    from
        contributions
    group by
        user_guid,
        tstamp,
        metric,
        amount,
        score
),

final as (
    select
        *,
        lead(valid_from, 1) over (
            partition by user_guid, tstamp, metric
            order by valid_from
        ) as valid_to
    from
        stg_contributions
)

select * from final
