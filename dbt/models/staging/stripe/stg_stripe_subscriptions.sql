with 

stripe_subscriptions as (
    select * from {{ source("stripe_extract", "stripe_subscriptions") }}
),

stg_stripe_subscriptions as (
    select 
        _airbyte_ab_id as airbyte_id,
        _airbyte_emitted_at as airbyte_emitted_at,
        --_airbyte_data:application
        --_airbyte_data:application_fee_percent
        _airbyte_data:billing::varchar as billing,
        --_airbyte_data:billing_cycle_anchor
        --_airbyte_data:billing_thresholds
        --_airbyte_data:cancel_at
        --_airbyte_data:cancel_at_period_end
        _airbyte_data:canceled_at::datetime as cancelled_at,
        --_airbyte_data:collection_method
        _airbyte_data:created::datetime as created_at,
        _airbyte_data:currency::varchar as currency,
        _airbyte_data:current_period_end::datetime as current_period_end,
        _airbyte_data:current_period_start::datetime as current_period_start,
        _airbyte_data:customer::varchar as customer_id,
        --_airbyte_data:days_until_due
        --_airbyte_data:default_payment_method
        --_airbyte_data:default_source
        --_airbyte_data:default_tax_rates
        --_airbyte_data:description
        --_airbyte_data:discount
        _airbyte_data:ended_at::datetime as ended_at,
        _airbyte_data:id::varchar as subscription_id
        --_airbyte_data:items
        --_airbyte_data:latest_invoice
        --_airbyte_data:livemode
        --_airbyte_data:metadata
        --_airbyte_data:next_pending_invoice_item_invoice
        --_airbyte_data:object
        --_airbyte_data:pause_collection
        --_airbyte_data:payment_settings
        --_airbyte_data:pending_invoice_item_interval
        --_airbyte_data:pending_setup_intent
        --_airbyte_data:pending_update
        --_airbyte_data:plan
    from 
        stripe_subscriptions
),

dedupe as (
    select
        *
    from 
        stg_stripe_subscriptions
    qualify
        row_number() over (partition by subscription_id order by airbyte_emitted_at desc) = 1
)

select * from dedupe