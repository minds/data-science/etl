with 

stripe_account_id_map as (
    select * from {{ source("stripe_extract", "stripe_account_id_map") }}
),

stripe_account as (
    select 
        stripe_acct_id as stripe_account_id,
        minds_user_guid as user_guid,
        min(extracted_at) as valid_from
    from 
        stripe_account_id_map
    group by 
        stripe_account_id,
        user_guid
),

ranged as (
    select
        *,
        lead(valid_from, 1) over (
            partition by stripe_account_id
            order by valid_from
        ) as valid_to
    from
        stripe_account
),

final as (
    select 
        stripe_account_id,
        user_guid,
        case
            when valid_to is null
            then true
            else false
        end as is_current
    from 
        ranged
)

select * from final where is_current