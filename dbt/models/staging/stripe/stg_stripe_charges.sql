with 

stripe_charges as (
    select * from {{ source("stripe_extract", "stripe_charges") }}
),

stg_stripe_charges as (
    select 
        _airbyte_ab_id as airbyte_id,
        _airbyte_emitted_at as airbyte_emitted_at,
        _airbyte_data:amount::int as amount,
        _airbyte_data:amount_captured::int as amount_captured,
        _airbyte_data:amount_refunded::int as amount_refunded,
        --_airbyte_data:application
        _airbyte_data:application_fee::varchar as application_fee_id,
        _airbyte_data:application_fee_amount::int as application_fee_amount,
        _airbyte_data:balance_transaction::varchar as balance_transaction,  
        --_airbyte_data:billing_details
        --_airbyte_data:calculated_statement_descriptor
        --_airbyte_data:captured
        _airbyte_data:created::datetime as transaction_tstamp,
        _airbyte_data:customer::varchar as customer_id,
        _airbyte_data:currency::varchar as currency,
        --_airbyte_data:description
        _airbyte_data:destination::varchar as destination, 
        --_airbyte_data:dispute
        --_airbyte_data:disputed
        --_airbyte_data:failure_balance_transaction
        --_airbyte_data:failure_code
        --_airbyte_data:failure_message
        --_airbyte_data:fraud_details
        _airbyte_data:id::varchar as charge_id, 
        --_airbyte_data:invoice
        --_airbyte_data:livemode
        _airbyte_data:metadata:user_guid::varchar as user_guid,
        _airbyte_data:metadata:boost_guid::varchar as boost_guid,
        _airbyte_data:metadata:boost_owner_guid::varchar as boost_owner_guid,
        _airbyte_data:metadata:orderId::varchar as order_id,
        _airbyte_data:invoice::varchar as invoice_id,
        --_airbyte_data:object
        _airbyte_data:on_behalf_of::varchar as on_behalf_of,
        --_airbyte_data:order
        --_airbyte_data:outcome
        _airbyte_data:paid::boolean as is_paid,
        --_airbyte_data:payment_intent
        --_airbyte_data:payment_method
        --_airbyte_data:payment_method_details
        --_airbyte_data:receipt_email
        --_airbyte_data:receipt_number
        --_airbyte_data:receipt_url
        --_airbyte_data:refunded
        --_airbyte_data:refunds
        --_airbyte_data:review
        --_airbyte_data:shipping
        --_airbyte_data:source
        --_airbyte_data:source_transfer
        --_airbyte_data:statement_descriptor
        --_airbyte_data:statement_descriptor_suffix
        _airbyte_data:status::varchar as charge_status,
        --_airbyte_data:transfer_data
        _airbyte_data:transfer_group::varchar as transfer_group_id
    from 
        stripe_charges
),

dedupe as (
    select
        *
    from 
        stg_stripe_charges
    qualify
        row_number() over (partition by charge_id order by airbyte_emitted_at desc) = 1
)

select * from dedupe