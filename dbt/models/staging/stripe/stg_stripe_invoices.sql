with 

stripe_invoices as (
    select * from {{ source("stripe_extract", "stripe_invoices") }}
),

stg_stripe_invoices as (
    select 
        _airbyte_ab_id as airbyte_id,
        _airbyte_emitted_at as airbyte_emitted_at,
        _airbyte_data:account_country::varchar as account_country,
        _airbyte_data:account_name::varchar as account_name,
        --account_tax_ids
        _airbyte_data:amount_due::int as amount_due,
        _airbyte_data:amount_paid::int as amount_paid,
        _airbyte_data:amount_remaining::int as amount_remaining,
        --application
        --application_fee_amount
        --attempt_count
        --attempted
        --auto_advance
        --automatic_tax:enabled
        --automatic_tax:status
        _airbyte_data:billing::varchar as billing,
        _airbyte_data:billing_reason::varchar as billing_reason,
        _airbyte_data:charge::varchar as charge_id,
        _airbyte_data:collection_method::varchar as collection_method,
        _airbyte_data:created::datetime as created_at,
        _airbyte_data:currency::varchar as currency,
        --_airbyte_data:custom_fields
        _airbyte_data:customer::varchar as customer_id,
        --_airbyte_data:customer_address
        --_airbyte_data:customer_email
        --_airbyte_data:customer_name
        --_airbyte_data:customer_phone
        --_airbyte_data:customer_shipping
        --_airbyte_data:customer_tax_exempt
        --_airbyte_data:customer_tax_ids
        --_airbyte_data:default_payment_method
        --_airbyte_data:default_source
        --_airbyte_data:default_tax_rates
        --_airbyte_data:description
        --_airbyte_data:discount
        --_airbyte_data:discounts
        --_airbyte_data:due_date
        _airbyte_data:ending_balance::int as ending_balance,
        --_airbyte_data:footer
        --_airbyte_data:hosted_invoice_url
        _airbyte_data:id::varchar as invoice_id,
        --_airbyte_data:invoice_pdf
        --_airbyte_data:last_finalization_error
        data_lines.value as data_lines,
        _airbyte_data:subscription::varchar as subscription_id
        --_airbyte_data:subtotal
        --_airbyte_data:subtotal_excluding_tax
        --_airbyte_data:tax
        --_airbyte_data:tax_percent
        --_airbyte_data:test_clock
        --_airbyte_data:total
        --_airbyte_data:total_discount_amounts
        --_airbyte_data:total_excluding_tax
        --_airbyte_data:total_tax_amounts
        --_airbyte_data:transfer_data
        --_airbyte_data:webhooks_delivered_at
    from 
        stripe_invoices,
        lateral flatten(input => parse_json(_airbyte_data:lines:data)) data_lines
),

invoice_data_lines as (
    select 
        airbyte_id,
        airbyte_emitted_at,
        account_country,
        account_name,
        amount_due,
        amount_paid,
        amount_remaining,
        billing,
        billing_reason,
        charge_id,
        collection_method,
        created_at,
        currency,
        customer_id,
        ending_balance,
        invoice_id,
        data_lines:id::varchar as invoice_line_id,
        data_lines:amount::int as line_amount,
        data_lines:description::varchar as line_description,
        data_lines:metadata:orderId::varchar as order_id,
        data_lines:plan:id::varchar as plan,
        data_lines:plan:product::varchar as product_id,
        subscription_id
    from 
        stg_stripe_invoices
),

dedupe as (
    select
        *
    from 
        invoice_data_lines
    qualify
        row_number() over (partition by invoice_id, invoice_line_id order by airbyte_emitted_at desc) = 1
)

select * from dedupe