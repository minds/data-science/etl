with 

stripe_products as (
    select * from {{ source("stripe_extract", "stripe_products") }}
),

stg_stripe_products as (
    select 
        _airbyte_ab_id as airbyte_id,
        _airbyte_emitted_at as airbyte_emitted_at,
        _airbyte_data:active::boolean as is_active,
        --_airbyte_data:attributes
        _airbyte_data:created::datetime as created_at,
        --_airbyte_data:default_price
        _airbyte_data:description::varchar as product_description,
        _airbyte_data:id::varchar as product_id,
        --_airbyte_data:images
        --_airbyte_data:livemode
        --_airbyte_data:metadata
        _airbyte_data:name::varchar as product_name,
        --_airbyte_data:object
        --_airbyte_data:package_dimensions
        --_airbyte_data:shippable
        --_airbyte_data:statement_descriptor
        --_airbyte_data:tax_code
        _airbyte_data:type::varchar as product_type,
        --_airbyte_data:unit_label
        _airbyte_data:updated::datetime as updated_at
        --_airbyte_data:url
    from 
        stripe_products
),

dedupe as (
    select
        *
    from 
        stg_stripe_products
    qualify
        row_number() over (partition by product_id order by airbyte_emitted_at desc) = 1
)

select * from dedupe