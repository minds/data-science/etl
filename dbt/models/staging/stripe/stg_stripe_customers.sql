with 

stripe_customers as (
    select * from {{ source("stripe_extract", "stripe_customers") }}
),

stg_stripe_customers as (
    select 
        _airbyte_ab_id as airbyte_id,
        _airbyte_emitted_at as airbyte_emitted_at,
        _airbyte_data:id::varchar as customer_id,
        _airbyte_data:created::datetime as created_at,
        _airbyte_data:account_balance::int as account_balance,
        _airbyte_data:balance::int as balance,
        _airbyte_data:currency::varchar as currency,
        _airbyte_data:default_currency::varchar as default_currency,
        _airbyte_data:email::varchar as email,
        _airbyte_data:name::varchar as customer_name,
        --_airbyte_data:address
        --_airbyte_data:default_source
        --_airbyte_data:delinquent
        --_airbyte_data:description
        --_airbyte_data:discount
        --_airbyte_data:invoice_prefix
        --_airbyte_data:invoice_settings:custom_fields
        --_airbyte_data:invoice_settings:default_payment_method
        --_airbyte_data:invoice_settings:footer
        --_airbyte_data:invoice_settings:rendering_options
        --_airbyte_data:livemode
        _airbyte_data:metadata:user_guid::varchar as user_guid
        --_airbyte_data:next_invoice_sequence
        --_airbyte_data:object
        --_airbyte_data:phone
        --_airbyte_data:preferred_locales
        --_airbyte_data:shipping
        --_airbyte_data:sources:data
        --_airbyte_data:sources:has_more
        --_airbyte_data:sources:object
        --_airbyte_data:sources:total_count
        --_airbyte_data:sources:url
        --_airbyte_data:subscriptions:data
        --_airbyte_data:subscriptions:has_more
        --_airbyte_data:subscriptions:object
        --_airbyte_data:subscriptions:total_count
        --_airbyte_data:subscriptions:url
        --_airbyte_data:tax_exempt
        --_airbyte_data:tax_ids:data
        --_airbyte_data:tax_ids:has_more
        --_airbyte_data:tax_ids:object
        --_airbyte_data:tax_ids:total_count
        --_airbyte_data:tax_ids:url
        --_airbyte_data:tax_info
        --_airbyte_data:tax_info_verification
        --_airbyte_data:test_clock
    from 
        stripe_customers
),

dedupe as (
    select
        *
    from 
        stg_stripe_customers
    qualify
        row_number() over (partition by customer_id order by airbyte_emitted_at desc) = 1
)

select * from dedupe