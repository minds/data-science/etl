with 

stripe_refunds as (
    select * from {{ source("stripe_extract", "stripe_refunds") }}
),

stg_stripe_refunds as (
    select
        _airbyte_ab_id as airbyte_id,
        _airbyte_emitted_at as airbyte_emitted_at,
        _airbyte_data:amount::int as refund_amount,
        _airbyte_data:balance_transaction:varchar as balance_transaction,
        _airbyte_data:charge::varchar as charge_id,
        _airbyte_data:created::datetime as created_at,
        _airbyte_data:currency::varchar as currency,
        _airbyte_data:id::varchar as refund_id,
        --_airbyte_data:metadata
        --_airbyte_data:object
        --_airbyte_data:payment_intent
        _airbyte_data:reason::varchar as refund_reason,
        --_airbyte_data:receipt_number
        --_airbyte_data:source_transfer_reversal
        _airbyte_data:status::varchar as refund_status
        --_airbyte_data:transfer_reversal
    from
        stripe_refunds
),

dedupe as (
    select
        *
    from 
        stg_stripe_refunds
    qualify
        row_number() over (partition by refund_id order by airbyte_emitted_at desc) = 1
)

select * from dedupe