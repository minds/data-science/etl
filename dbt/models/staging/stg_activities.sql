with

activity as (
    select * from {{ source("cassandra_extract", "activity") }}
),

cassandra_activities as (
    select
        key as activity_guid,
        owner_guid as user_guid,
        container_guid as group_guid,
        entity_guid,
        access_id,
        time_created,
        time_updated,
        time_sent,
        language,
        deleted as is_deleted,
        edited as is_edited,
        enabled as is_enabled,
        mature as is_mature,
        try_parse_json(remind_object):guid::int as remind_guid,
        nsfw,
        nsfw_lock,
        rating,
        spam as is_spam,
        tags,
        boost_rejection_reason,
        regexp_extract_all(thumbs_up_user_guids, '[[:digit:]]+') as votes_up,
        regexp_extract_all(thumbs_down_user_guids, '[[:digit:]]+') as votes_down,
        title,
        blurb,
        message,
        min(extracted_at) as valid_from
    from
        activity
    group by
        activity_guid,
        user_guid,
        group_guid,
        entity_guid,
        access_id,
        time_created,
        time_updated,
        time_sent,
        language,
        is_deleted,
        is_edited,
        is_enabled,
        is_mature,
        remind_object,
        nsfw,
        nsfw_lock,
        rating,
        is_spam,
        tags,
        boost_rejection_reason,
        votes_up,
        votes_down,
        title,
        blurb,
        message
),

final as (
    select
        *,
        case 
            when remind_guid is not null then 1
            else 0
        end as is_remind,
        lead(valid_from, 1) over (
            partition by activity_guid
            order by valid_from
        ) as valid_to
    from
        cassandra_activities
)

select * from final
