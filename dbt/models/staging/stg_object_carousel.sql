with

object_carousel as (
  select * from {{ source("cassandra_extract", "object_carousel") }}
),

carousel as (
    select
        key as entity_guid,
        owner_guid as user_guid,
        container_guid as group_guid,
        access_id,
        enabled as is_enabled,
        time_created,
        time_updated,
        flags,
        regexp_extract_all(thumbs_up_user_guids, '[[:digit:]]+') as votes_up,
        regexp_extract_all(thumbs_down_user_guids, '[[:digit:]]+') as votes_down,
        min(extracted_at) as valid_from
    from
        object_carousel
    group by
        entity_guid,
        user_guid,
        group_guid,
        access_id,
        is_enabled,
        time_created,
        time_updated,
        flags,
        votes_up,
        votes_down
),

final as (
    select
        *,
        lead(valid_from,1) over (
            partition by user_guid
            order by valid_from
        ) as valid_to
    from
        carousel
)

select * from final
