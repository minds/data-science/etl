with

object_image as (
  select * from {{ source("cassandra_extract", "object_image") }}
),

image as (
    select
        key as entity_guid,
        filename,
        owner_guid as user_guid,
        container_guid as group_guid,
        access_id,
        super_subtype,
        time_created,
        time_updated,
        time_sent,
        language,
        rating,
        hidden as is_hidden,
        mature as is_mature,
        nsfw,
        nsfw_lock,
        boost_rejection_reason,
        regexp_extract_all(thumbs_up_user_guids, '[[:digit:]]+') as votes_up,
        regexp_extract_all(thumbs_down_user_guids, '[[:digit:]]+') as votes_down,
        min(extracted_at) as valid_from
    from
        object_image
    group by
        entity_guid,
        filename,
        user_guid,
        group_guid,
        access_id,
        super_subtype,
        time_created,
        time_updated,
        time_sent,
        language,
        rating,
        is_hidden,
        is_mature,
        nsfw,
        nsfw_lock,
        boost_rejection_reason,
        votes_up,
        votes_down
),

final as (
    select
        *,
        lead(valid_from,1) over (
            partition by user_guid
            order by valid_from
        ) as valid_to
    from
        image
)

select * from final
