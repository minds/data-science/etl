with
cassandra_notifications as (
    select
        key as id,
        from_guid,
        to_guid,
        object_guid,
        access_id,
        notification_view,
        time_created,
        time_updated,
        enabled,
        min(extracted_at) as valid_from

    from
        {{ source("cassandra_extract", "notification") }}

    group by
        id,
        from_guid,
        to_guid,
        object_guid,
        access_id,
        notification_view,
        time_created,
        time_updated,
        enabled
)

select
    *,
    lead(valid_from) over (partition by id order by valid_from) as valid_to

from
    cassandra_notifications
