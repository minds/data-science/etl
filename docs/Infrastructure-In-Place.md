# Documentation of the infrastructure in place

Try to keep the documentation as up-to-date as possible, to minimize confusion, miscommunication, and to cultivate a habit of making our products well-documented and ready for handover. 

## Overview

Briefly mention what technologies are implemented, who has control over them, what models are running, etc.

## Warehouse (Redshift etc.)

Document users, databases, schemas, etc. 

## dbt

Document profiles, run modes, etc.

## Extract & Load

## BI - Superset, Metabase, Jupyter, etc.
