# Docker Instructions

## Composing an Image


## Environment Variables

In the docker directory of the project, create a `.env` file with the environment variables that are needed. 

The variables that are used by the Prefect flow need the following prefix: `PREFECT__CONTEXT__SECRETS__`

```bash
GITHUB_TOKEN=
GITHUB_ORG_NAME=
GITHUB_REPO_NAME=

PREFECT__CONTEXT__SECRETS_PREFECT_PROJECT_NAME=

PREFECT__CONTEXT__SECRETS__{VARIABLE}=
PREFECT__CONTEXT__SECRETS__{VARIABLE}=
PREFECT__CONTEXT__SECRETS__{VARIABLE}=
```

The names of the environment variables need to be present in the [docker/docker-compose.yml](../docker/docker-compose.yml) as well, or otherwise docker will not include them while composing the image. More specifically, under the `services:agent:environment` config. 