import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

from sqlalchemy import create_engine, MetaData, ForeignKey, Table, Column, Integer, String, Boolean, DateTime, Float
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from os import environ as env
import gc
from copy import copy
from datetime import datetime
from cassandra_metadata import get_metadata
from threading import Thread, current_thread
from multiprocessing import Process, Queue, current_process
from urllib.parse import quote

PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_CASSANDRA_SCHEMA = Secret('SNOWFLAKE_CASSANDRA_SCHEMA').get()
SNOWFLAKE_DBT_SCHEMA = Secret('SNOWFLAKE_DBT_SCHEMA').get()

REDSHIFT_HOST = Secret('REDSHIFT_HOST').get()
REDSHIFT_PORT = Secret('REDSHIFT_PORT').get()
REDSHIFT_USER = Secret('REDSHIFT_USER').get()
REDSHIFT_PASSWORD = Secret('REDSHIFT_PASSWORD').get()
REDSHIFT_DATABASE = Secret('REDSHIFT_DATABASE').get()
REDSHIFT_CASSANDRA_SCHEMA = Secret('REDSHIFT_CASSANDRA_SCHEMA').get()
REDSHIFT_DBT_SCHEMA = Secret('REDSHIFT_DBT_SCHEMA').get()
CASSANDRA_USER = Secret('CASSANDRA_USER').get()
CASSANDRA_PASSWORD = Secret('CASSANDRA_PASSWORD').get()
CASSANDRA_HOST = Secret('CASSANDRA_HOST').get()

metadata = MetaData(schema=SNOWFLAKE_CASSANDRA_SCHEMA)
users_tags = Table('pseudo_users_tag_vectors', metadata,
    Column('user_id', String(length=22)),
    Column('art', Float),
    Column('blockchain', Float),
    Column('blog', Float),
    Column('comedy', Float),
    Column('crypto', Float),
    Column('education', Float),
    Column('fashion', Float),
    Column('film', Float),
    Column('food', Float),
    Column('freespeech', Float),
    Column('gaming', Float),
    Column('health', Float),
    Column('history', Float),
    Column('journalism', Float),
    Column('memes', Float),
    Column('minds', Float),
    Column('mindsth', Float),
    Column('music', Float),
    Column('myphoto', Float),
    Column('nature', Float),
    Column('news', Float),
    Column('nutrition', Float),
    Column('outdoors', Float),
    Column('photography', Float),
    Column('poetry', Float),
    Column('politics', Float),
    Column('science', Float),
    Column('spirituality', Float),
    Column('sports', Float),
    Column('technology', Float),
    Column('travel', Float),
    Column('videos', Float)
)   

def get_db():
    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_CASSANDRA_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)

def get_cassandra():
    auth_provider = PlainTextAuthProvider(CASSANDRA_USER, CASSANDRA_PASSWORD)
    cluster = Cluster([CASSANDRA_HOST], auth_provider=auth_provider, prepare_on_all_hosts = True)
    cass = cluster.connect()
    cass.default_fetch_size = 10000
    return cass

def init_db():
    global metadata
    metadata.create_all(get_db())

def cassandra_values(conn):
    stmt = conn.prepare("select * from minds.pseudo_user_hashtags")
    for row in conn.execute(stmt):
        yield row

def process_record(row):
    record = {
        'pseudo_id': row.pseudo_id,
        'hashtag': row.hashtag,
    }
    return record

def executor(queue, logger):
    name = f"{current_process().name}_{current_thread().name}"
    conn = get_db().connect()
    logger.info(f"{name} listening on queue\n")
    while True:
        statement = queue.get()
        if statement == None:
            queue.put(None)
            break
        try:
            conn.execute(statement)
            del statement
        except BaseException as e:
            logger.warn("{} {} {}: {}".format(name, type(e), e, e.args))
    conn.close()

def setup(queue, logger):
    logger.info("Initialising DB")
    init_db()

    logger.info("Starting Snowflake insert threads")
    Thread(target=executor, name="Thread1", args=(queue, logger)).start()
    logger.info("Threads started")

    return queue

@task
def extract():
    global keys, schema, table_buffers, fields, metadata
    queue = Queue()
    logger = prefect.context.get("logger")
    logger.info("Setting up")
    setup(queue, logger)
    logger.info("Setup complete, connecting to cassandra")

    logger.info("Cassandra connection complete, starting extract")
    tag_list = ( 'art', 'blockchain', 'blog', 'comedy', 'crypto', 'education', 'fashion',
        'film', 'food', 'freespeech', 'gaming', 'health', 'history', 'journalism',
        'memes', 'minds', 'mindsth', 'music', 'myphoto', 'nature', 'news', 'nutrition',
        'outdoors', 'photography', 'poetry', 'politics', 'science', 'spirituality', 'sports',
        'technology', 'travel', 'videos')
    tag_index = {}

    i = 1
    for tag in tag_list:
        tag_index[tag] = i
        i += 1

    user_template = [0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

    ids = {}
    try:
        cass = get_cassandra()
        for row in cassandra_values(cass):
            record = process_record(row)
            if record['pseudo_id'] not in ids:
                ids[record['pseudo_id']] = copy(user_template)
                ids[record['pseudo_id']][0] = record['pseudo_id']
            if record['hashtag'] in tag_index:
                ids[record['pseudo_id']][tag_index[record['hashtag']]] = 1.0
    except BaseException as e:
        logger.warn(f"Unable to process because:")
        logger.warn(f"{type(e)} {e}: {e.args}")
    
    records = []
    for key in ids.keys():
        records.append(ids[key])

    logger.info(f"Extract complete, flushing {len(records)} records to {SNOWFLAKE_CASSANDRA_SCHEMA}.pseudo_users_tag_vectors")
    delete = metadata.tables[f"{SNOWFLAKE_CASSANDRA_SCHEMA}.pseudo_users_tag_vectors"].delete()
    ins = metadata.tables[f"{SNOWFLAKE_CASSANDRA_SCHEMA}.pseudo_users_tag_vectors"].insert()
    queue.put(delete)
    offset = 0
    while offset < len(records):
        queue.put(ins.values(records[offset:offset+1000]))
        offset += 1000
        
    logger.info("Record flush complete, closing queue")
    queue.put(None)

with Flow("extract-cassandra-pseudo-tags") as flow:
    extract()

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
