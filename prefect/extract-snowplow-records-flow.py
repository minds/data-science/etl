import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

from sqlalchemy import create_engine
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from cassandra.concurrent import execute_concurrent_with_args
from os import environ as env
from urllib.parse import quote

from datetime import datetime

PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_CASSANDRA_SCHEMA = Secret('SNOWFLAKE_CASSANDRA_SCHEMA').get()
SNOWFLAKE_DBT_SCHEMA = Secret('SNOWFLAKE_DBT_SCHEMA').get()

CASSANDRA_HOST = Secret('CASSANDRA_HOST').get()
CASSANDRA_USER = Secret('CASSANDRA_USER').get()
CASSANDRA_PASSWORD = Secret('CASSANDRA_PASSWORD').get()

def get_snowflake():
    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        'PUBLIC',
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)

@task
def update_snowflake():
    copy_events = """
    copy into snowplow_atomic.events
        from @S3/snowplow/enriched/good/
        file_format=(
            type=csv
            compression=auto
            record_delimiter='\n'
            field_delimiter='\t'
            skip_header=0
        )
        on_error=continue
    """
    copy_bad_events = """
    copy into snowplow_atomic.bad_events
        from @S3/snowplow/enriched-bad/
        file_format=(
            type=csv
            compression=auto
            record_delimiter='\n'
            field_delimiter='\t'
            skip_header=0
        )
        on_error=continue
    """

    logger = prefect.context.get("logger")
    try:
        engine = get_snowflake()
        conn   = engine.connect()

        logger.info(f"Connection to Snowflake successfully opened")
        logger.info(f"Updating Snowplow data from S3")

        conn.execute(copy_events)
        conn.execute(copy_bad_events)

    finally:
        conn.close()
        engine.dispose()

        logger.info(f"Connection to Snowflake successfully closed")
    
with Flow("extract-snowplow-records") as flow:
    update_snowflake()

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
