# Import library
import prefect
from prefect import task, Flow, Parameter
from prefect.client import Secret
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.sql import text
from urllib.parse import quote
from os import environ as env

import numpy as np ## scientific computation
import pandas as pd ## loading dataset file

import datetime

import networkx as nx
from community import community_louvain

PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_CASSANDRA_SCHEMA = Secret('SNOWFLAKE_CASSANDRA_SCHEMA').get()
SNOWFLAKE_DBT_SCHEMA = Secret('SNOWFLAKE_DBT_SCHEMA').get()

def get_db():
    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_DBT_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )

    return create_engine(url, connect_args = {'client_session_keep_alive': True})


def get_interactions():
    conn = get_db().connect()
    interactions = pd.read_sql(f"""
        select
            *
        from
            {SNOWFLAKE_DBT_SCHEMA}.int_user_interaction_events
        order by
            time_created desc
        limit
            18000000
        """, conn)

    return interactions

# bring in user data, specifically the user_type relating to token farming
def get_users():
    conn = get_db().connect()
    users = pd.read_sql(f"""
        select
            user_guid::string as user_guid,
            user_type::string as user_type
        from
            {SNOWFLAKE_DBT_SCHEMA}.dmn_users
    """, conn)

    return users

@task
def create_graph():
# group transactions by sender > receiver and sum the transaction value\n",
    sum_interactions = get_interactions().groupby(['owner_guid','owner_user_type','engager_guid','engager_user_type'])['activity_guid'].count().reset_index()

# create a tuple for adding edges to the graph model
    tuple_interactions = []
    for i in range(sum_interactions.shape[0]):
        sender = sum_interactions.loc[i, 'owner_guid']
        receiver = sum_interactions.loc[i, 'engager_guid']
        amount = float(sum_interactions.loc[i, 'activity_guid'])
        tuple_interactions.append((sender, receiver, amount))

# create an array of unique user_id in the transactions table
    unique_transactors = pd.unique(sum_interactions[['owner_guid', 'engager_guid']].values.ravel('K'))

    users = get_users()
    nodes = users[users['user_guid'].isin(unique_transactors)].reset_index(drop=True)
    nodes_index = nodes.set_index('user_guid', drop=True)
    user_list = (nodes['user_guid'])

    G = nx.Graph()
# create nodes and their attributes
    G.add_nodes_from(user_list)
    for i in G.nodes:
        G.nodes[i]['user_tf_type'] = nodes_index.loc[i]['user_type']
#create edges and their weights
    G.add_weighted_edges_from(ebunch_to_add=tuple_interactions)
    return G

@task
def define_communities(graph):
    comms = community_louvain.best_partition(graph)
    communities = pd.DataFrame.from_dict(comms, orient='index', columns=['community'])
    communities.reset_index(inplace=True)
    communities.rename(columns = {'index' : 'user_guid'}, inplace=True)

    return communities


@task
def upload_communities(df):
    conn = get_db().connect()
    df.to_sql('int_interaction_clusters', conn, schema=SNOWFLAKE_CASSANDRA_SCHEMA, index=False,
                        if_exists='replace', chunksize = 5000,
                        dtype={'user_guid':  sqlalchemy.types.VARCHAR(),
                               'community': sqlalchemy.types.VARCHAR(length=65535)
                              })
    conn.close()


with Flow("identify-user-communities") as flow:
    upload_communities(define_communities(create_graph()))

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
