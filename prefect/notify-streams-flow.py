import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

from sqlalchemy import create_engine
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
import pulsar
from pulsar.schema import AvroSchema, JsonSchema, Record, Integer, Long, Double, Boolean, String
import json
from os import environ as env
from os import remove
from datetime import datetime
from cassandra_entities_metadata import get_metadata
from threading import Thread, current_thread
from multiprocessing import Process, Queue, current_process
from urllib.parse import quote

PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_CASSANDRA_SCHEMA = Secret('SNOWFLAKE_CASSANDRA_SCHEMA').get()
SNOWFLAKE_DBT_SCHEMA = Secret('SNOWFLAKE_DBT_SCHEMA').get()

CASSANDRA_USER = Secret('CASSANDRA_USER').get()
CASSANDRA_PASSWORD = Secret('CASSANDRA_PASSWORD').get()
CASSANDRA_HOST = Secret('CASSANDRA_HOST').get()

PULSAR_URL = 'pulsar+ssl://ac9142769e6df42fbaa3a6565a232275-51f8e532103492f8.elb.us-east-1.amazonaws.com:6651/'
PULSAR_ROOT = 'persistent://minds-com/engine'

DATA_DIR = '/root/data'

metadata = get_metadata(schema=SNOWFLAKE_CASSANDRA_SCHEMA)

def get_db():
    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_CASSANDRA_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)

def get_pulsar_client():
    client = pulsar.Client(
        PULSAR_URL,
        tls_trust_certs_file_path='/home/ubuntu/minds-ca.crt',
        tls_allow_insecure_connection=True
    )
    return client

def get_pulsar_producer(client, topic, schema_class):
    producer = client.create_producer(topic=f"{PULSAR_ROOT}/{topic}", schema=JsonSchema(schema_class))

    return producer

class SpamComment(Record):
    comment_guid = Long(required=True)
    owner_guid = Long(required=True)
    entity_guid = Long()
    parent_guid_l1 = Long() 
    parent_guid_l2 = Long()
    parent_guid_l3 = Long()
    time_created = Long(required=True)
    spam_predict = Double(required=True)
    activity_views = Integer(required=True)
    last_engagement = Long(required=True)
    score = Double(required=True)

def get_spam_comments():
    query = f"""
    select
        comment_guid,
        user_guid as owner_guid,
        entity_guid,
        parent_guid_l1,
        parent_guid_l2,
        parent_guid_l3,
        timestampdiff(millisecond, '1970-01-01 00:00:00'::timestamp_ntz, time_created) as time_created,
        spam_predict,
        activity_views,
        timestampdiff(millisecond, '1970-01-01 00:00:00'::timestamp_ntz, last_engagement) as last_engagement,
        score
    from
        {SNOWFLAKE_DBT_SCHEMA}.auto_moderation_comments
    where
        time_created is not null
    order by
        score desc
    limit 5000
    """

    with get_db().connect() as conn:
        result = conn.execute(query)
        for row in result:
            yield row

def send_spam_comments(logger):
    logger.info("Sending spam comments report")
    try:
        client = get_pulsar_client()
        logger.info("Opening 'admin-report-spam-comments pulsar stream")
        producer = get_pulsar_producer(client, 'admin-report-spam-comments', SpamComment)
        logger.info("Loading spam comments records")
        for row in get_spam_comments():
            record = SpamComment(
                comment_guid = row['comment_guid'],
                owner_guid = row['owner_guid'],
                entity_guid = row['entity_guid'],
                parent_guid_l1 = row['parent_guid_l1'],
                parent_guid_l2 = row['parent_guid_l2'],
                parent_guid_l3 = row['parent_guid_l3'],
                time_created = row['time_created'],
                spam_predict = row['spam_predict'],
                activity_views = row['activity_views'],
                last_engagement = row['last_engagement'],
                score = row['score']
            )
            producer.send(record)
    except BaseException as e:
        logger.warning(f"Spam commments exception: {type(e)} {e}: {e.args}")
        raise e
    finally:
        client.close()

    logger.info("Finished sending spam comments report")

class SpamAccount(Record):
    user_guid = Long(required=True)
    total_comments = Integer()
    spam_comments = Integer()
    spam_comments_with_link = Integer()
    spam_percent = Double()
    link_percent = Double()
    unique_spam_links = Integer()
    secs_since_most_recent_spam_comment = Integer()
    score = Double(required=True)

def get_spam_accounts():
    query = f"""
    select
        user_guid,
        total_comments,
        spam_comments,
        spam_comments_with_link,
        spam_percent::float as spam_percent,
        link_percent::float as link_percent,
        unique_spam_links,
        secs_since_most_recent_spam_comment,
        score
    from
        {SNOWFLAKE_DBT_SCHEMA}.auto_moderation_spam_accounts
    order by
        score desc
    limit 5000
    """

    with get_db().connect() as conn:
        result = conn.execute(query)
        for row in result:
            yield row

def send_spam_accounts(logger):
    logger.info("Sending spam accounts report")
    try:
        client = get_pulsar_client()
        logger.info("Opening 'admin-report-spam-accounts pulsar stream")
        producer = get_pulsar_producer(client, 'admin-report-spam-accounts', SpamAccount)
        logger.info("Loading spam accounts records")
        for row in get_spam_accounts():
            record = SpamAccount(
                user_guid = row['user_guid'],
                total_comments = row['total_comments'],
                spam_comments = row['spam_comments'],
                spam_comments_with_link = row['spam_comments_with_link'],
                spam_percent = row['spam_percent'],
                link_percent = row['link_percent'],
                unique_spam_links = row['unique_spam_links'],
                secs_since_most_recent_spam_comment = row['secs_since_most_recent_spam_comment'],
                score = row['score']
            )
            producer.send(record)
    except BaseException as e:
        logger.warning(f"Spam accounts exception: {type(e)} {e}: {e.args}")
        raise e
    finally:
        client.close()

    logger.info("Finished sending spam accounts report")

class TokenAccount(Record):
    user_guid = Long(required=True)
    time_created = Long(required=True)
    user_type = String()
    engagement_count = Integer()
    avg_engagement_score = Double()
    content_count = Integer()
    avg_content_score = Double()
    score = Double()

def get_token_accounts():
    query = f"""
    select
        user_guid::number(38) as user_guid,
        timestampdiff(millisecond, '1970-01-01 00:00:00'::timestamp_ntz, time_created) as time_created,
        user_type,
        engagement_count,
        avg_engagement_score,
        content_count,
        avg_content_score,
        score
    from
        {SNOWFLAKE_DBT_SCHEMA}.auto_moderation_token_farmers
    order by
        score desc
    limit 500
    """

    with get_db().connect() as conn:
        result = conn.execute(query)
        for row in result:
            yield row

def send_token_accounts(logger):
    logger.info("Sending token accounts report")
    try:
        client = get_pulsar_client()
        logger.info("Opening 'admin-report-token-accounts pulsar stream")
        producer = get_pulsar_producer(client, 'admin-report-token-accounts', TokenAccount)
        logger.info("Loading token accounts records")
        for row in get_token_accounts():
            record = TokenAccount(
                user_guid = row['user_guid'],
                time_created = row['time_created'],
                user_type = row['user_type'],
                engagement_count = row['engagement_count'],
                avg_engagement_score = row['avg_engagement_score'],
                content_count = row['content_count'],
                avg_content_score = row['avg_content_score'],
                score = row['score']
            )
            producer.send(record)
    except BaseException as e:
        logger.warning(f"Token accounts exception: {type(e)} {e}: {e.args}")
        raise e
    finally:
        client.close()

    logger.info("Finished sending token accounts report")

@task
def send_reports():
    logger = prefect.context.get('logger')
    send_spam_comments(logger)
    send_spam_accounts(logger)
    send_token_accounts(logger)

with Flow("notify-streams") as flow:
    send_reports()

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
