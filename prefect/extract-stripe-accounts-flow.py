from re import I
import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

import stripe
import json

from sqlalchemy import create_engine, MetaData, Table, Column
from sqlalchemy.types import String, BigInteger, DateTime
from snowflake.sqlalchemy import ARRAY, OBJECT

from os import environ as env
from datetime import datetime
from urllib.parse import quote

from cassandra_tables_metadata import get_metadata

PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()

SNOWFLAKE_STRIPE_SCHEMA = Secret('SNOWFLAKE_STRIPE_SCHEMA').get()
STRIPE_API_KEY = Secret('STRIPE_API_KEY').get()

DATA_DIR = '/root/data'

def get_metadata(schema='SNOWFLAKE_STRIPE_SCHEMA'):
    metadata = MetaData()

    Table('stripe_account_id_map', metadata,
        Column('stripe_acct_id', String(length=256)),
        Column('minds_user_guid', BigInteger),
        Column('extracted_at', DateTime)
    )
    Table('stripe_accounts', metadata,
        Column('stripe_acct_id', String(length=256)),
        Column('acct_data', String),
        Column('extracted_at', DateTime)
    )

    return metadata

def get_db():
    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_STRIPE_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)

def init_db():
    metadata = get_metadata()
    metadata.create_all(get_db())

def get_acct_ids():
    with get_db().connect() as conn:
        result = conn.execute(f"""
            select
                distinct _airbyte_data:destination::varchar as destination
            from
                {SNOWFLAKE_STRIPE_SCHEMA}._AIRBYTE_RAW_CHARGES
            where
                destination like 'acct%'
        """)
        for row in result.fetchall():
            yield row[0]

def map_stripe_account(acct_id):
    stripe.api_key = STRIPE_API_KEY
    try:
        acct = stripe.Account.retrieve(acct_id)
        user_guid = int(acct['metadata']['user_guid'])
    except:
        user_guid = None
    extracted_at = datetime.now()
    record = {
        'stripe_acct_id': acct_id,
        'minds_user_guid': user_guid,
        'extracted_at': extracted_at
    }
    return record

def process_stripe_account(account, extracted_at):
    external_count = len(account['external_accounts']['data'])
    account['external_accounts'] = external_count

    ### Uncomment to include email information in generated records
    #email = None
    #if 'individual' in account and 'email' in account['individual']:
    #    email = account['individual']['email']
    #if 'company' in account and 'email' in account['company']:
    #    email = account['company']['email']
    #account['email'] = email

    if 'individual' in account: del(account['individual'])
    if 'company' in account: del(account['company'])
    record = {
        'stripe_acct_id': account['id'],
        'acct_data': json.dumps(account),
        'extracted_at': extracted_at
    }

    return record

@task
def extract_stripe_accounts():
    logger = prefect.context.get('logger')
    logger.info("Initialising DB")
    init_db()

    logger.info("Extracting account records")
    stripe.api_key = STRIPE_API_KEY
    metadata = get_metadata()
    records = []
    extracted_at = datetime.now()
    
    accounts = stripe.Account.list(limit=100)
    for account in accounts.auto_paging_iter():
        acct_dict = account.to_dict_recursive()
        record = process_stripe_account(acct_dict, extracted_at)
        records.append(record)

    logger.info("Inserting account records")
    with get_db().connect() as conn:
        ins = metadata.tables['stripe_accounts'].insert()
        offset = 0
        while offset < len(records):
            logger.info(f"Inserting account records at offset {offset}")
            conn.execute(ins.values(records[offset:offset+1000]))
            offset += 1000

@task
def extract_stripe_acct_map():
    logger = prefect.context.get('logger')
    logger.info("Initialising DB")
    init_db()

    logger.info("Mapping account IDs")
    metadata = get_metadata()
    map = [map_stripe_account(acct_id) for acct_id in get_acct_ids()]

    logger.info("Inserting mapped IDs")
    with get_db().connect() as conn:
        ins = metadata.tables['stripe_account_id_map'].insert()
        conn.execute(ins.values(map))


with Flow("extract-stripe-accounts") as flow:
    extract_stripe_acct_map()
    extract_stripe_accounts()

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
