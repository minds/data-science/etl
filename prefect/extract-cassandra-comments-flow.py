import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

from sqlalchemy import create_engine, MetaData, ForeignKey, Table, Column, Integer, String, Boolean, DateTime
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from cassandra.concurrent import execute_concurrent_with_args
from os import environ as env
import gc
from datetime import datetime
from cassandra_metadata import get_metadata
from threading import Thread, current_thread
from multiprocessing import Process, Queue, current_process
from urllib.parse import quote

PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_CASSANDRA_SCHEMA = Secret('SNOWFLAKE_CASSANDRA_SCHEMA').get()
SNOWFLAKE_DBT_SCHEMA = Secret('SNOWFLAKE_DBT_SCHEMA').get()

REDSHIFT_HOST = Secret('REDSHIFT_HOST').get()
REDSHIFT_PORT = Secret('REDSHIFT_PORT').get()
REDSHIFT_USER = Secret('REDSHIFT_USER').get()
REDSHIFT_PASSWORD = Secret('REDSHIFT_PASSWORD').get()
REDSHIFT_DATABASE = Secret('REDSHIFT_DATABASE').get()
REDSHIFT_CASSANDRA_SCHEMA = Secret('REDSHIFT_CASSANDRA_SCHEMA').get()
REDSHIFT_DBT_SCHEMA = Secret('REDSHIFT_CASSANDRA_SCHEMA').get()
REDSHIFT_DBT_SCHEMA = Secret('REDSHIFT_DBT_SCHEMA').get()
CASSANDRA_USER = Secret('CASSANDRA_USER').get()
CASSANDRA_PASSWORD = Secret('CASSANDRA_PASSWORD').get()
CASSANDRA_HOST = Secret('CASSANDRA_HOST').get()

metadata = get_metadata(schema=SNOWFLAKE_CASSANDRA_SCHEMA)

def get_db():
    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_CASSANDRA_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)

def init_db():
    global metadata
    metadata.create_all(get_db())

def build_ranges(cluster):
    tm = cluster.metadata.token_map
    tm.rebuild_keyspace('minds', build_if_absent=True)
    ranges = []
    last = None
    for token in tm.ring:
        if last != None:
            ranges.append((last, token.value))
        last = token.value
    if last:
        ranges.append((last, 170141183460469231731687303715884105728))
    return ranges

def cassandra_values(token_ranges, conn):
    stmt = conn.prepare("select * from minds.comments where token(entity_guid) >= ? and token(entity_guid) < ?")
    for token_range in token_ranges:
        for row in conn.execute(stmt, token_range):
            yield row

def process_record(row, extracted_at):
    record = {
        'key': row.guid,
        'owner_guid': row.owner_guid,
        'entity_guid': row.entity_guid,
        'parent_guid_l1': row.parent_guid_l1 if row.parent_guid_l1 else None,
        'parent_guid_l2': row.parent_guid_l2 if row.parent_guid_l2 else None,
        'parent_guid_l3': row.parent_guid_l3 if row.parent_guid_l3 else None,
        'access_id': row.access_id,
        'deleted': row.flags['deleted'] if row.flags else None,
        'edited': row.flags['edited'] if row.flags else None,
        'mature': row.flags['mature'] if row.flags else None,
        'spam': row.flags['spam'] if row.flags else None,
        'has_children': row.has_children,
        'body': row.body[:65535] if row.body else None,
        'character_count': len(row.body) if row.body else 0,
        'replies_count': row.replies_count,
        'time_created': row.time_created,
        'time_updated': row.time_updated,
        'votes_up_count': len(row.votes_up) if row.votes_up else 0,
        'votes_down_count': len(row.votes_down) if row.votes_down else 0,
        'votes_up': str(row.votes_up[:2845]) if row.votes_up else None,
        'votes_down': str(row.votes_down[:2845]) if row.votes_down else None,
        'extracted_at': extracted_at
    }
    return record

def executor(queue, logger):
    name = f"{current_process().name}_{current_thread().name}"
    conn = get_db().connect()
    logger.info(f"{name} listening on queue\n")
    while True:
        statement = queue.get()
        if statement == None:
            queue.put(None)
            break
        try:
            conn.execute(statement)
            del statement
        except BaseException as e:
            logger.warn("{} {} {}: {}".format(name, type(e), e, e.args))
    conn.close()

def setup(queue, logger):
    logger.info("Initialising DB")
    init_db()

    logger.info("Starting Snowflake insert threads")
    Thread(target=executor, name="Thread1", args=(queue, logger)).start()
    Thread(target=executor, name="Thread2", args=(queue, logger)).start()
    Thread(target=executor, name="Thread3", args=(queue, logger)).start()
    Thread(target=executor, name="Thread4", args=(queue, logger)).start()
    logger.info("Threads started")

    return queue

@task
def extract():
    global ringsize, ring, next_entry, keys, schema, table_buffers, fields, metadata
    queue = Queue()
    logger = prefect.context.get("logger")
    logger.info("Setting up")
    setup(queue, logger)
    logger.info("Setup complete, connecting to cassandra")

    auth_provider = PlainTextAuthProvider(username=CASSANDRA_USER, password=CASSANDRA_PASSWORD)
    cluster = Cluster([CASSANDRA_HOST], auth_provider=auth_provider, prepare_on_all_hosts = True)
    cass = cluster.connect()
    logger.info("Cassandra connection complete, starting extract")

    extracted_at = datetime.now()

    records = []
    ins = metadata.tables[f"{SNOWFLAKE_CASSANDRA_SCHEMA}.comments"].insert()
    try:
        for row in cassandra_values(build_ranges(cluster), cass):
            record = process_record(row, extracted_at)
            records.append(record)
            if len(records) >= 10000:
                queue.put(ins.values(records))
                del records
                records = []
    except BaseException as e:
        logger.warn(f"Unable to process because:")
        logger.warn(f"{type(e)} {e}: {e.args}")
    logger.info("Extract complete, flushing records")
    queue.put(ins.values(records))
        
    logger.info("Record flush complete, closing queue")
    queue.put(None)

with Flow("extract-cassandra-comments") as flow:
    extract()

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
