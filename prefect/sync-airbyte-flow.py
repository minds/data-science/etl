import prefect
from prefect import task, Flow, Parameter
import requests
from time import sleep

import os
PROJECT_NAME = os.getenv("PREFECT_PROJECT_NAME")

ENDPOINT="http://airbyte-server:8001/api/v1"

@task
def monitor_sync(jobs_ids):
    logger = prefect.context.get("logger")

    while len(jobs_ids):
        done_list = []
        for connection, job_id in jobs_ids.items():
            response = requests.post(
                ENDPOINT+'/jobs/get',
                json = {
                    'id': job_id
                }
            )
            if response.status_code != 200:
                logger.error("{} status code {}: {}".format(connection, response.status_code, response.reason))
                del(jobs_ids[connection])
                continue
            json = response.json()

            status = json["job"]["status"]
            attempts = len(json["attempts"])
            logger.info("Connection {} status: {}, {} attempt(s)".format(connection, status, attempts))

            if status != "pending" and status != "running":
                done_list.append(connection)

        for connection in done_list:
            del(jobs_ids[connection])

        sleep(15)

@task
def start_sync(connections):
    if connections and connections > '':
        connection_list = connections.split(';')
    else:
        return

    jobs_ids = {}
    
    logger = prefect.context.get("logger")

    for connection in connection_list:
        logger.info("Syncing connection {}".format(connection))
        response = requests.post(
            ENDPOINT+'/connections/sync',
            json = {
                'connectionId': connection
            }
        )
        if response.status_code != 200:
            logger.error("Connection {} status code {}: {}".format(connection, response.status_code, response.reason))
            logger.info(response.json())
        else:
            json = response.json()
            id = json["job"]["id"]
            jobs_ids[connection] = id
            logger.info("Started connection {}, job id: {}".format(connection, id))

    
    return jobs_ids

with Flow("sync-airbyte") as flow:
    connections = Parameter('connections', default='')
    monitor_sync(start_sync(connections=connections))

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])