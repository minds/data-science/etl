import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

from sqlalchemy import create_engine, MetaData, ForeignKey, Table, Column, Integer, String, Boolean, DateTime
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from cassandra.concurrent import execute_concurrent_with_args
from cassandra.policies import WhiteListRoundRobinPolicy, DCAwareRoundRobinPolicy
from os import environ as env
from os import remove
from datetime import datetime
from cassandra_entities_metadata import get_metadata
from threading import Thread, current_thread
from multiprocessing import Process, Queue, current_process
from urllib.parse import quote

PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_CASSANDRA_SCHEMA = Secret('SNOWFLAKE_CASSANDRA_SCHEMA').get()
SNOWFLAKE_DBT_SCHEMA = Secret('SNOWFLAKE_DBT_SCHEMA').get()

CASSANDRA_USER = Secret('CASSANDRA_USER').get()
CASSANDRA_PASSWORD = Secret('CASSANDRA_PASSWORD').get()
CASSANDRA_HOST = Secret('CASSANDRA_HOST').get()

DATA_DIR = '/root/data'

ringsize = 1000
ring = [None] * ringsize
next_entry = 0
keys = {}

table_buffers = {}
table_batches = {}
table_headers = {}

fields = {}

metadata = get_metadata(schema=SNOWFLAKE_CASSANDRA_SCHEMA)

def get_db():
    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_CASSANDRA_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)

def init_db():
    global metadata
    metadata.create_all(get_db())

def init_buffers():
    global table_buffers, table_batches, table_headers, metadata

    for table in metadata.sorted_tables:
        table_buffers[table.name] = []
        table_batches[table.name] = 1
        header = []
        for column_meta in table.columns:
            header.append(column_meta.name)
        table_headers[table.name] = header

def init_fields():
    global fields, metadata
    
    fields["type"] = True
    fields["subtype"] = True
    for table in metadata.sorted_tables:
        for column in table.columns:
            fields[column.name] = True

def build_ranges(cluster):
    tm = cluster.metadata.token_map
    tm.rebuild_keyspace('minds', build_if_absent=True)
    ranges = []
    last = None
    for token in tm.ring:
        if last != None:
            ranges.append((last, token.value))
        last = token.value
    if last:
        ranges.append((last, 170141183460469231731687303715884105728))
    return ranges

def cassandra_values(token_ranges, conn):
    stmt = conn.prepare("select * from minds.entities where token(key) >= ? and token(key) < ?")
    for (success, result) in execute_concurrent_with_args(conn, stmt, token_ranges, concurrency=6, results_generator=True):
        if success:
            for row in result:
                yield (row[0], row[1], row[2])
        else:
            raise result

def to_boolean(value):
    lower_value = value.lower()
    for string in ['1', 'true', 'yes', '1645600380']: # Joe Rogan is pro
        if string == lower_value:
            return True
    for string in ['0', 'false', 'no']:
        if string == lower_value:
            return False
    return None

def to_datetime(value):
    try:
        int_value = int(value)
        return str(datetime.fromtimestamp(int_value))
    except:
        return str(value)

def to_integer(value):
    try:
        i = int(value)
        if i > 9223372036854775807 or i < -9223372036854775808:
            return None
        else:
            return int(value)
    except:
        return None

def to_string(value, size):
    escaped = str(value).replace('\r', '').replace('\\', '\\\\').replace(',','\\,')[0:size]
    while escaped[-1:] == '\\':
        escaped = escaped[0:-1]
    return escaped

def to_map(value):
    try: 
        map = {}
        for k, v in value.items():
            if isinstance(v, int) or isinstance(v, float):
                map[str(k)] = v
            elif v == None:
                map[str(k)] = False
            else:
                map[str(k)] = str(v)
        return str(map).replace(',','\\,')
    except:
        return None

def to_array(value):
    try:
        array = []
        for e in value:
            array.append(e)
        return str(array).replace(',','\\,')
    except:
        return None

def make_batch(table_name, header, records, extracted_at, batch_number):
    return {
        'filename': f"{table_name}_{extracted_at.strftime('%Y%m%d-%H%M%S')}_{batch_number:>04d}.csv",
        'table': table_name,
        'header': header,
        'records': records
    }

def flush_key(index, queue, logger):
    global ringsize, ring, next_entry, keys, schema, table_buffers, table_batches, table_headers, fields, metadata
    key = ring[index]
    rows = keys[key]
    attrs = {}
    for row in rows:
        key, attr, value = row
        attrs[attr] = value
    table = ""
    extracted_at = datetime.now()
    if not "type" in attrs and not ("deleted" in attrs and attrs["deleted"] == "1"):
        table = "unmatched"
        buffer = table_buffers[table]
        for row in rows:
            record = {}
            record["key"], record["attr"], record["value"] = row
            record["value"] = to_string(record["value"], 65535)
            record["extracted_at"] = extracted_at
            buffer.append(record)
    elif "type" in attrs:
        table = attrs["type"]
        if "subtype" in attrs and attrs["subtype"]:
            table += '_'+attrs["subtype"]
        if table in table_buffers and not (table == "user" and not "username" in attrs):
            buffer = table_buffers[table]

            record = {}
            for column_meta in metadata.tables[f"{SNOWFLAKE_CASSANDRA_SCHEMA}.{table}"].columns:
                column = column_meta.name
                if column in attrs and attrs[column]:
                    value = attrs[column]
                    col_type = str(column_meta.type)
                    if col_type == 'INTEGER' or col_type == 'BIGINT':
                        value = to_integer(value)
                    elif col_type[0:7] == 'VARCHAR':
                        if len(col_type[8:-1]) > 0:
                            size = int(col_type[8:-1])
                        else:
                            size = 65535
                        value = to_string(value, size)
                    elif col_type == 'BOOLEAN':
                        value = to_boolean(value)
                    elif col_type == 'DATETIME':
                        value = to_datetime(value)
                    elif col_type == 'OBJECT':
                        value = to_map(value)
                    elif col_type == 'ARRAY':
                        value = to_array(value)
                    record[column] = value
                else:
                    record[column] = None

            record["key"] = to_integer(key)
            record["extracted_at"] = datetime.now()

            buffer.append(record)
    
    if table in table_buffers and len(table_buffers[table]) >= 100000:
        logger.info(f"Flushing {len(table_buffers[table])} records for {table} to queue ({queue.qsize()} entries)")
        queue.put( make_batch(table, table_headers[table], table_buffers[table], extracted_at, table_batches[table]) )
        table_batches[table] += 1

        del(table_buffers[table])
        table_buffers[table] = []

    del(keys[key])
    ring[index] = None

def new_key(key, queue, logger):
    global ringsize, ring, next_entry, keys
    index = next_entry % ringsize
    if ring[index]:
        flush_key(index, queue, logger)
    ring[index] = key
    keys[key] = []
    next_entry += 1

def write_batch(file, batch):
    file.write(','.join(batch['header'])+'\r\n')
    for record in batch['records']:
        file.write(','.join(map(lambda column: str(record[column]) if record[column] else '', batch['header'])) + '\r\n')

def executor(queue, logger):
    name = f"{current_process().name}_{current_thread().name}"
    logger.info(f"{name} listening on queue\n")
    while True:
        batch = queue.get()
        if batch == None:
            queue.put(None)
            break

        try:
            logger.info(f"Writing CSV file {DATA_DIR}/{batch['filename']}")
            with open(f"{DATA_DIR}/{batch['filename']}", 'w', encoding='UTF8', newline='') as f:
                write_batch(f, batch)

            with get_db().connect() as conn:
                logger.info(f"Staging CSV file file:///{DATA_DIR}/{batch['filename']}")
                conn.execute(f"put file:///{DATA_DIR}/{batch['filename']} @{SNOWFLAKE_CASSANDRA_SCHEMA}.staging")

                logger.info(f"Copying staged file {batch['filename']} into table {batch['table'].upper()}")
                conn.execute(f"""copy into {SNOWFLAKE_CASSANDRA_SCHEMA}."{batch['table'].upper()}" from @{SNOWFLAKE_CASSANDRA_SCHEMA}.staging/{batch['filename']} file_format = (type=csv skip_header=1 record_delimiter='\\r\\n' escape='\\\\' empty_field_as_null=TRUE encoding=UTF8 )""")

            logger.info(f"Removing CSV file /{DATA_DIR}/{batch['filename']}")
            remove(f"/{DATA_DIR}/{batch['filename']}")
        except BaseException as e:
            logger.warning(f"{batch['filename']} {type(e)} {e}: {e.args}")
        finally:
            del batch

def launcher(queue, logger):
    Thread(target=executor, name="Thread1", args=(queue, logger)).start()
    Thread(target=executor, name="Thread2", args=(queue, logger)).start()

def setup(queue, logger):
    logger.info("Initialising DB")
    init_db()
    logger.info("Initialising fields")
    init_fields()
    logger.info("Initialising buffers")
    init_buffers()

    logger.info("Starting Snowflake insert threads")
    Process(target=launcher, name="Process1", args=(queue, logger)).start()
    logger.info("Threads started")

    return queue

@task
def extract_all_entities():
    global ringsize, ring, next_entry, keys, schema, table_buffers, fields, metadata
    queue = Queue()
    logger = prefect.context.get("logger")
    logger.info("Setting up")
    setup(queue, logger)
    logger.info("Setup complete, connecting to cassandra")

    auth_provider = PlainTextAuthProvider(username=CASSANDRA_USER, password=CASSANDRA_PASSWORD)
    cluster = Cluster([CASSANDRA_HOST], auth_provider=auth_provider, prepare_on_all_hosts = True, 
        control_connection_timeout=120, connect_timeout=120,
        protocol_version=4, load_balancing_policy=DCAwareRoundRobinPolicy()
    )
    cass = cluster.connect()
    logger.info("Cassandra connection complete, starting extract")

    token_ranges = build_ranges(cluster)
    offset = 0
    increment = 6
    while offset < len(token_ranges):
        logger.info(f"Processing token ranges {offset} - {offset+increment}")
        try:
            for row in cassandra_values(token_ranges[offset:offset+increment], cass):
                key, attr, value = row
                attr = attr.replace(':', '_')
                if attr in fields:
                    if not key in keys:
                        new_key(key, queue, logger)
                    keys[key].append((key, attr, value))
        except BaseException as e:
            logger.warn(f"Unable to process ranges {offset} - {offset+increment} because:")
            logger.warn(f"{type(e)} {e}: {e.args}")
            cass = cluster.connect()
        finally:
            logger.info(f"Finished processing token ranges {offset} - {offset+increment}")

        offset += increment

    logger.info("Extract complete, flushing key buffer")
    for x in range(0,ringsize):
        index = (next_entry + x) % ringsize
        if ring[index]:
            flush_key(index, queue, logger)
        
    logger.info("Key flush complete, flushing tables")
    extracted_at = datetime.now()
    for table in table_buffers.keys():
        if len(table_buffers[table]) > 0:
            logger.info(f"Flushing {len(table_buffers[table])} records for {table} to queue ({queue.qsize()} entries)")
            queue.put( make_batch(table, table_headers[table], table_buffers[table], extracted_at, table_batches[table]) )

    logger.info("Table flush complete, closing queue")
    queue.put(None)

with Flow("extract-cassandra") as flow:
    extract_all_entities()

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
