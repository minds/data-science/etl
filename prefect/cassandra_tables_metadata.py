from sqlalchemy import Float, MetaData, Table, Column, Numeric, BigInteger, Integer, String, Boolean, DateTime
from snowflake.sqlalchemy import ARRAY, OBJECT

### WARNING: The first column of each table below MUST BE the partition key for the Cassandra table ###
### WARNING: The order of columns much match exactly the column order of the corresponding Snowflake table ###

def get_metadata(schema='cassandra_extract'):
    metadata = MetaData(schema=schema)

    Table('blockchain_transactions_mainnet', metadata,
        Column("user_guid", BigInteger),
        Column("timestamp", DateTime),
        Column("wallet_address", String(length=65535)),
        Column("tx", String(length=65535)),
        Column("amount", Numeric(38)),
        Column("completed", Boolean),
        Column("contract", String(length=65535)),
        Column("data", String(length=65535)),
        Column("failed", Boolean),
        Column("extracted_at", DateTime)
    )

    Table("contributions", metadata,
        Column("user_guid", BigInteger),
        Column("timestamp", DateTime),
        Column("metric", String(length=65535)),
        Column("amount", BigInteger),
        Column("score", BigInteger),
        Column("extracted_at", DateTime)
    )

    Table("rewards_withdrawals", metadata,
        Column("user_guid", BigInteger),
        Column("timestamp", DateTime),
        Column("tx", String(length=65535)),
        Column("address", String(length=65535)),
        Column("amount", Numeric(38)),
        Column("completed", Boolean),
        Column("completed_tx", String(length=65535)),
        Column("gas", BigInteger),
        Column("status", String(length=65535)),
        Column("extracted_at", DateTime)
    )

    Table("token_rewards", metadata,
        Column("user_guid", BigInteger),
        Column("reward_type", String(length=65535)),
        Column("date", DateTime),
        Column("multiplier", Float),
        Column("payout_tx", String(length=65535)),
        Column("score", Float),
        Column("token_amount", Float),
        Column("tokenomics_version", BigInteger),
        Column("extracted_at", DateTime)
    )

    Table("wire", metadata,
        Column("receiver_guid", BigInteger),
        Column("method", String(length=65535)),
        Column("timestamp", DateTime),
        Column("entity_guid", BigInteger),
        Column("wire_guid", BigInteger),
        Column("amount", BigInteger),
        Column("recurring", Boolean),
        Column("sender_guid", BigInteger),
        Column("status", String(length=65535)),
        Column("wei", Numeric(38)),
        Column("extracted_at", DateTime)
    )

    Table('comments', metadata,
        Column('entity_guid', BigInteger),
        Column('owner_guid', BigInteger),
        Column('guid', BigInteger),
        Column('container_guid', BigInteger),
        Column('parent_guid_l1', BigInteger),
        Column('parent_guid_l2', BigInteger),
        Column('parent_guid_l3', BigInteger),
        Column('access_id', BigInteger),
        Column('body', String(length=65535)),
        Column('flags', OBJECT),
        Column('has_children', Boolean),
        Column('replies_count', Integer),
        Column('time_created', DateTime),
        Column('time_updated', DateTime),
        Column('votes_up', ARRAY),
        Column('votes_down', ARRAY),
        Column('extracted_at', DateTime)
    )

    Table('boosts', metadata,
        Column('type', String(length=256)),
        Column('guid', BigInteger),
        Column('owner_guid', BigInteger),
        Column('destination_guid', BigInteger),
        Column('state', String(length=256)),
        Column('data', String(65535)),
        Column('extracted_at', DateTime)
    )

    Table('friends', metadata,
        Column('key', BigInteger),
        Column('column1', BigInteger),
        Column('value', BigInteger),
        Column('extracted_at', DateTime)
    )

    Table('reports', metadata,
        Column('guid', BigInteger),
        Column('owner_guid', BigInteger),
        Column('reporter_guid', BigInteger),
        Column('entity_guid', BigInteger),
        Column('entity_luid', String(length=1024)),
        Column('action', String(length=256)),
        Column('state', String(length=256)),
        Column('reason', String(length=256)),
        Column('reason_note', String(length=2048)),
        Column('appeal_note', String(length=2048)),
        Column('time_created', DateTime),
        Column('extracted_at', DateTime)
    )

    Table('moderation_reports', metadata,
        Column('entity_urn', String(length=256)),
        Column('entity_owner_guid', BigInteger),
        Column('reason_code', Integer),
        Column('sub_reason_code', Integer),
        Column('timestamp', DateTime),
        Column('state', String(length=256)),
        Column('state_changes', OBJECT),
        Column('reports', ARRAY),
        Column('initial_jury', OBJECT),
        Column('appeal_jury', OBJECT),
        Column('appeal_note', String(length=2048)),
        Column('uphold', Boolean),
        Column('user_hashes', ARRAY),
        Column('extracted_at', DateTime)
    )

    Table('pseudo_user_hashtags', metadata,
        Column('pseudo_id', String(length=22)),
        Column('hashtag', String(length=256)),
        Column('extracted_at', DateTime)
    )

    Table('user_hashtags', metadata,
        Column('user_guid', BigInteger),
        Column('hashtag', String(length=256)),
        Column('extracted_at', DateTime)
    )

    Table('social_compass_answers', metadata,
        Column('user_guid', BigInteger),
        Column('question_id', String(length=256)),
        Column('current_value', Integer),
        Column('extracted_at', DateTime)
    )

    Table('wire_support_tier', metadata,
        Column('entity_guid', BigInteger),
        Column('guid', BigInteger),
        Column('description', String(length=65535)),
        Column('has_tokens', Boolean),
        Column('has_usd', Boolean),
        Column('name', String(length=256)),
        Column('public', Boolean),
        Column('usd', Float),
        Column('extracted_at', DateTime)
    )


    return metadata
