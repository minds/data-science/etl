from sqlalchemy import Float, MetaData, Table, Column, Numeric, BigInteger, Integer, String, Boolean, DateTime
from snowflake.sqlalchemy import ARRAY, OBJECT

### WARNING: The order of columns much match exactly the column order of the corresponding Snowflake table ###

def get_metadata(schema='vitess_extract'):
    metadata = MetaData(schema=schema)

    Table('superminds', metadata,
        Column("guid", BigInteger),
        Column("activity_guid", BigInteger),
        Column("reply_activity_guid", BigInteger),
        Column("sender_guid", BigInteger),
        Column("receiver_guid", BigInteger),
        Column("status", Integer),
        Column("payment_amount", Float),
        Column("payment_method", Integer),
        Column("payment_reference", String(length=1024)),
        Column("created_timestamp", DateTime),
        Column("updated_timestamp", DateTime),
        Column("twitter_required", Boolean),
        Column("reply_type", Integer),
        Column("extracted_at", DateTime)
    )

    Table('boosts', metadata,
        Column("guid", BigInteger),
        Column("owner_guid", BigInteger),
        Column("entity_guid", BigInteger),
        Column("target_suitability", Integer),
        Column("target_location", Integer),
        Column("payment_method", Integer),
        Column("payment_amount", Float),
        Column("payment_tx_id", String),
        Column("daily_bid", Float),
        Column("duration_days", Integer),
        Column("status", Integer),
        Column("created_timestamp", DateTime),
        Column("updated_timestamp", DateTime),
        Column("approved_timestamp", DateTime),
        Column("extracted_at", DateTime)
    )

    return metadata
