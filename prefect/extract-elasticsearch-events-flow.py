import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

from sqlalchemy import create_engine
from threading import Thread, current_thread
from multiprocessing import Process, Queue, current_process
from time import sleep
from os import environ as env
from os import remove
from datetime import datetime
from urllib.parse import quote
import requests

from cassandra_tables_metadata import get_metadata

PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_ES_SCHEMA = Secret('SNOWFLAKE_ES_SCHEMA').get()

ES_USER = env.get('PREFECT__CONTEXT__SECRETS__ES_USER')
ES_PASSWORD = env.get('PREFECT__CONTEXT__SECRETS__ES_PASSWORD')
ES_BASE = 'https://a7767ca99088a42d58a098b8795bb42f-febc0d643005d91f.elb.us-east-1.amazonaws.com:9200'
ES_METRICS_FEED = 'minds-metrics-*'

DATA_DIR = '/root/data'

def get_db():
    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_ES_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)

def write_batch(file, batch):
    file.write(','.join(batch['header'])+'\n')
    for record in batch['records']:
        record['_extracted_at_millis'] = batch['extracted_at']
        file.write(','.join(map(lambda column: str(record[column]) if record[column] else '', batch['header'])) + '\n')

def executor(queue, logger):
    name = f"{current_process().name}_{current_thread().name}"
    logger.info(f"{name} listening on queue\n")
    while True:
        batch = queue.get()
        if batch == None:
            queue.put(None)
            break

        try:
            logger.info(f"Writing CSV file {DATA_DIR}/{batch['filename']}")
            with open(f"{DATA_DIR}/{batch['filename']}", 'w', encoding='UTF8', newline='') as f:
                write_batch(f, batch)

            with get_db().connect() as conn:
                logger.info(f"Staging CSV file file:///{DATA_DIR}/{batch['filename']}")
                res = conn.execute(f"put file:///{DATA_DIR}/{batch['filename']} @{SNOWFLAKE_ES_SCHEMA}.staging")
                logger.info(f"Staging result: {res.fetchone()}")

                logger.info(f"Copying staged file {batch['filename']} into table {batch['table']}")
                res = conn.execute(f"""copy into {SNOWFLAKE_ES_SCHEMA}.{batch['table']} from @{SNOWFLAKE_ES_SCHEMA}.staging/{batch['filename']} file_format = (type=csv skip_header=1 record_delimiter='\\n' escape='\\\\' empty_field_as_null=TRUE encoding=UTF8 )""")
                logger.info(f"Copying result: {res.fetchone()}")

            logger.info(f"Removing CSV file /{DATA_DIR}/{batch['filename']}")
            remove(f"/{DATA_DIR}/{batch['filename']}")
        except BaseException as e:
            logger.warn(f"{batch['filename']} {type(e)} {e}: {e.args}")
        finally:
            del batch

def launcher(queue, logger):
    Thread(target=executor, name="Thread1", args=(queue, logger)).start()

def setup(queue, logger):
    logger.info("Starting Snowflake insert threads")
    Process(target=launcher, name="Process1", args=(queue, logger)).start()
    logger.info("Threads started")

    return queue

def make_batch(records, extracted_at, batch_number):
    header = [
        'user_guid',
        'action',
        'timestamp',
        'millis',
        'entity_type',
        'entity_guid',
        'entity_container_guid',
        'entity_owner_guid',
        '_extracted_at_millis'
    ]
    extracted_at_string = datetime.fromtimestamp(extracted_at/1000).strftime('%Y%m%d-%H%M%S')
    return {
        'filename': f"engagement_events_{extracted_at_string}_{batch_number:>04d}.csv",
        'table': 'engagement_events',
        'extracted_at': extracted_at,
        'header': header,
        'records': records
    }

def es_search(query):
    resp = requests.post(
        f"{ES_BASE}/{ES_METRICS_FEED}/_search?scroll=1m",
        headers = { 'Content-Type': 'application/json' },
        auth = (ES_USER,ES_PASSWORD),
        data = query,
        verify = False
    )
    if resp.status_code == 400:
        print(f"Response 400:\n{resp.text}")
        return None
    else:
        return resp
    
def es_scroll(scroll_id):
    body = f"""{{ "scroll": "1m", "scroll_id": "{scroll_id}" }}\n"""
    resp = requests.post(
        f"{ES_BASE}/_search/scroll",
        headers = { 'Content-Type': 'application/json' },
        auth = (ES_USER,ES_PASSWORD),
        data = body,
        verify = False
    )
    if resp.status_code == 400:
        print(f"Response 400:\n{resp.text}")
        return None
    else:
        return resp

def get_last_extract():
    with get_db().connect() as conn:
        res = conn.execute(f"""select max(_extracted_at_millis) from {SNOWFLAKE_ES_SCHEMA}.engagement_events""")
        last_extract = res.fetchone()[0]
    return last_extract if last_extract != None else 1664578800000

@task
def extract_events():
    queue = Queue()
    logger = prefect.context.get("logger")
    logger.info("Setting up")
    setup(queue, logger)

    logger.info(f"Initializing extract")
    last_extract = get_last_extract()
    max_extract = last_extract + (14 * 24 * 3600 * 1000)
    extracted_at = int(datetime.now().timestamp()/60)*60000
    if extracted_at > max_extract:
        extracted_at = max_extract
    query = {
        'size': 5000,
        'sort': [ {'@timestamp': {'order': 'asc'}}],
        'query': {
            'bool': {
                'must': [
                    {'term': {'type': 'action'}},
                ],
                'must_not': [
                    {'term': {'action': 'boost'}},
                    {'term': {'action': 'pageview'}},
                    {'term': {'action': 'vote:up:cancel'}},
                    {'term': {'action': 'vote:down:cancel'}},
                    {'term': {'action': 'delete'}},
                    {'term': {'action': 'block'}},
                    {'term': {'action': 'subscribe'}},
                    {'term': {'action': 'unsubscribe'}},
                    {'term': {'action': 'joined'}},
                    {'term': {'action': 'join'}},
                    {'term': {'action': 'signup'}},
                    {'term': {'action': 'email:confirm'}},
                    {'term': {'action': 'email:clicks'}},
                    {'term': {'action': 'email:unsubscribe'}},
                    {'term': {'action': 'login'}},
                    {'term': {'action': 'block'}},
                    {'term': {'action': 'unblock'}},
                    {'term': {'action': 'unlock'}},
                    {'term': {'action': 'referral'}},
                    {'term': {'action': 'accept'}},
                    {'term': {'action': 'reject'}},
                    {'term': {'action': 'ban'}},
                    {'term': {'action': 'jury_duty'}},
                    {'term': {'action': 'active'}}
                ],
                'filter': [
                    {'range': {'@timestamp': {'gte': last_extract}}},
                    {'range': {'@timestamp': {'lt': extracted_at}}}

                ]
            }
        }
    }
    engagement = {
        'vote:up': True,
        'vote:down': True,
        'comment': True,
        'remind': True
    }

    records = []
    batch_number = 1

    logger.info(f"Starting extract")
    total_records = 0
    offset = 0

    resp = es_search(str(query).replace("'",'"'))
    if '_scroll_id' in resp.json():
        scroll_id = resp.json()['_scroll_id']
    else:
        scroll_id = None

    while resp != None:
        documents = resp.json()['hits']['hits']
        for doc in documents:
            event = doc['_source']
            if not event['action'] in engagement:
                continue
            record = {
                'user_guid': event['user_guid'],
                'action': event['action'],
                'timestamp': str(datetime.fromtimestamp(event['@timestamp']/1000)),
                'millis': event['@timestamp'],
                'entity_type': event['entity_type'] if 'entity_type' in event else '',
                'entity_guid': event['entity_guid'] if 'entity_guid' in event else '',
                'entity_container_guid': event['entity_container_guid'] if 'entity_container_guid' in event else '',
                'entity_owner_guid': event['entity_owner_guid'] if 'entity_owner_guid' in event else ''
            }
            
            records.append(record)

            if len(records) >= 200000:
                queue.put( make_batch(records, extracted_at, batch_number) )
                total_records += len(records)
                logger.info(f"Added batch of {len(records)}, {total_records} total, queue size: {queue.qsize()}, token range offset: {offset}")
                batch_number += 1
                records = []

        if scroll_id != None and len(documents) > 0:
            resp = es_scroll(scroll_id)
        else:
            resp = None

    logger.info(f"Extract complete, flushing {len(records)} records")
    if len(records) > 0:
        queue.put( make_batch(records, extracted_at, batch_number) )

    while not queue.empty():
        logger.info(f"{queue.qsize()} batches in queue")
        sleep(10)

    logger.info("All batches processed, closing queue")
    queue.put(None)

with Flow("extract-elasticsearch-events") as flow:
    extract_events()

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
