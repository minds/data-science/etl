import prefect
from prefect import task, Flow
from prefect.tasks.prefect import StartFlowRun

import os
PROJECT_NAME = os.getenv("PREFECT_PROJECT_NAME")

update_snowplow = StartFlowRun(flow_name='extract-snowplow-records', project_name=PROJECT_NAME, wait=True)
cassandra_incremental = StartFlowRun(flow_name='extract-cassandra-incremental', project_name=PROJECT_NAME, wait=True) 
cassandra_tables = StartFlowRun(flow_name='extract-cassandra-tables', project_name=PROJECT_NAME, wait=True)
vitess_tables = StartFlowRun(flow_name='extract-vitess-tables', project_name=PROJECT_NAME, wait=True)
elastic_events = StartFlowRun(flow_name='extract-elasticsearch-events', project_name=PROJECT_NAME, wait=True)
stripe_accounts = StartFlowRun(flow_name='extract-stripe-accounts', project_name=PROJECT_NAME, wait=True)

run_dbt = StartFlowRun(flow_name='run-dbt', project_name=PROJECT_NAME, wait=True)

update_recommendations = StartFlowRun(flow_name='update-recommendations', project_name=PROJECT_NAME, wait=True)
update_activity_recommendations = StartFlowRun(flow_name='update-activity-recommendations', project_name=PROJECT_NAME, wait=True)
extract_user_score = StartFlowRun(flow_name='extract-user-score-quality', project_name=PROJECT_NAME, wait=True)

comment_classification_model = StartFlowRun(flow_name='create-comment-classification-model', project_name=PROJECT_NAME, wait=True)
classify_comments = StartFlowRun(flow_name='classify-new-comments', project_name=PROJECT_NAME, wait=True)

#identify_communities = StartFlowRun(flow_name='identify-user-communities', project_name=PROJECT_NAME, wait=True)

notify_streams = StartFlowRun(flow_name='notify-streams', project_name=PROJECT_NAME, wait=True)


with Flow('all-etl') as flow:
    cass_inc = cassandra_incremental(upstream_tasks=[update_snowplow])
    dbt = run_dbt(upstream_tasks=[cass_inc, cassandra_tables, vitess_tables, elastic_events, stripe_accounts]) 

    comment_model = comment_classification_model(upstream_tasks=[dbt])
    classify = classify_comments(upstream_tasks=[comment_model])

    #identify_communities(upstream_tasks=[dbt])

    recommendations = update_recommendations(upstream_tasks=[dbt])
    activity_recommendations = update_activity_recommendations(upstream_tasks=[dbt])
    user_score = extract_user_score(upstream_tasks=[recommendations])

    notify_streams(upstream_tasks=[user_score, classify])

flow.register(project_name=PROJECT_NAME, labels=['data-etl']) 