import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

from sqlalchemy import create_engine

import re
import os
from os import environ as env
from urllib.parse import quote

import pandas as pd

from nltk.stem.porter import PorterStemmer

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
import pickle


PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_CASSANDRA_SCHEMA = Secret('SNOWFLAKE_CASSANDRA_SCHEMA').get()
SNOWFLAKE_DBT_SCHEMA = Secret('SNOWFLAKE_DBT_SCHEMA').get()

ARTIFACTS_PATH = "/root/data"


def get_db():
    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_CASSANDRA_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)


def comment_sample():
    """
    Returns a training dataset, using both reported and non-reported comments
    """

    conn = get_db().connect()

    comment_sample_df = pd.read_sql(
    f"""
        with base as (
        select
            entity_urn as key,
            is_upheld
        from
            {SNOWFLAKE_DBT_SCHEMA}.stg_moderation_reports
        where
            entity_type = 'comment'
            and reason_code = 8
            and valid_to is null
        ),

        spam as (
        select distinct
            comments.user_guid,
            comments.body as comment,
            1 as is_spam
        from
            base
        inner join
            {SNOWFLAKE_DBT_SCHEMA}.dmn_comments as comments
                on comments.comment_guid::varchar = base.key::varchar
        where
            comments.body like '%$%' or
            base.is_upheld = true
        ),

        authentic as (
        select distinct
            comments.user_guid,
            comments.body as comment,
            count(distinct comments.entity_guid) as comment_count,
            0 as is_spam
        from
            {SNOWFLAKE_DBT_SCHEMA}.dmn_comments as comments
        where
            len(comments.body) > 25
        group by 1,2
        having
            comment_count < 5
        order by
            random()
        limit 20000
        )

        select user_guid as user_id, comment, is_spam from spam
        union
        select user_guid as user_id, comment, is_spam from authentic""", conn)

    conn.close()

    return comment_sample_df.dropna().reset_index()


@task
def create_model():
    from nltk.corpus import stopwords

    logger = prefect.context.get("logger")
    logger.info("Creating training data set...")

    corpus = []
    comment_sample_df = comment_sample()

    logger.info("Tokenising Comments...")
    for i in range(0, len(comment_sample_df)):
        comment = re.sub("[^a-zA-Z0-9]", " ", comment_sample_df["comment"][i])
        comment = comment.lower()
        comment = comment.split()
        pe = PorterStemmer()
        stopword = stopwords.words("english")
        comment = [pe.stem(word) for word in comment if not word in set(stopword)]
        comment = " ".join(comment)
        corpus.append(comment)

    logger.info("Creating X and y training variables...")
    cv = CountVectorizer()
    cv.fit(corpus)
    pickle.dump(cv, open(os.path.join(ARTIFACTS_PATH, 'count_vectorizer.pkl'), 'wb'))
    X = cv.transform(corpus).toarray()
    y = comment_sample_df['is_spam']

    lrc = LogisticRegression(solver='liblinear', penalty='l1')
    logger.info("Fitting model...")
    lrc.fit(X, y)

    logger.info("Comment classification model created")

    return lrc


@task
def save_model(model):
    logger = prefect.context.get("logger")
    logger.info("Saving model to Pickle file")

    pickle.dump(model, open(os.path.join(ARTIFACTS_PATH, 'comment_classifier.pkl'), 'wb'))


with Flow("create-comment-classification-model") as flow:
    save_model(create_model())

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
