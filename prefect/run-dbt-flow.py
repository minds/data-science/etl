import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret
from prefect.tasks.shell import ShellTask
from prefect.tasks.dbt import DbtShellTask

import os
PROJECT_NAME = os.getenv("PREFECT_PROJECT_NAME")

# Data Warehouse credentials stored in environment (.env or Doppler)
SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_DBT_SCHEMA = Secret('SNOWFLAKE_DBT_SCHEMA').get()

DBT_ENVIRONMENT = Secret('DBT_ENVIRONMENT').get()

logger = prefect.context.get("logger")

shell = ShellTask(helper_script="cd /root/data-etl")

dbt = DbtShellTask(
    profile_name='minds_analytics', 
    environment=DBT_ENVIRONMENT,
    dbt_kwargs={
        'type': 'snowflake',
        'account': SNOWFLAKE_ACCOUNT,
        'user': SNOWFLAKE_USER,
        'password': SNOWFLAKE_PASSWORD,
        'role': SNOWFLAKE_ROLE,
        'warehouse': SNOWFLAKE_WAREHOUSE,
        'database': SNOWFLAKE_DATABASE,
        'schema': SNOWFLAKE_DBT_SCHEMA,
        'threads': 6
    },
    helper_script="cd /root/data-etl/dbt",
    overwrite_profiles=True,
    return_all=True,
    log_stderr=True
)

with Flow('run-dbt') as flow:
    git_pull = shell(command="git pull")
    dbt_deps = dbt(command="dbt deps", upstream_tasks=[git_pull])
    dbt_seed = dbt(command="dbt seed", upstream_tasks=[dbt_deps])
    dbt_run = dbt(command="dbt run", upstream_tasks=[dbt_seed])

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
