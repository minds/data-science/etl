import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

from sqlalchemy import create_engine, MetaData, Table, Index, Column, Integer, BigInteger, String, Float, Boolean, DateTime

from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider

from os import environ as env
from functools import reduce
from datetime import datetime
from time import sleep
import pandas
from math import log, exp
from sklearn.linear_model import LogisticRegression
from urllib.parse import quote

import requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_CASSANDRA_SCHEMA = Secret('SNOWFLAKE_CASSANDRA_SCHEMA').get()
SNOWFLAKE_DBT_SCHEMA = Secret('SNOWFLAKE_DBT_SCHEMA').get()

VITESS_HOST = Secret('VITESS_HOST').get()
VITESS_PORT = Secret('VITESS_PORT').get()
VITESS_SCHEMA = Secret('VITESS_SCHEMA').get()
VITESS_USER = Secret('VITESS_USER').get()
VITESS_PASSWORD = Secret('VITESS_PASSWORD').get()

CASSANDRA_USER = Secret('CASSANDRA_USER').get()
CASSANDRA_PASSWORD = Secret('CASSANDRA_PASSWORD').get()
CASSANDRA_HOST = Secret('CASSANDRA_HOST').get()
ES_USER = Secret('ES_USER').get()
ES_PASSWORD = Secret('ES_PASSWORD').get()
ES_BASE = 'https://a7767ca99088a42d58a098b8795bb42f-febc0d643005d91f.elb.us-east-1.amazonaws.com:9200'
ES_RECOMMENDATIONS_FEED = 'minds-clustered-entities-feed'

metadata = MetaData(schema=SNOWFLAKE_CASSANDRA_SCHEMA)
users_cluster_map = Table('users_cluster_map', metadata,
    Column('user_id', String(25)),
    Column('cluster', Integer)
)

tag_list = ( 'art', 'blockchain', 'blog', 'comedy', 'crypto', 'education', 'fashion',
    'film', 'food', 'freespeech', 'gaming', 'health', 'history', 'journalism',
    'memes', 'minds', 'mindsth', 'music', 'myphoto', 'nature', 'news', 'nutrition',
    'outdoors', 'photography', 'poetry', 'politics', 'science', 'spirituality', 'sports',
    'technology', 'travel', 'videos')

# Each record in `medoids` is a boolean vector of tags selected from the `tag_list` above
# Each record represents the medoid of one of 20 clusters of user tag selections

medoids = [
    [1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0],
    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
    [1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0],
    [1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0],
    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
    [1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0],
    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    [1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0],
    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
    [1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
    [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    [0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
    [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0],
    [1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0],
    [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0],
    [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
]
# `means` is the mean of the abs of the per-tag coefficient values from logistic regression of engagement
# across all documents that are regressed. It is a measure of how predictive a given tag selection is of
# engagement, and is used to compute the manhattan distance for clustering users and assigning users to
# a cluster
means = [
    0.3545987246994849, 0.241748598233092, 0.11993687044340288, 0.31503514512797737,
    0.2946099864613166, 0.030747736991024625, 0.06128259150456022, 0.22975133866678363,
    0.35005582559667153, 0.0899882774161916, 0.11275091790082134, 0.030101218816828722,
    0.041582341548202555, 0.31211355732437773, 0.37815002948556764, 0.3470340051741046,
    0.007656946531000879, 0.34159866682006373, 0.2581057344823159, 0.31274554357187684,
    0.34420010572402143, 0.3005038090966229, 0.3886378444247918, 0.3069674081204343,
    0.1653008549393813, 0.08200647465132051, 0.10135619994625591, 0.2895407129537117,
    0.2486517928058211, 0.3563846586489099, 0.2971924835643093, 0.271767184675284
 ]
max_distance = reduce(lambda a, b: a + b, means)

def get_db():
    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_CASSANDRA_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)

def init_db():
    global metadata
    metadata.create_all(get_db())

def get_cassandra():
    auth_provider = PlainTextAuthProvider(CASSANDRA_USER, CASSANDRA_PASSWORD)
    cluster = Cluster([CASSANDRA_HOST], auth_provider=auth_provider, prepare_on_all_hosts = True)
    cass = cluster.connect()
    cass.default_fetch_size = 10000
    return cass

def get_vitess():
    url = "mysql+mysqldb://{}:{}@{}:{}/{}".format(
        VITESS_USER,
        quote(VITESS_PASSWORD),
        VITESS_HOST,
        VITESS_PORT,
        VITESS_SCHEMA
    )
    return create_engine(url)

def get_vitess_metadata(schema=VITESS_SCHEMA):
    metadata = MetaData(schema=schema)

    Table('recommendations_clustered_recs', metadata,
        Column('entity_guid', BigInteger),
        Column('entity_owner_guid', BigInteger),
        Column('cluster_id', Integer),
        Column('score', Float),
        Column('total_views', Integer),
        Column('total_engagement', Integer),
        Column('first_engaged', DateTime),
        Column('last_engaged', DateTime),
        Column('last_updated', DateTime),
        Column('time_created', DateTime),
        Index('idx_cluster_id', 'cluster_id')
    )

    return metadata

def init_vitess(logger):
    logger.info("Getting Vitess table metadata")
    metadata = get_vitess_metadata()
    logger.info("Initializing Vitess table")
    metadata.create_all(get_vitess())

def get_content_regressions(logger):
    conn = get_db().connect()
    logger.info('loading engagement vectors')
    training = pandas.read_sql(f"""
with filtered as (
    select
        entity_guid
    from
        {SNOWFLAKE_DBT_SCHEMA}.int_tagged_engagement_post_stats
    where
        organic_engagement_ratio > 0.10
        and organic_engagement_ratio > (total_views * (-0.2 / 200)) + 0.30
        and total_engagements >= 7
        and is_public = true
    order by
        first_engagement desc
    limit
        1500
),
vectors as (
    select
        int_tagged_engagement_vectors.*
    from
        {SNOWFLAKE_DBT_SCHEMA}.int_tagged_engagement_vectorS
        inner join filtered on
            int_tagged_engagement_vectors.entity_guid = filtered.entity_guid
)
select * from vectors
            """, conn)

    entities = {}
    for record in training.itertuples():
        if record[1] not in entities:
            entities[record[1]] = True

    logger.info(f"Regressing engagement vectors for {len(entities)} entities")
    for e in entities.keys():
        mask = training['entity_guid'] == e
        X = training[mask].iloc[:,4:36]
        y = training[mask].iloc[:,3]
        entities[e] = LogisticRegression().fit(X, y)

    conn.close()
    return entities

def get_engagement_stats(logger):
    conn = get_db().connect()
    logger.info('loading engagement stats')

    stats = {}
    for row in conn.execute(f"select * from {SNOWFLAKE_DBT_SCHEMA}.int_tagged_engagement_stats"):
        record = (
            row.first_engagement,
            row.last_engagement,
            row.total_views,
            row.total_engagements
        )
        stats[row.entity_guid] = record

    conn.close()
    return stats

def get_activity_properties(regressions, logger):
    conn = get_db().connect()
    logger.info('loading activity properties')

    properties = {}
    offset = 0
    id_list = list(regressions.keys())
    while offset < len(regressions):
        in_str = '(' + ', '.join(id_list[offset:offset+1000]) + ')'
        for row in conn.execute(f"select activity_guid as entity_guid, user_guid as entity_owner_guid, time_created from {SNOWFLAKE_DBT_SCHEMA}.stg_activities where activity_guid in {in_str}"):
            record = (
                row.entity_owner_guid,
                row.time_created
            )
            properties[str(row.entity_guid)] = record
        offset += 1000

    conn.close()
    logger.info(f"loaded {len(properties)} activity properties")
    return properties

def get_user_vectors(logger):
    with get_db().connect() as conn:
        logger.info('loading user tag vectors')
        users = pandas.read_sql(f"select * from {SNOWFLAKE_DBT_SCHEMA}.int_users_tag_vectors union all select * from {SNOWFLAKE_CASSANDRA_SCHEMA}.pseudo_users_tag_vectors", conn)
    return users

def get_content_scores(regressions, logger):
    tagmap = []
    for i in range(0, len(medoids)):
        tags = []
        for j in range(0, len(medoids[i])):
            if medoids[i][j] > 0.0:
                tags.append(tag_list[j])
        tagmap.append(tags)

    scores = []

    logger.info('scoring content')
    for entity in regressions.keys():
        coef = regressions[entity].coef_[0].tolist()
        intercept = regressions[entity].intercept_[0]
        for i in range(0, len(medoids)):
            logit = intercept
            for j in range(0,32):
                logit += medoids[i][j] * coef[j]

            odds = exp(logit)
            prob = odds / (1+odds)

            scores.append((entity, i, prob, tagmap[i]))

    return scores

def match_cluster(user_vector):
    distance = max_distance
    cluster = -1
    for i in range(0, len(medoids)):
        d = 0
        for j in range(0, len(user_vector)):
            if user_vector[j] != medoids[i][j]:
                d += means[j]
        if d < distance:
            distance = d
            cluster = i
    return cluster

def map_users_clusters(users, logger):
    cluster_map = []
    user_vectors = users.iloc[:,1:33].values.tolist()
    user_ids = users.iloc[:,0:1].values.tolist()

    logger.info('mapping users to clusters')
    for i in range(0,len(user_ids)):
        cluster_map.append( [user_ids[i], match_cluster(user_vectors[i])] )

    return cluster_map

def extract_content_scores(last_updated, logger):
    logger.info("Getting content regressions")
    regressions = get_content_regressions(logger)
    logger.info("Getting content scores")
    scores = get_content_scores(regressions, logger)
    logger.info("Getting engagement stats")
    stats = get_engagement_stats(logger)
    logger.info("Getting engagement properties")
    properties = get_activity_properties(regressions, logger)
    logger.info(f"Processing {len(scores)} score records")
    update_str = last_updated.strftime("%Y-%m-%dT%H:%M:%SZ")
    for record in scores:
        rstats = stats[record[0]]
        rprops = properties[record[0]]
        doc_id = f"{record[0]}-{record[1]}"
        json = {
            'entity_guid': record[0],
            'entity_owner_guid': rprops[0],
            'cluster_id': record[1],
            'score': record[2],
            'total_views': rstats[2],
            'total_engagement': rstats[3],
            '@first_engaged': rstats[0].strftime("%Y-%m-%dT%H:%M:%SZ"),
            '@last_engaged': rstats[1].strftime("%Y-%m-%dT%H:%M:%SZ"),
            '@last_updated': update_str,
            '@time_created': rprops[1].strftime("%Y-%m-%dT%H:%M:%SZ")
        }
        record = {
            'entity_guid': record[0],
            'entity_owner_guid': rprops[0],
            'cluster_id': record[1],
            'score': record[2],
            'total_views': rstats[2],
            'total_engagement': rstats[3],
            'first_engaged': rstats[0],
            'last_engaged': rstats[1],
            'last_updated': last_updated,
            'time_created': rprops[1]
        }
        yield (doc_id, json, record)

def es_bulk_ops(ops, logger):
    logger.info(f"Bulk inserting {len(ops)/2} records")
    ops.append('\n')
    body = '\n'.join(ops)
    resp = requests.post(
        f"{ES_BASE}/{ES_RECOMMENDATIONS_FEED}/_bulk",
        headers = { 'Content-Type': 'application/json' },
        auth = (ES_USER,ES_PASSWORD),
        data = body,
        verify = False
    )
    logger.info(f"Bulk insert response code: {resp.status_code} {resp.reason}")
    if resp.status_code == 400:
        logger.warn(f"Response:\n{resp.text}")

@task
def insert_content_scores():
    logger = prefect.context.get("logger")
    logger.info("Initializing Vitess")
    init_vitess(logger)
    logger.info("Vitess initialized")
    conn = get_vitess().connect()
    last_updated = datetime.now()
    ins = get_vitess_metadata().tables[f"{VITESS_SCHEMA}.recommendations_clustered_recs"].insert()
    ops = []
    records = []
    logger.info("Starting ETL")
    for doc_id, json, record in extract_content_scores(last_updated, logger):
        ops.append(f"""{{ "update" : {{ "_id": "{doc_id}", "_index" : "minds-clustered-entities-feed" }} }}""")
        ops.append(f"""{{ "doc" : {str(json).replace("'",'"')}, "doc_as_upsert" : true }}""")
        records.append(record)
        if len(ops) >= 1000:
            es_bulk_ops(ops, logger)
            conn.execute(ins.values(records))
            ops = []
            records = []

    es_bulk_ops(ops, logger)
    conn.execute(ins.values(records))
    conn.close()

    return last_updated

@task
def clear_old_content_scores(last_updated):
    logger = prefect.context.get("logger")
    update_str = last_updated.strftime("%Y-%m-%dT%H:%M:%SZ")
    query = { "query": { 'range': { "@last_updated": { "lt": update_str } } } }

    logger.info(f"Sleeping 5 minutes for Elasticsearch to process inserts")
    sleep(300)
    logger.info(f"""Delete old recommendations query: {str(query).replace("'",'"')}""")
    resp = requests.post(
        f"{ES_BASE}/{ES_RECOMMENDATIONS_FEED}/_delete_by_query",
        auth= (ES_USER, ES_PASSWORD),
        headers = { 'Content-Type': 'application/json' },
        json=query,
        verify=False)
    logger.info(f"Delete old recommendations response: {resp.status_code} {resp.reason}")
    logger.info(resp.json())

    logger.info(f"Deleting old recommendations from vitess")
    with get_vitess().connect() as conn:
        conn.execute(f"delete from {VITESS_SCHEMA}.recommendations_clustered_recs where last_updated < '{last_updated.strftime('%Y-%m-%d %H:%M:%S')}'")


def do_user_mapping(logger):
    users = get_user_vectors(logger)
    cluster_map = map_users_clusters(users, logger)

    return cluster_map

@task
def update_user_cluster_map():
    logger = prefect.context.get("logger")
    logger.info("Updating user cluster map")
    logger.info("Initializing database")
    conn = get_db().connect()
    init_db()

    logger.info("Mapping users")
    cluster_map = do_user_mapping(logger)

    logger.info("Clearing old map")
    delete = metadata.tables[f"{SNOWFLAKE_CASSANDRA_SCHEMA}.users_cluster_map"].delete()
    conn.execute(delete)

    logger.info(f"Inserting new map of {len(cluster_map)} users")
    ins = metadata.tables[f"{SNOWFLAKE_CASSANDRA_SCHEMA}.users_cluster_map"].insert()
    offset = 0
    while offset < len(cluster_map):
        conn.execute(ins.values(cluster_map[offset:offset+1000]))
        offset += 1000
    logger.info("Insertion of user cluster map complete")


with Flow("update-recommendations") as flow:
    scores = clear_old_content_scores(insert_content_scores())
    update_user_cluster_map(upstream_tasks=[scores])

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
