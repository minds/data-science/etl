import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

from sqlalchemy import create_engine
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from cassandra.concurrent import execute_concurrent_with_args
from os import environ as env
from urllib.parse import quote

from datetime import datetime

PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_CASSANDRA_SCHEMA = Secret('SNOWFLAKE_CASSANDRA_SCHEMA').get()
SNOWFLAKE_DBT_SCHEMA = Secret('SNOWFLAKE_DBT_SCHEMA').get()

CASSANDRA_HOST = Secret('CASSANDRA_HOST').get()
CASSANDRA_USER = Secret('CASSANDRA_USER').get()
CASSANDRA_PASSWORD = Secret('CASSANDRA_PASSWORD').get()

CHUNK_SIZE = 20000

def divide_chunks(value_list, chunk_size=CHUNK_SIZE):
    """
    Divides iterables into smaller batches
    """

    for i in range(0, len(value_list), chunk_size):
        yield value_list[i:i + chunk_size]

def get_snowflake():
    """
    Connect to snowflake
    """

    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_CASSANDRA_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)

def get_cassandra():
    """
    Connect to cassandra
    """

    auth_provider = PlainTextAuthProvider(username=CASSANDRA_USER, password=CASSANDRA_PASSWORD)
    cluster = Cluster([CASSANDRA_HOST], auth_provider=auth_provider)
    return cluster

def fetch_predictions(logger):
    """
    Fetches the spam predictions from the Snowflake
    """

    query = f"""
    select
        user_id,
        account_type,
        getdate() as timestamp,
        probability 
    from
        {SNOWFLAKE_DATABASE}.{SNOWFLAKE_DBT_SCHEMA}.dmn_spam_predictions;
    """
    
    try:
        engine = get_snowflake()
        conn   = engine.connect()

        logger.info(f"Connection to Snowflake successfully opened")
        logger.info(f"Fetching entries from {SNOWFLAKE_DATABASE}.{SNOWFLAKE_DBT_SCHEMA}.dmn_spam_predictions")

        result_set   = conn.execute(query).fetchall()

    finally:
        conn.close()
        engine.dispose()

        logger.info(f"Connection to Snowflake successfully closed")
    
    logger.info(f"Fetched {len(result_set)} entries from {SNOWFLAKE_DATABASE}.{SNOWFLAKE_DBT_SCHEMA}.dmn_spam_predictions")

    return result_set

@task
def extract_user_scores():
    """
    Loads the spam predictions into Cassandra 
    """
    
    logger = prefect.context.get("logger")

    query = """
    insert into
        user_quality_scores (
            user_id, 
            category, 
            timestamp, 
            score
        )
    values (
        %s,
        %s,
        %s,
        %s
    )
    """

    try:
        cass    = get_cassandra()
        session = cass.connect('minds')
        
        logger.info(f"Connection to Cassandra successfully opened")
        
        predictions = fetch_predictions(logger)
        
        logger.info(f"Inserting {len(predictions)} entries into minds.user_quality_scores")
        for chunk in divide_chunks(predictions):
            execute_concurrent_with_args(
                session,
                query,
                chunk,
                concurrency=CHUNK_SIZE,
                raise_on_first_error=True
            )
        logger.info(f"Inserted {len(predictions)} entries into minds.user_quality_scores")

    finally:
        cass.shutdown()
        logger.info(f"Connection to Cassandra successfully closed")
    

with Flow("extract-user-score-quality") as flow:
    extract_user_scores()

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
