import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

from os import environ as env
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy import Float, MetaData, Table, Column, BigInteger, Integer

import pandas
from urllib.parse import quote

from scipy.cluster.hierarchy import linkage
from scipy.sparse import coo_array

PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_CASSANDRA_SCHEMA = Secret('SNOWFLAKE_CASSANDRA_SCHEMA').get()
SNOWFLAKE_DBT_SCHEMA = Secret('SNOWFLAKE_DBT_SCHEMA').get()

VITESS_HOST = Secret('VITESS_HOST').get()
VITESS_PORT = Secret('VITESS_PORT').get()
VITESS_SCHEMA = Secret('VITESS_SCHEMA').get()
VITESS_USER = Secret('VITESS_USER').get()
VITESS_PASSWORD = Secret('VITESS_PASSWORD').get()

batchsize = 10000

def get_sf_engine():

    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_CASSANDRA_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)

def get_vitess_engine():
    url = "mysql+mysqldb://{}:{}@{}:{}/{}?charset=utf8mb4&binary_prefix=true".format(
        VITESS_USER,
        quote(VITESS_PASSWORD),
        VITESS_HOST,
        VITESS_PORT,
        VITESS_SCHEMA
    )
    return create_engine(url)

def get_recs_metadata(schema='vitess_extract'):
    metadata = MetaData(schema=schema)
    
    Table('recommendations_cluster_members', metadata,
        Column('cluster_id', Integer),
        Column('user_id', BigInteger)
    )
    
    return metadata

def get_vitess_metadata(schema='minds'):
    metadata = MetaData(schema=schema)
    
    Table('recommendations_user_cluster_map', metadata,
          Column('user_id', BigInteger),
          Column('cluster_id', Integer)
    )
    
    Table('recommendations_cluster_activity_map', metadata,
        Column('cluster_id', Integer),
        Column('activity_guid', BigInteger),
        Column('channel_guid', BigInteger),
        Column('score', Float),
    )
    
    return metadata

engagement_query = """
with
user_channel_engagement as (
    select * from dbt_prod.int_user_channel_engagement
),
user_filter as (
    select
        user_id,
        sum(engaged_count) as engaged_count,
        max(last_engaged) as last_engaged
    from
        user_channel_engagement
    group by
        user_id
),
user_channels as (
    select
        user_channel_engagement.user_id,
        user_channel_engagement.channel_guid,
        user_channel_engagement.engaged_count,
        user_channel_engagement.first_engaged,
        user_channel_engagement.last_engaged,
        user_channel_engagement.engagement_score
    from
        user_channel_engagement
        inner join user_filter on
            user_channel_engagement.user_id = user_filter.user_id
        where
            user_filter.last_engaged > dateadd(day, -60, current_date())
            and user_filter.engaged_count > 5
            and user_channel_engagement.last_engaged > dateadd(day, -90, current_date())
            and user_channel_engagement.user_id <> user_channel_engagement.channel_guid
)
select * from user_channels
"""

scored_query = """
with records as (
    select
        recommendations_cluster_members.cluster_id,
        int_user_engagement_records.*,
        case
            when downvote_count > 0 then -1
            when upvote_count > 0 or remind_count > 0 or comment_count > 0 then 1
            else 0
        end as net_engagement
    from
        dbt_prod.int_user_engagement_records
        inner join vitess_extract.recommendations_cluster_members on
            int_user_engagement_records.user_id = recommendations_cluster_members.user_id
    where
        int_user_engagement_records.user_id <> int_user_engagement_records.channel_guid
        and int_user_engagement_records.access_id = 2
),
user_stats as (
    select
        cluster_id,
        entity_guid,
        channel_guid,
        sum(net_engagement) as engaged_count
    from
        records
    group by
        cluster_id,
        entity_guid,
        channel_guid
),
pseudo_stats as (
    select
        entity_guid,
        count(*) as viewer_count,
        sum(case when engaged_count > 0 then 1 else 0 end) as engaged_count,
        min(first_viewed) as first_viewed,
        max(last_viewed) as last_viewed
    from
        dbt_prod.int_pseudo_engagement_records
    group by
        entity_guid
),
stats as (
    select
        user_stats.cluster_id,
        user_stats.entity_guid,
        user_stats.channel_guid,
        user_stats.engaged_count as user_engaged_count,
        pseudo_stats.engaged_count as pseudo_engaged_count,
        pseudo_stats.viewer_count,
        pseudo_stats.first_viewed,
        pseudo_stats.last_viewed
    from
        user_stats
        inner join pseudo_stats on
            user_stats.entity_guid = pseudo_stats.entity_guid
),
scored as (
    select
        *,
        user_engaged_count::float/viewer_count::float as user_engagement_ratio,
        pseudo_engaged_count::float/viewer_count::float as pseudo_engagement_ratio
    from
        stats
    where
        viewer_count > 10
        and last_viewed > dateadd(day, -60, current_date())
        and user_engagement_ratio >= 0.05
),
deleted as (
    select distinct
        entity_guid as deleted_entity_guid
    from
        dbt_prod.stg_snowplow_events_userid
    where
        action = 'delete'
        and entity_type = 'activity' or entity_type is null
        and entity_guid is not null
        and collector_tstamp > '2022-09-01'
),
scored_undeleted as (
    select
        scored.*,
        deleted.deleted_entity_guid
    from
        scored
        left join deleted on
            scored.entity_guid::varchar = deleted.deleted_entity_guid
    where
        deleted_entity_guid is null
)
select cluster_id, entity_guid, channel_guid, user_engagement_ratio from scored_undeleted
order by cluster_id, user_engagement_ratio desc
"""

@task
def update_activity_recommendations():
    logger = prefect.context.get("logger")
    
    logger.info("Initializing database")
    metadata = get_recs_metadata()
    with get_sf_engine().connect() as conn:
        metadata.create_all(conn)
        
    metadata = get_vitess_metadata()
    with get_vitess_engine().connect() as conn:
        metadata.create_all(conn)

    logger.info("Loading user channel engagement data")
    with get_sf_engine().connect() as conn:
        user_channel_engagement = pandas.read_sql(engagement_query, conn)
    
    logger.info("Building sparse engagement array")
    user_map = {}
    channel_map = {}
    user_list = []
    channel_list = []

    rows = []
    cols = []
    data = []

    user_index = 0
    channel_index = 0
    for record in user_channel_engagement.itertuples(index=False):
        user = record[0]
        channel = record[1]
        score = record[5]
        
        if not user in user_map:
            user_map[user] = user_index
            user_list.append(user)
            user_index += 1
            
        if not channel in channel_map:
            channel_map[channel] = channel_index
            channel_list.append(channel)
            channel_index += 1
            
        rows.append(user_map[user])
        cols.append(channel_map[channel])
        data.append(score)

    engagement_array = coo_array((data, (rows, cols))).toarray()

    logger.info("Building channel engagement clusters")
    new_clusters = linkage(engagement_array, method='complete', metric='cityblock')

    logger.info("Mapping users to channel engagement clusters")
    clusters = {}
    for uid, index in user_map.items():
        clusters[index] = (index, index, 0.0, 1, (int(uid),))
    cluster_number = len(user_list)
    for cluster in new_clusters:
        left = int(cluster[0])
        right = int(cluster[1])
        distance = cluster[2]
        size = int(cluster[3])
        clusters[cluster_number] = (left, right, distance, size, (clusters[left][4] + clusters[right][4]))
        cluster_number += 1

    rec_clusters={}
    user_cluster={}
    max_cluster = None
    max_cluster_len = 0
    for cluster_number in range(0, len(clusters)):
        c = clusters[cluster_number]
        left = c[0]
        right = c[1]

        if c[2] > 0.75 and c[2] < 1.95 and c[3] >= 20:
            if left in rec_clusters and clusters[left][3] < 50:
                del(rec_clusters[left])
            if right in rec_clusters and clusters[right][3] < 50:
                del(rec_clusters[right])
            if clusters[left][3] == 1 and right in rec_clusters:
                del(rec_clusters[right])
            if clusters[right][3] == 1 and left in rec_clusters:
                del(rec_clusters[left])
            if left in rec_clusters and right in rec_clusters:
                continue
            rec_clusters[cluster_number] = c
            if len(c[4]) > max_cluster_len:
                max_cluster = cluster_number
                max_cluster_len = len(c[4])
                logger.info(f"Max cluster {max_cluster} size {max_cluster_len}")
            for user in c[4]:
                if not user in user_cluster or not user_cluster[user] in rec_clusters:
                    user_cluster[user] = cluster_number

    logger.info("Updating cluster assignments in Snowflake")
    ins = get_recs_metadata().tables['vitess_extract.recommendations_cluster_members'].insert()
    values = []
    for cluster_id, cluster in rec_clusters.items():
        for user_id in cluster[4]:
            values.append({'cluster_id': cluster_id, 'user_id': user_id})
    with get_sf_engine().connect() as conn:
        conn.execute('delete from vitess_extract.recommendations_cluster_members where true')
        offset = 0
        while offset < len(values):
            conn.execute(ins.values(values[offset:offset+batchsize]))
            offset += batchsize

    logger.info("Extracting activity engagement scores by cluster")
    scores = []
    counts = {}
    with get_sf_engine().connect() as conn:
        res = conn.execute(scored_query)
        for row in res:
            cluster = row[0]
            if not cluster in counts:
                counts[cluster] = 0
            counts[cluster] += 1
            if counts[cluster] <= 1000:
                scores.append({
                    'cluster_id': row[0],
                    'activity_guid': row[1],
                    'channel_guid': row[2],
                    'score': row[3]
                })

    logger.info(f"Updating {len(user_cluster)+1} user cluster assignments and {len(scores)} cluster recommendations in Vitess")
    users = []
    for user_id, cluster_id in user_cluster.items():
        users.append({
            'user_id': user_id,
            'cluster_id': cluster_id
        })
    users.append({'user_id': 0, 'cluster_id': max_cluster})

    with get_vitess_engine().begin() as conn:
        logger.info(f"Start transaction at: {datetime.now()}")
        ins = get_vitess_metadata().tables['minds.recommendations_cluster_activity_map'].insert()
        offset = 0
        conn.execute('delete from minds.recommendations_cluster_activity_map where true')
        while offset < len(scores):
            conn.execute(ins.values(scores[offset:offset+batchsize]))
            logger.info(f"Inserted recommendations batch {offset}-{offset+batchsize} at {datetime.now()}")
            offset += batchsize

        ins = get_vitess_metadata().tables['minds.recommendations_user_cluster_map'].insert()
        offset = 0
        conn.execute('delete from minds.recommendations_user_cluster_map where true')
        while offset < len(users):
            conn.execute(ins.values(users[offset:offset+batchsize]))
            logger.info(f"Inserted users batch {offset}-{offset+batchsize} at {datetime.now()}")
            offset += batchsize

with Flow("update-activity-recommendations") as flow:
    update_activity_recommendations()

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
