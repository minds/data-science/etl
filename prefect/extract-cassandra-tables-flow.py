import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

from sqlalchemy import create_engine
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from cassandra.concurrent import execute_concurrent_with_args
from cassandra.policies import DCAwareRoundRobinPolicy
from threading import Thread, current_thread
from multiprocessing import Process, Queue, current_process
from time import sleep
from os import environ as env
from os import remove
from datetime import datetime
from urllib.parse import quote

from cassandra_tables_metadata import get_metadata

PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_CASSANDRA_SCHEMA = Secret('SNOWFLAKE_CASSANDRA_SCHEMA').get()

CASSANDRA_USER = Secret('CASSANDRA_USER').get()
CASSANDRA_PASSWORD = Secret('CASSANDRA_PASSWORD').get()
CASSANDRA_HOST = Secret('CASSANDRA_HOST').get()

DATA_DIR = '/root/data'

metadata = get_metadata(schema=SNOWFLAKE_CASSANDRA_SCHEMA)

def get_db():
    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_CASSANDRA_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)

def init_db():
    global metadata
    metadata.create_all(get_db())

def get_cassandra():
    auth_provider = PlainTextAuthProvider(username=CASSANDRA_USER, password=CASSANDRA_PASSWORD)
    cluster = Cluster([CASSANDRA_HOST], auth_provider=auth_provider, prepare_on_all_hosts = True, 
        control_connection_timeout=120, connect_timeout=120,
        protocol_version=4, load_balancing_policy=DCAwareRoundRobinPolicy())
    return cluster

def build_ranges():
    cluster = get_cassandra()
    cluster.connect()
    tm = cluster.metadata.token_map
    tm.rebuild_keyspace('minds', build_if_absent=True)
    ranges = []
    last = None
    for token in tm.ring:
        if last != None:
            ranges.append((last, token.value))
        last = token.value
    if last:
        ranges.append((last, 170141183460469231731687303715884105728))
    return ranges


def cassandra_values(token_range, table_name, key_name):
    with get_cassandra().connect() as conn:
        stmt = conn.prepare(f"select * from minds.{table_name} where token({key_name}) >= ? and token({key_name}) < ?")
        for (success, result) in execute_concurrent_with_args(conn, stmt, token_range, concurrency=3, results_generator=True):
            if success:
                for row in result:
                    yield row
            else:
                raise result

bool_map = {
    '1': True,
    'true': True,
    'yes': True,
    '1645600380': True, # Joe Rogan is pro
    '0': False,
    'false': False,
    'no': False
}

def to_boolean(value):
    try:
        return bool_map[str(value).lower()]
    except:
        return None

def to_datetime(value):
    try:
        int_value = int(value)
        return str(datetime.fromtimestamp(int_value))
    except:
        return str(value)

def to_integer(value):
    try:
        i = int(value)
        if i > 9223372036854775807 or i < -9223372036854775808:
            return None
        else:
            return i
    except:
        return None

def to_number(value, precision):
    try:
        i = int(value)
        p = len(str(i).replace('-',''))
        if p > precision:
            return None
        else:
            return i
    except:
        return None

def to_string(value, size):
    escaped = str(value).replace('\r', '').replace('\\', '\\\\').replace(',','\\,')[0:size]
    while escaped[-1:] == '\\':
        escaped = escaped[0:-1]
    return escaped

def to_map(value):
    try: 
        map = {}
        for k, v in value.items():
            if isinstance(v, int) or isinstance(v, float):
                map[str(k)] = v
            elif v == None:
                map[str(k)] = False
            else:
                map[str(k)] = str(v)
        return str(map).replace(',','\\,')
    except:
        return None

def to_array(value):
    try:
        array = []
        for e in value:
            array.append(e)
        return str(array).replace(',','\\,')
    except:
        return None

def map_column(column):
    type = str(column.type)
    if type == 'BIGINT' or type == 'INTEGER':
        return lambda row, name: to_integer(getattr(row, name)) if getattr(row, name) != None else None
    elif type[0:7] == 'NUMERIC':
        precision = int(type[8:-1])
        return lambda row, name, pr=precision: to_string(getattr(row, name), pr) if getattr(row, name) != None else None
    elif type[0:7] == 'VARCHAR':
        size = int(type[8:-1])
        return lambda row, name, sz=size: to_string(getattr(row, name), sz) if getattr(row, name) != None else None
    elif type == 'DATETIME':
        return lambda row, name: to_datetime(getattr(row, name)) if getattr(row, name) != None else None
    elif type == 'BOOLEAN':
        return lambda row, name: to_boolean(getattr(row, name)) if getattr(row, name) != None else None
    elif type == 'OBJECT':
        return lambda row, name: to_map(getattr(row, name)) if getattr(row, name) != None else None
    elif type == 'ARRAY':
        return lambda row, name: to_array(getattr(row, name)) if getattr(row, name) != None else None
    else:
        return lambda row, name: str(getattr(row, name)) if getattr(row, name) != None else None

def write_batch(file, batch):
    file.write(','.join(batch['header'])+'\r\n')
    for record in batch['records']:
        file.write(','.join(map(lambda column: str(record[column]) if record[column] else '', batch['header'])) + '\r\n')

def executor(queue, logger):
    name = f"{current_process().name}_{current_thread().name}"
    logger.info(f"{name} listening on queue\n")
    while True:
        batch = queue.get()
        if batch == None:
            queue.put(None)
            break

        try:
            logger.info(f"Writing CSV file {DATA_DIR}/{batch['filename']}")
            with open(f"{DATA_DIR}/{batch['filename']}", 'w', encoding='UTF8', newline='') as f:
                write_batch(f, batch)

            with get_db().connect() as conn:
                logger.info(f"Staging CSV file file:///{DATA_DIR}/{batch['filename']}")
                conn.execute(f"put file:///{DATA_DIR}/{batch['filename']} @{SNOWFLAKE_CASSANDRA_SCHEMA}.staging")

                logger.info(f"Copying staged file {batch['filename']} into table {batch['table']}")
                conn.execute(f"""copy into {SNOWFLAKE_CASSANDRA_SCHEMA}.{batch['table']} from @{SNOWFLAKE_CASSANDRA_SCHEMA}.staging/{batch['filename']} file_format = (type=csv skip_header=1 record_delimiter='\\r\\n' escape='\\\\' empty_field_as_null=TRUE encoding=UTF8 )""")

            logger.info(f"Removing CSV file /{DATA_DIR}/{batch['filename']}")
            remove(f"/{DATA_DIR}/{batch['filename']}")
        except BaseException as e:
            logger.warn(f"{batch['filename']} {type(e)} {e}: {e.args}")
        finally:
            del batch

def launcher(queue, logger):
    Thread(target=executor, name="Thread1", args=(queue, logger)).start()
    Thread(target=executor, name="Thread2", args=(queue, logger)).start()
    Thread(target=executor, name="Thread3", args=(queue, logger)).start()

def setup(queue, logger):
    logger.info("Initialising DB")
    init_db()

    logger.info("Starting Snowflake insert threads")
    Process(target=launcher, name="Process1", args=(queue, logger)).start()
    logger.info("Threads started")

    return queue

def make_batch(table_name, header, records, extracted_at, batch_number):
    return {
        'filename': f"{table_name}_{extracted_at.strftime('%Y%m%d-%H%M%S')}_{batch_number:>04d}.csv",
        'table': table_name,
        'header': header,
        'records': records
    }

@task
def extract_all_tables():
    global ringsize, ring, next_entry, keys, schema, table_buffers, fields, metadata
    queue = Queue()
    logger = prefect.context.get("logger")
    logger.info("Setting up")
    setup(queue, logger)

    logger.info("Building cassandra cluster token ranges")
    token_ranges = build_ranges()
    
    all_table_names = [table.name for table in metadata.sorted_tables]

    for table_name in all_table_names:

        logger.info(f"{table_name} | Initializing extract")
        extracted_at = datetime.now()
        header = []
        column_map = {}
        records = []
        batch_number = 1
        table = metadata.tables[f"{SNOWFLAKE_CASSANDRA_SCHEMA}.{table_name}"]
        for column in table.columns:
            header.append(column.name)
            if not column.name == 'extracted_at':
                column_map[column.name] = map_column(column)
        primary_key = header[0]

        logger.info(f"{table_name} | Starting extract")
        total_records = 0
        offset = 0
        while offset < len(token_ranges):
            try:
                for row in cassandra_values(token_ranges[offset:offset+100], table_name, primary_key):
                    record = {'extracted_at': extracted_at}
                    for column in header:
                        if not column == 'extracted_at':
                            record[column] = column_map[column](row, column)

                    records.append(record)

                    if len(records) >= 200000:
                        queue.put( make_batch(table_name, header, records, extracted_at, batch_number) )
                        total_records += len(records)
                        logger.info(f"{table_name} | Added batch of {len(records)}, {total_records} total, queue size: {queue.qsize()}, token range offset: {offset}")
                        batch_number += 1
                        records = []
                offset += 100

            except BaseException as e:
                logger.warn(f"{table_name}:{offset} Unable to process because:")
                logger.warn(f"{type(e)} {e}: {e.args}")

        logger.info(f"{table_name} | Extract complete, flushing {len(records)} records")
        if len(records) > 0:
            queue.put( make_batch(table_name, header, records, extracted_at, batch_number) )

    while not queue.empty():
        logger.info(f"{queue.qsize()} batches in queue")
        sleep(10)

    logger.info("All batches processed, closing queue")
    queue.put(None)


with Flow("extract-cassandra-tables") as flow:
    extract_all_tables()

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
