from datetime import datetime
import prefect
from prefect import task, Flow
from prefect.client.secrets import Secret

import re
import os
from threading import Thread, current_thread
from multiprocessing import Process, Queue, current_process
from time import sleep
from os import environ as env
from os import remove
from urllib.parse import quote

from sqlalchemy import create_engine
from sqlalchemy import MetaData, Table, Column, Float, Numeric, BigInteger, Integer, String, Boolean, DateTime
from sqlalchemy.types import *

import pulsar
import pulsar.schema as pschema

import pandas as pd
from nltk.stem.porter import PorterStemmer
from sklearn.feature_extraction.text import CountVectorizer

import pickle


PROJECT_NAME = env["PREFECT_PROJECT_NAME"]

SNOWFLAKE_ACCOUNT = Secret('SNOWFLAKE_ACCOUNT').get()
SNOWFLAKE_USER = Secret('SNOWFLAKE_USER').get()
SNOWFLAKE_PASSWORD = Secret('SNOWFLAKE_PASSWORD').get()
SNOWFLAKE_ROLE = Secret('SNOWFLAKE_ROLE').get()
SNOWFLAKE_WAREHOUSE = Secret('SNOWFLAKE_WAREHOUSE').get()
SNOWFLAKE_DATABASE = Secret('SNOWFLAKE_DATABASE').get()
SNOWFLAKE_CASSANDRA_SCHEMA = Secret('SNOWFLAKE_CASSANDRA_SCHEMA').get()
SNOWFLAKE_DBT_SCHEMA = Secret('SNOWFLAKE_DBT_SCHEMA').get()

PULSAR_URL = 'pulsar+ssl://ac9142769e6df42fbaa3a6565a232275-51f8e532103492f8.elb.us-east-1.amazonaws.com:6651/'
PULSAR_ROOT = 'persistent://minds-com/engine'

DATA_DIR = "/root/data"

def get_model():
    lrc = pickle.load(open(os.path.join(DATA_DIR, 'comment_classifier.pkl'), 'rb'))
    cv = pickle.load(open(os.path.join(DATA_DIR, 'count_vectorizer.pkl'), 'rb'))
    return (lrc, cv)

def get_db():
    url = "snowflake://{}:{}@{}/{}/{}?warehouse={}&role={}".format(
        SNOWFLAKE_USER,
        quote(SNOWFLAKE_PASSWORD),
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_DATABASE,
        SNOWFLAKE_CASSANDRA_SCHEMA,
        SNOWFLAKE_WAREHOUSE,
        SNOWFLAKE_ROLE
    )
    return create_engine(url)


def get_pulsar_client():
    client = pulsar.Client(
        PULSAR_URL,
        tls_trust_certs_file_path='/home/ubuntu/minds-ca.crt',
        tls_allow_insecure_connection=True
    )
    return client

def get_pulsar_producer(client, topic, schema_class):
    producer = client.create_producer(topic=f"{PULSAR_ROOT}/{topic}", schema=pschema.JsonSchema(schema_class))

    return producer

def new_review(new_review, lrc):
    """ This function will process new comments, vectorize them
    and return a prediction as to spam (1) or not (0)"""
    from nltk.corpus import stopwords

    (lrc, cv) = get_model()

    new_review = new_review
    new_review = re.sub('[^a-zA-Z]', ' ', new_review)
    new_review = new_review.lower()
    new_review = new_review.split()
    ps = PorterStemmer()
    all_stopwords = stopwords.words('english')
    all_stopwords.remove('not')
    new_review = [ps.stem(word) for word in new_review if not word in   set(all_stopwords)]
    new_review = ' '.join(new_review)
    new_corpus = [new_review]
    new_X_test = cv.transform(new_corpus).toarray()
    new_y_pred = lrc.predict_proba(new_X_test)[:,1]
    return new_y_pred


@task
def new_comments():
    logger = prefect.context.get("logger")
    conn = get_db().connect()
    (lrc, cv) = get_model()

    logger.info("Loading unscored comments")
    comments_new = pd.read_sql(
    f"""
    select distinct
        stg_comments.comment_guid,
        stg_comments.user_guid as owner_guid,
        stg_comments.entity_guid,
        stg_comments.parent_guid_l1::varchar as parent_guid_l1,
        stg_comments.parent_guid_l2::varchar as parent_guid_l2,
        stg_comments.parent_guid_l3::varchar as parent_guid_l3,
        stg_comments.time_created,
        stg_comments.body,
        int_comments_predict_spam.spam_predict
    from
        {SNOWFLAKE_DBT_SCHEMA}.stg_comments
        left join {SNOWFLAKE_CASSANDRA_SCHEMA}.int_comments_predict_spam on
            stg_comments.comment_guid = int_comments_predict_spam.comment_guid
    where
        stg_comments.valid_to is null
        and stg_comments.time_created is not null
        and stg_comments.body > ''
        and stg_comments.user_guid is not null
    having
        spam_predict is null
    order by
        time_created
    limit
        500000
    """, conn)
    conn.close()
    logger.info(f"Returned dataframe (rows, cols): {str(comments_new.shape)}")

    logger.info("Mapping spam prediction model to comments")
    comments_new['spam_predict'] = comments_new['body'].apply(new_review, args=(lrc,))
    comments_new['spam_predict'] = comments_new['spam_predict'].astype('float')

    return comments_new

def build_metadata():
    metadata = MetaData(schema=f"{SNOWFLAKE_CASSANDRA_SCHEMA}")

    Table("int_comments_predict_spam", metadata,
        Column("comment_guid", BigInteger),
        Column("owner_guid", BigInteger),
        Column("time_created", DateTime),
        Column("body", String(length=65535)),
        Column("spam_predict", Float),
        Column("extracted_at", DateTime)
    )
    return metadata

class CommentSpamScore(pschema.Record):
    comment_guid = pschema.Long(required=True)
    owner_guid = pschema.Long(required=True)
    entity_guid = pschema.Long()
    parent_guid_l1 = pschema.Long() 
    parent_guid_l2 = pschema.Long()
    parent_guid_l3 = pschema.Long()
    time_created = pschema.Long(required=True)
    spam_score = pschema.Double(required=True)

def to_string(value, size=65535):
    escaped = str(value).replace('\r', '').replace('\\', '\\\\').replace(',','\\,')[0:size]
    while escaped[-1:] == '\\':
        escaped = escaped[0:-1]
    return escaped

def init_db():
    metadata = build_metadata()
    metadata.create_all(get_db())

def divide_chunks(value_list, chunk_size=5000):
    for i in range(0, len(value_list), chunk_size):
        yield value_list[i:i + chunk_size]

def write_batch(file, batch):
    file.write(','.join(batch['header'])+'\r\n')
    for record in batch['records']:
        file.write(','.join(map(lambda column: str(record[column]) if record[column] else '', batch['header'])) + '\r\n')

def send_batch(producer, batch):
    for record in batch['records']:
        comment_score = CommentSpamScore(
            comment_guid = record['comment_guid'],
            owner_guid = record['owner_guid'],
            entity_guid = record['entity_guid'],
            parent_guid_l1 = int(record['parent_guid_l1']) if record['parent_guid_l1'] != None else None,
            parent_guid_l2 = int(record['parent_guid_l2']) if record['parent_guid_l2'] != None else None,
            parent_guid_l3 = int(record['parent_guid_l3']) if record['parent_guid_l3'] != None else None,
            time_created = int(record['time_created'].timestamp() * 1000),
            spam_score = record['spam_predict']
        )
        producer.send(comment_score)


def executor(queue, logger):
    name = f"{current_process().name}_{current_thread().name}"
    logger.info(f"{name} listening on queue\n")

    client = get_pulsar_client()
    producer = get_pulsar_producer(client, 'score-comments-for-spam', CommentSpamScore)
    
    while True:
        batch = queue.get()
        if batch == None:
            queue.put(None)
            client.close()
            break

        try:
            logger.info(f"Sending batch to pulsar queue")
            send_batch(producer, batch)

            logger.info(f"Writing CSV file {DATA_DIR}/{batch['filename']}")
            with open(f"{DATA_DIR}/{batch['filename']}", 'w', encoding='UTF8', newline='') as f:
                write_batch(f, batch)

            with get_db().connect() as conn:
                logger.info(f"Staging CSV file file:///{DATA_DIR}/{batch['filename']}")
                conn.execute(f"put file:///{DATA_DIR}/{batch['filename']} @{SNOWFLAKE_CASSANDRA_SCHEMA}.staging")

                logger.info(f"Copying staged file {batch['filename']} into table {batch['table']}")
                conn.execute(f"""copy into {SNOWFLAKE_CASSANDRA_SCHEMA}.{batch['table']} from @{SNOWFLAKE_CASSANDRA_SCHEMA}.staging/{batch['filename']} file_format = (type=csv skip_header=1 record_delimiter='\\r\\n' escape='\\\\' empty_field_as_null=TRUE encoding=UTF8 )""")

            logger.info(f"Removing CSV file /{DATA_DIR}/{batch['filename']}")
            remove(f"/{DATA_DIR}/{batch['filename']}")
        except BaseException as e:
            logger.warn(f"{batch['filename']} {type(e)} {e}: {e.args}")
        finally:
            del batch

def launcher(queue, logger):
    Thread(target=executor, name="Thread1", args=(queue, logger)).start()
    Thread(target=executor, name="Thread2", args=(queue, logger)).start()

def setup(queue, logger):
    logger.info("Initialising DB")
    init_db()

    logger.info("Starting Snowflake insert threads")
    Process(target=launcher, name="Process1", args=(queue, logger)).start()
    logger.info("Threads started")

    return queue


def make_batch(table_name, header, records, extracted_at, batch_number):
    return {
        'filename': f"{table_name}_{extracted_at.strftime('%Y%m%d-%H%M%S')}_{batch_number:>04d}.csv",
        'table': table_name,
        'header': header,
        'records': records
    }

@task
def load_new_comments(table_name, dataframe):
    logger = prefect.context.get("logger")
    logger.info("Setting up")
    queue = Queue()
    setup(queue, logger)

    header = ['comment_guid', 'owner_guid', 'time_created', 'body', 'spam_predict', 'extracted_at']
    values = dataframe.to_dict('records')
    extracted_at = datetime.now()

    logger.info(f"Loading {len(values)} entries into {table_name}.")
    batch_number = 1 
    for chunk in divide_chunks(values, 100000):
        logger.info(f"Batch {batch_number} of length: {len(chunk)}")
        records = []
        for record in chunk:
            record['body'] = to_string(record['body'])
            record['time_created'] = record['time_created'].to_pydatetime()
            record['extracted_at'] = extracted_at
            records.append(record)

        batch = make_batch(table_name, header, records, extracted_at, batch_number)
        logger.info(f"Queuing batch {batch_number}")
        queue.put(batch)
        batch_number += 1

    while not queue.empty():
        logger.info(f"{queue.qsize()} batches in queue")
        sleep(10)

    logger.info("All batches processed, closing queue")
    queue.put(None)

with Flow("classify-new-comments") as flow:
    load_new_comments('int_comments_predict_spam', new_comments())

flow.register(project_name=PROJECT_NAME, labels=['data-etl'])
