#!/bin/bash

# Add prerequisite debian packages
apt update
apt install -y git libmariadb-dev-compat

# Set git SSH key
echo $GIT_PRIVATE_KEY | tr ";" "\n" > /root/.ssh/id_ed25519
chmod 600 /root/.ssh/id_ed25519

# Clone repo for running dbt
git clone ${GIT_USER}@${GIT_HOST}:${GIT_ORG_NAME}/${GIT_REPO_NAME}.git /root/data-etl

# Add prerequisite python packages
pip3 install -r /root/prefect/requirements.txt

# Set up prefect and add flows
cp /root/prefect/config.toml /root/.prefect/config.toml

prefect server create-tenant --name "Minds" --slug minds 
prefect create project "${PREFECT_PROJECT_NAME}"

# Register all Prefect flows in the flows directory
find /root/flows -name \*-flow.py | xargs -n 1 python3

# Make data staging directory
mkdir /root/data

# Install nltk stopwords
python3 -m nltk.downloader stopwords

# Start prefect agent
prefect agent local start --label data-etl --import-path /root/flows